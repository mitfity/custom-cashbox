module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        concat :{
            options: {
                separator: '\n'
            },
            mainApp: {
                src: [
                    'js/core/core.js',
                    'js/core/**/init.js',
                    'js/core/**/*.js',
                    'js/submodules/**/init.js',
                    'js/submodules/**/Model.js',
                    'js/submodules/**/Collection.js',
                    'js/submodules/**/View.js',
                    'js/submodules/**/*.js',
                    'js/main.js'
                ],
                dest: 'js/<%= pkg.name %>.js'
            }
        },
        jst: {
            compile: {
                options: {
                    prettify: true
                },
                files: {
                    "js/templates.js": ['js/core/**/*.tpl.html', 'js/submodules/**/*.tpl.html']
                }
            }
        },
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n'
            },
            mainApp: {
                files: {
                    'js/<%= pkg.name %>.min.js': ['<%= concat.mainApp.dest %>']
                }
            }
        },
        jshint: {
            files: ['js/core/**/*.js', 'js/submodules/**/*.js', 'js/main.js'],
            options: {
                // options here to override JSHint defaults
                globals: {
                    jQuery: true,
                    console: true,
                    module: true,
                    document: true
                }
            }
        },
        watch: {
            js: {
                files: ['<%= jshint.files %>'],
                tasks: ['jshint', 'concat', 'uglify']
            },
            html: {
                files: ['js/core/**/*.tpl.html', 'js/submodules/**/*.tpl.html'],
                tasks: ['jst']
            }
        }
    });

    require('load-grunt-tasks')(grunt);
    //grunt.loadNpmTasks('grunt-contrib-uglify');
    //grunt.loadNpmTasks('grunt-contrib-jshint');
    //grunt.loadNpmTasks('grunt-contrib-watch');
    //grunt.loadNpmTasks('grunt-contrib-concat');
    //grunt.loadNpmTasks('grunt-contrib-jst');

    grunt.registerTask('default', ['jshint', 'concat', 'uglify']);

};