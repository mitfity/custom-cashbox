<?php

/**
 * Created by PhpStorm.
 * User: yurii
 * Date: 13.08.15
 * Time: 16:59
 */
class API
{
    /**
     * Property: method
     * The HTTP method this request was made in, either GET, POST, PUT or DELETE
     */
    protected $method = '';
    /**
     * Property: endpoint
     * The Model requested in the URI. eg: /files
     */
    protected $endpoint = '';
    /**
     * Property: verb
     * An optional additional descriptor about the endpoint, used for things that can
     * not be handled by the basic methods. eg: /files/process
     */
    protected $verb = '';
    /**
     * Property: args
     * Any additional URI components after the endpoint and verb have been removed, in our
     * case, an integer ID for the resource. eg: /<endpoint>/<verb>/<arg0>/<arg1>
     * or /<endpoint>/<arg0>
     */
    protected $args = Array();
    /**
     * Property: file
     * Stores the input of the PUT request
     */
    protected $file = Null;

    /**
     * Property: request
     * Stores the get request data
     */
    protected $request = Null;

    /**
     * Constructor: __construct
     * Allow for CORS, assemble and pre-process the data
     */
    public function __construct($request) {

        $this->addCORSHeaders();
        $this->parseArgs($request);
        $this->parseMethod();
    }

    /**
     * CORS configuration.
     */
    private function addCORSHeaders() {
        header("Access-Control-Allow-Orgin: *");
        header("Access-Control-Allow-Methods: *");
        header("Content-Type: application/json");
    }

    /**
     * Parse url.
     * @param $request
     */
    private function parseArgs($request) {
        $this->args = explode('/', trim($request, '/'));
        // Step over root routing directory
        while( $val = array_shift( $this->args ) ) {
            if ($val == 'api') {
                break;
            }
        }
        $this->endpoint = array_shift($this->args);
        if (array_key_exists(0, $this->args) && !is_numeric($this->args[0])) {
            $this->verb = array_shift($this->args);
        }
    }

    /**
     * Parse http method.
     * @throws Exception Unexpected header
     */
    private function parseMethod() {
        $this->method = $_SERVER['REQUEST_METHOD'];
        if ($this->method == 'POST' && array_key_exists('HTTP_X_HTTP_METHOD', $_SERVER)) {
            if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'DELETE') {
                $this->method = 'DELETE';
            } else if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'PUT') {
                $this->method = 'PUT';
            } else {
                throw new Exception("Unexpected Header");
            }
        }

        switch($this->method) {
            case 'DELETE':
            case 'POST':
                $this->request = $this->_cleanInputs($_POST);
                $this->file = file_get_contents("php://input");
                break;
            case 'GET':
                $this->request = $this->_cleanInputs($_GET);
                break;
            case 'PUT':
                $this->request = $this->_cleanInputs($_GET);
                $this->file = file_get_contents("php://input");
                break;
            default:
                self::sendResponse('Invalid Method', 405);
                break;
        }
    }

    public function processAPI() {
        $directories = glob('submodules/' . $this->endpoint , GLOB_ONLYDIR);

        if (!count($directories) > 0) {
            self::sendResponse('Such route is not found.', 404);
            return;
        }

        $directory = $directories[0];

        $fileName = "Controller";

        $files = glob($directory . "/$fileName.php");

        if (!count($files) > 0) {
            self::sendResponse('Such route is not found.', 404);
            return;
        }

        try  {
            $file = $files[0];
            require($file);

            $controllerClassName = ucfirst($this->endpoint) . 'Controller';
            if (!class_exists($controllerClassName)) {
                self::sendResponse('Contrtoller not found ' . $controllerClassName, 404);
                return;
            }

            $controller = new $controllerClassName();
            $controller->processRoute($this->method, $this->verb, $this->args, $this->file, $this->request);
        } catch(Exception $e) {
            self::sendResponse($e->getMessage(), 500);
            return;
        }
    }

    private function _cleanInputs($data) {
        $clean_input = Array();
        if (is_array($data)) {
            foreach ($data as $k => $v) {
                $clean_input[$k] = $this->_cleanInputs($v);
            }
        } else {
            $clean_input = trim(strip_tags($data));
        }

        return $clean_input;
    }

    public static function sendResponse($data, $status = 200, $errorMessage = '') {
        $errorMessage = ($errorMessage) ? $errorMessage : self::requestStatus($status);
        header("HTTP/1.1 " . $status . " " . $errorMessage);
        echo json_encode($data, JSON_NUMERIC_CHECK);
    }

    private static function requestStatus($code) {
        $status = array(
            100 => 'Continue',
            101 => 'Switching Protocols',
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            203 => 'Non-Authoritative Information',
            204 => 'No Content',
            205 => 'Reset Content',
            206 => 'Partial Content',
            300 => 'Multiple Choices',
            301 => 'Moved Permanently',
            302 => 'Moved Temporarily',
            303 => 'See Other',
            304 => 'Not Modified',
            305 => 'Use Proxy',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            407 => 'Proxy Authentication Required',
            408 => 'Request Time-out',
            409 => 'Conflict',
            410 => 'Gone',
            411 => 'Length Required',
            412 => 'Precondition Failed',
            413 => 'Request Entity Too Large',
            414 => 'Request-URI Too Large',
            415 => 'Unsupported Media Type',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
            504 => 'Gateway Time-out',
            505 => 'HTTP Version not supported'
        );

        return ($status[$code]) ? $status[$code] : 'Code message not specified.';
    }
}