<?php

/**
 * Created by PhpStorm.
 * User: yurii
 * Date: 04.09.15
 * Time: 15:54
 */

require_once __DIR__ . '/../API.php';

class Contrtoller
{

    public function processRoute($method, $verb, $args, $file, $request) {
        $apiMethod = 'api';

        if ($verb) {
            $apiMethod .= ucfirst($verb);
        }

        $beforeRouteMethod = $apiMethod . 'BeforeRoute';
        $apiMethod .= ucfirst(strtolower($method));

        if (method_exists($this, $apiMethod)) {

            if (method_exists($this, $beforeRouteMethod)) {
                $this->{$beforeRouteMethod}($apiMethod, $args, $file, $request);
                return;
            }

            $this->{$apiMethod}($args, $file, $request);
            return;
        }

        API::sendResponse('Such route is not defined.', 404);
    }

}