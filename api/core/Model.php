<?php

/**
 * Created by PhpStorm.
 * User: yurii
 * Date: 04.09.15
 * Time: 16:15
 */

require_once __DIR__ . '/../API.php';
require_once __DIR__ . '/../utils/DBConnection.php';
require_once __DIR__ . '/../utils/StdObject.php';
require_once __DIR__ . '/../core/Validator.php';


class Model
{

    protected $tableName;

    protected $db;

    protected $attributes;

    protected function __construct($tableName)
    {
        $this->tableName = $tableName;
        $this->db = DBConnection::getInstance()->getConnection();
        $this->attributes = new StdObject();
    }

    /**
     * First element of filters array will be selected as responseError->field and responseError->errorMessage target.
     * @param $filters array Filters array with {
     *      'name' => 'email',
     *      'value' => 'cat@bad.com',
     *      'sanitizeType' => PDO::PARAM_STR
     * } like objects.
     * @return Bool
     */
    protected function alreadyExists($filters, $dontResponseError = false) {
        $alreadyExistsSql = "SELECT * from " . $this->tableName . " WHERE ";

        foreach ($filters as $filter) {
            $index = array_search($filter, $filters);
            $equalSign = "=";

            if ($filter->equalSign) {
                $equalSign = $filter->equalSign;
            }

            $columnName = $filter->name;
            if ($filter->columnName) {
                $columnName = $filter->columnName;
            }

            if ($index !== count($filters) - 1) {
                $alreadyExistsSql .= $columnName . " " . $equalSign . " :" . $filter->name . " AND ";
            } else {
                $alreadyExistsSql .= $columnName . " " . $equalSign . " :" . $filter->name;
            }
        }

        $stmt = $this->db->prepare($alreadyExistsSql);

        foreach ($filters as $filter) {
            $stmt->bindParam(':' . $filter->name, $filter->value, $filter->sanitizeType);
        }

        try {
            $stmt->execute();
        } catch (Exception $e) {
            echo 'Error: ' . $e->getMessage() . "\r\n";
        }

        $alreadyExists = $stmt->rowCount() > 0;
        if ($alreadyExists) {
            $responseError = new StdObject();
            $firstFilter = $filters[0];
            $responseError->field = $firstFilter->name;
            $responseError->errorMessage = 'Such ' . $firstFilter->name . ' already exists.';

            if (!$dontResponseError) {
                Api::sendResponse($responseError, 409);
            }
            return true;
        }

        return false;
    }

    public function set($argument) {
        if (gettype($argument) === "object") {
            foreach ($argument as $property => $value) {
                $this->attributes->{$property} = $value;
            }
        }
    }

    public function get($attributeName) {
        if (gettype($attributeName) === "string") {
            return $this->attributes->{$attributeName};
        }

        return $this->attributes;
    }

    protected function missingAttributes($attributes) {
        if (!is_array($attributes)) {
            return true;
        }

        foreach ($attributes as $attribute) {
            if (!isset($this->attributes->{$attribute})) {
                $responseError = new StdObject();
                $responseError->field = $attribute;
                $responseError->errorMessage = 'Please enter ' . $attribute . '.';

                API::sendResponse($responseError, 403);
                return true;
            }
        }

        return false;
    }

}