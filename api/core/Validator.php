<?php

/**
 * Created by PhpStorm.
 * User: yurii
 * Date: 04.09.15
 * Time: 18:10
 */
class Validator
{

    private static $instance;

    /**
     * Returns the *Singleton* instance of this class.
     */
    public static function getInstance()
    {
        if (null === static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    /**
     * Protected constructor to prevent creating a new instance of the
     * *Singleton* via the `new` operator from outside of this class.
     */
    protected function __construct()
    {
    }

    /**
     * Private clone method to prevent cloning of the instance of the
     * *Singleton* instance.
     */
    private function __clone()
    {
    }

    /**
     * Private unserialize method to prevent unserializing of the *Singleton*
     * instance.
     */
    private function __wakeup()
    {
    }

    public function isValidEmail($email) {
        return (bool)filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    public function isValidPhone($phone) {
        return (bool)preg_match("/^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{1,100}$/", $phone);
    }

    public function isValidDate($date) {
        return (bool)preg_match("/^((0?[1-9]|1[012])[\-\ \/.](0?[1-9]|[12][0-9]|3[01])[\-\ \/.](19|20)?[0-9]{2})*$/", $date);
    }

}