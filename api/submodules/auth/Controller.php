<?php
/**
 * Created by PhpStorm.
 * User: yurii
 * Date: 08.09.15
 * Time: 19:34
 */

require_once __DIR__.'/../../core/Contrtoller.php';
require_once __DIR__.'/Model.php';

class AuthController extends Contrtoller {

    protected function apiLoginPost($args, $file) {
        if ($args) {
            API::sendResponse('POST method for this route is not allowed', 404);
            return;
        }

        $model = new AuthModel();

        if ($file) {
            $model->update(json_decode($file));
        }
    }

    protected function apiLoginGet($args, $file) {
        $model = new AuthModel();
        $model->fetch();
    }

    protected function apiLogoutBeforeRoute($apiMethod, $args, $file) {
        $userId = $_SESSION["userId"];

        if (!isset($userId)) {
            API::sendResponse('You are not logged in.', 401);
            return;
        }

        $this->{$apiMethod}($args, $file);
    }

    protected function apiLogoutPost($args, $file) {
        session_destroy();
        API::sendResponse('Logged out.', 200);
    }

}