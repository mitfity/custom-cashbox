<?php
/**
 * Created by PhpStorm.
 * User: yurii
 * Date: 08.09.15
 * Time: 19:40
 */

require_once __DIR__.'/../../core/Model.php';
require_once __DIR__.'/../../utils/Utils.php';

class AuthModel extends Model
{

    public function __construct()
    {
        parent::__construct(strtolower(basename(__DIR__)));
        $this->tableName = 'user';
    }

    public function fetch() {
        $userId = $_SESSION["userId"];

        if (!isset($userId)) {
            API::sendResponse('You are not logged in.', 401);
            return;
        }

        $sql = "SELECT userId, firstName, lastName, avatar, role, isActive, email, phone FROM user" .
            " WHERE userId = :userId";
        $user = null;

        try {
            $db = $this->db;
            $stmt = $db->prepare($sql);
            $stmt->bindParam(':userId', $userId, PDO::PARAM_INT);
            $stmt->execute();
            $user = $stmt->fetch(PDO::FETCH_OBJ);
            $db = null;
        } catch(PDOException $e) {
            API::sendResponse($e->getMessage(), 500);
            return;
        }

        API::sendResponse($user, 200);
    }

    public function update($authConfig) {
        $this->set($authConfig);

        try {
            $db = $this->db;

            $responseError = new stdClass();

            if ($this->missingAttributes(array('email', 'password'))) {
                return;
            }

            if (!Validator::getInstance()->isValidEmail($this->get('email'))) {
                API::sendResponse('Invalid email format', 400);
                return;
            }

            if (!$this->alreadyExists(array(
                new StdObject(array(
                    'name' => 'email',
                    'value' => $this->get('email'),
                    'sanitizeType' => PDO::PARAM_STR
                ))), true)
            ) {
                $responseError->field = 'email';
                $responseError->errorMessage = "Such email doesn't exist.";
                API::sendResponse($responseError, 403);
                return;
            }

            $sql = "SELECT * FROM user WHERE email = :email";
            $stmt = $db->prepare($sql);
            $stmt->bindParam(':email', $this->get('email'), PDO::PARAM_STR);
            $stmt->execute();
            $user = $stmt->fetch(PDO::FETCH_OBJ);

            if (!password_verify($this->get('password'), $user->password)) {
                $responseError->field = 'password';
                $responseError->errorMessage = "Wrong password.";
                API::sendResponse($responseError, 403);
                return;
            }

            if (!$user->isActive) {
                $responseError->field = 'email';
                $responseError->errorMessage = "This user is banned.";
                API::sendResponse($responseError, 403);
                return;
            }

            unset($user->password);
            $user->userId = (int) $user->userId;
            $_SESSION["userId"] = $user->userId;

            API::sendResponse($user, 200);
            return;
        } catch(PDOException $e) {
            API::sendResponse($e->getMessage(), 500);
        }
    }


}