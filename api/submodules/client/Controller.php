<?php
/**
 * Created by PhpStorm.
 * User: yurii
 * Date: 09.09.15
 * Time: 16:36
 */

require_once __DIR__.'/../../core/Contrtoller.php';
require_once __DIR__.'/Model.php';

class ClientController extends Contrtoller {

    protected function apiBeforeRoute($apiMethod, $args, $file) {
        $userId = $_SESSION["userId"];

        if (!isset($userId)) {
            API::sendResponse('You are not logged in.', 401);
            return;
        }

        $this->{$apiMethod}($args, $file);
    }

    protected function apiGet($args, $file) {
        $model = new ClientModel();

        if ($args) {
            $id = $args[0];
            $model->set(new StdObject(array(
                'clientId' => $id
            )));
            $model->fetch();
            return;
        }

        $model->retrieve();
    }

    protected function apiPost($args, $file) {
        if ($args) {
            API::sendResponse('POST method for this route is not allowed', 404);
            return;
        }

        $model = new ClientModel();

        if ($file) {
            $model->create(json_decode($file));
        }
    }

    protected function apiPut($args, $file) {
        if (!$args) {
            API::sendResponse('PUT method for this route is not allowed', 404);
            return;
        }

        $model = new ClientModel();
        $id = $args[0];
        $model->set(new StdObject(array(
            'clientId' => $id
        )));

        if ($file) {
            $model->update(json_decode($file));
        }
    }

    protected function apiResetPost($args, $file) {
        $model = new ClientModel();

        if ($file) {
            $model->reset(json_decode($file));
        }
    }

}