<?php
/**
 * Created by PhpStorm.
 * User: yurii
 * Date: 09.09.15
 * Time: 16:37
 */

require_once __DIR__.'/../../core/Model.php';
require_once __DIR__.'/../../utils/Utils.php';

class ClientModel extends Model
{

    public function __construct() {
        parent::__construct(strtolower(basename(__DIR__)));
    }

    public function retrieve() {
        $sql = "select * FROM client WHERE removed = 0";
        $clients = null;

        try {
            $db = $this->db;
            $stmt = $db->prepare($sql);
            $stmt->execute();
            $clients = $stmt->fetchAll(PDO::FETCH_OBJ);
            $db = null;
        } catch(PDOException $e) {
            API::sendResponse($e->getMessage(), 500);
            return;
        }

        API::sendResponse($clients, 200);
    }

    public function fetch() {
        try {
            $sql = "select * FROM client WHERE clientId = :clientId AND removed = 0";
            $db = $this->db;
            $stmt = $db->prepare($sql);
            $stmt->bindParam(':clientId', $this->get('clientId'), PDO::PARAM_INT);
            $stmt->execute();
            $client = $stmt->fetchAll(PDO::FETCH_OBJ);
            $db = null;

            if (!$client) {
                API::sendResponse('Client not found', 404);
                return;
            }

            API::sendResponse($client[0], 200);
            return;
        } catch(PDOException $e) {
            API::sendResponse($e->getMessage());
            return;
        }
    }

    public function create($clientConfig) {
        $this->set($clientConfig);

        try {
            $db = $this->db;

            if ($this->missingAttributes(array('email', 'phone')) || !$this->validateData()) {
                return;
            }

            if ($this->get('secondAddress') || $this->get('firstAddress') || $this->get('zip') || $this->get('city') ||
                $this->get('state') || $this->get('country')) {
                if ( $this->missingAttributes(array('firstAddress', 'zip', 'city', 'state', 'country')) ) {
                    return;
                }
            }

            if ($this->alreadyExists(array(
                    new StdObject(array(
                        'name' => 'phone',
                        'value' => $this->get('phone'),
                        'sanitizeType' => PDO::PARAM_STR
                    ))
                )) || $this->alreadyExists(array(
                    new StdObject(array(
                        'name' => 'email',
                        'value' => $this->get('email'),
                        'sanitizeType' => PDO::PARAM_STR
                    ))
                ))
            ) {
                return;
            }

            $sql = "INSERT INTO client (firstName, lastName, email, phone, organization, country, " .
                " state, city, zip, firstAddress, secondAddress, removed) " .
                "VALUES (:firstName, :lastName, :email," .
                " :phone, :organization, :country," .
                " :state, :city, :zip," .
                " :firstAddress, :secondAddress, :removed);";

            $stmt = $db->prepare($sql);
            $stmt->bindParam(':firstName', $this->get('firstName'), PDO::PARAM_STR);
            $stmt->bindParam(':lastName', $this->get('lastName'), PDO::PARAM_STR);
            $stmt->bindParam(':email', $this->get('email'), PDO::PARAM_STR);
            $stmt->bindParam(':phone', $this->get('phone'), PDO::PARAM_STR);
            $stmt->bindParam(':organization', $this->get('organization'), PDO::PARAM_STR);
            $stmt->bindParam(':country', $this->get('country'), PDO::PARAM_STR);
            $stmt->bindParam(':state', $this->get('state'), PDO::PARAM_STR);
            $stmt->bindParam(':city', $this->get('city'), PDO::PARAM_STR);
            $stmt->bindParam(':zip', $this->get('zip'), PDO::PARAM_STR);
            $stmt->bindParam(':firstAddress', $this->get('firstAddress'), PDO::PARAM_STR);
            $stmt->bindParam(':secondAddress', $this->get('secondAddress'), PDO::PARAM_STR);
            $stmt->bindParam(':removed', $this->get('removed'), PDO::PARAM_INT);
            $stmt->execute();

            $this->set(new StdObject(array(
                'clientId' => (int)$db->lastInsertId()
            )));

            $db = null;

            API::sendResponse(json_encode($this->attributes), 201);
            return;
        } catch(PDOException $e) {
            API::sendResponse($e->getMessage(), 500);
        }
    }

    public function update($clientConfig) {
        $this->set($clientConfig);

        try {
            $db = $this->db;

            if ($this->missingAttributes(array('email', 'phone')) || !$this->validateData()) {
                return;
            }

            if ($this->get('secondAddress') || $this->get('firstAddress') || $this->get('zip') || $this->get('city') ||
                $this->get('state') || $this->get('country')) {
                if ( $this->missingAttributes(array('firstAddress', 'zip', 'city', 'state', 'country')) ) {
                    return;
                }
            }

            if ($this->alreadyExists(array(
                    new StdObject(array(
                        'name' => 'phone',
                        'value' => $this->get('phone'),
                        'sanitizeType' => PDO::PARAM_STR
                    )),
                    new StdObject(array(
                        'name' => 'clientId',
                        'value' => $this->get('clientId'),
                        'sanitizeType' => PDO::PARAM_INT,
                        'equalSign' => '<>'
                    ))
                )) || $this->alreadyExists(array(
                    new StdObject(array(
                        'name' => 'email',
                        'value' => $this->get('email'),
                        'sanitizeType' => PDO::PARAM_STR
                    )),
                    new StdObject(array(
                        'name' => 'clientId',
                        'value' => $this->get('clientId'),
                        'sanitizeType' => PDO::PARAM_INT,
                        'equalSign' => '<>'
                    ))
                ))
            ) {
                return;
            }

            $sql = "UPDATE client " .
                "SET firstName = :firstName, lastName = :lastName," .
                " email = :email, phone = :phone, " .
                " organization = :organization," .
                " country = :country, state = :state, city = :city," .
                " zip = :zip, firstAddress = :firstAddress," .
                " secondAddress = :secondAddress, removed = :removed " .
                "WHERE clientId = :clientId";

            $stmt = $db->prepare($sql);
            $stmt->bindParam(':firstName', $this->get('firstName'), PDO::PARAM_STR);
            $stmt->bindParam(':lastName', $this->get('lastName'), PDO::PARAM_STR);
            $stmt->bindParam(':email', $this->get('email'), PDO::PARAM_STR);
            $stmt->bindParam(':phone', $this->get('phone'), PDO::PARAM_STR);
            $stmt->bindParam(':organization', $this->get('organization'), PDO::PARAM_STR);
            $stmt->bindParam(':country', $this->get('country'), PDO::PARAM_STR);
            $stmt->bindParam(':state', $this->get('state'), PDO::PARAM_STR);
            $stmt->bindParam(':city', $this->get('city'), PDO::PARAM_STR);
            $stmt->bindParam(':zip', $this->get('zip'), PDO::PARAM_STR);
            $stmt->bindParam(':firstAddress', $this->get('firstAddress'), PDO::PARAM_STR);
            $stmt->bindParam(':secondAddress', $this->get('secondAddress'), PDO::PARAM_STR);
            $stmt->bindParam(':removed', $this->get('removed'), PDO::PARAM_INT);
            $stmt->bindParam(':clientId', $this->get('clientId'), PDO::PARAM_INT);
            $stmt->execute();
            $db = null;

            API::sendResponse(json_encode($this->attributes), 201);
            return;
        } catch(PDOException $e) {
            API::sendResponse($e->getMessage(), 500);
        }
    }

    private function validateData() {
        if (!Validator::getInstance()->isValidEmail($this->get('email'))) {
            API::sendResponse('Invalid email format', 400);
            return false;
        }

        if (!Validator::getInstance()->isValidPhone($this->get('phone'))) {
            API::sendResponse('Invalid phone format', 400);
            return false;
        }

        return true;
    }

}