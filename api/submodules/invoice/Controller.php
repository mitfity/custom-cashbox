<?php
/**
 * Created by PhpStorm.
 * User: yurii
 * Date: 09.09.15
 * Time: 16:46
 */


require_once __DIR__.'/../../core/Contrtoller.php';
require_once __DIR__.'/Model.php';

class InvoiceController extends Contrtoller {

    protected function apiBeforeRoute($apiMethod, $args, $file) {
        $userId = $_SESSION["userId"];

        if (!isset($userId)) {
            API::sendResponse('You are not logged in.', 401);
            return;
        }

        $this->{$apiMethod}($args, $file);
    }

    protected function apiGet($args, $file) {
        $model = new InvoiceModel();

        if ($args) {
            $id = $args[0];
            $model->set(new StdObject(array(
                'invoiceId' => $id
            )));
            $model->fetch();
            return;
        }

        $model->retrieve();
    }

    protected function apiPost($args, $file) {
        if ($args) {
            API::sendResponse('POST method for this route is not allowed', 404);
            return;
        }

        $model = new InvoiceModel();

        if ($file) {
            $model->create(json_decode($file));
        }
    }

    protected function apiPut($args, $file) {
        if (!$args) {
            API::sendResponse('PUT method for this route is not allowed', 404);
            return;
        }

        $model = new InvoiceModel();
        $id = (int) $args[0];

        if ($file) {
            $model->update(json_decode($file), $id);
        }
    }

    protected function apiDelete($args, $file) {
        if (!$args) {
            API::sendResponse('DELETE method for this route is not allowed', 404);
            return;
        }

        $model = new InvoiceModel();
        $id = (int) $args[0];

        $model->delete($id);
    }

}