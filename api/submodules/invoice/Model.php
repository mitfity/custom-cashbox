<?php
/**
 * Created by PhpStorm.
 * User: yurii
 * Date: 09.09.15
 * Time: 16:46
 */

require_once __DIR__.'/../../core/Model.php';
require_once __DIR__.'/../../utils/Utils.php';

class InvoiceModel extends Model
{

    public function __construct() {
        parent::__construct(strtolower(basename(__DIR__)));
    }

    public function retrieve() {
        $sql = "select *, DATE_FORMAT(dateCreated, '%m/%d/%Y') AS dateCreated FROM invoice";
        $invoices = null;

        try {
            $db = $this->db;
            $stmt = $db->prepare($sql);
            $stmt->execute();
            $invoices = $stmt->fetchAll(PDO::FETCH_OBJ);
            $db = null;
        } catch(PDOException $e) {
            API::sendResponse($e->getMessage(), 500);
            return;
        }

        API::sendResponse($invoices, 200);
    }

    public function fetch() {
        try {
            $sql = "select *, DATE_FORMAT(dateCreated, '%m/%d/%Y') AS dateCreated FROM invoice" .
                " WHERE invoiceId = :invoiceId";
            $db = $this->db;
            $stmt = $db->prepare($sql);
            $stmt->bindParam(':invoiceId', $this->get('invoiceId'), PDO::PARAM_INT);
            $stmt->execute();
            $invoice = $stmt->fetchAll(PDO::FETCH_OBJ);

            $sql = "SELECT * FROM invoiceItem WHERE invoiceId = :invoiceId";
            $stmt = $db->prepare($sql);
            $stmt->bindParam(':invoiceId', $invoice[0]->invoiceId, PDO::PARAM_INT);
            $stmt->execute();
            $invoice[0]->invoiceItems = $stmt->fetchAll(PDO::FETCH_OBJ);

            $db = null;

            if (!$invoice) {
                API::sendResponse('Invoice not found', 404);
                return;
            }

            API::sendResponse($invoice[0], 200);
            return;
        } catch(PDOException $e) {
            API::sendResponse($e->getMessage());
            return;
        }
    }

    public function create($invoiceConfig) {
        $this->set($invoiceConfig);

        try {
            $db = $this->db;

            if ($this->missingAttributes(array('userId', 'invoiceId', 'dateCreated')) || !$this->validateData()) {
                return;
            }
            if ($this->alreadyExists(array(
                    new StdObject(array(
                        'name' => 'invoiceId',
                        'value' => $this->get('invoiceId'),
                        'sanitizeType' => PDO::PARAM_INT
                    ))
                ))
            ) {
                return;
            }

            // Calculate total
            $this->set(new StdObject(array(
                'total' => 0
            )));
            foreach ($this->get('invoiceItems') as $item) {
                $this->set(new StdObject(array(
                    'total' => $this->get('total') + $item->price * $item->count * (1.0 + $item->tax / 100)
                )));
            }

            // Insetr invoice
            $stmt = $db->prepare("INSERT INTO invoice (invoiceId, userId, clientId, dateCreated, total)" .
                " VALUES (:invoiceId, :userId, :clientId," .
                " STR_TO_DATE(:dateCreated, '%m/%d/%Y'), :total);");

            $stmt->bindParam(':invoiceId', $this->get('invoiceId'), PDO::PARAM_INT);
            $stmt->bindParam(':userId', $this->get('userId'), PDO::PARAM_INT);
            $stmt->bindParam(':clientId', $this->get('clientId'), PDO::PARAM_INT);
            $stmt->bindParam(':dateCreated', $this->get('dateCreated'), PDO::PARAM_STR);
            $stmt->bindParam(':total', $this->get('total'), PDO::PARAM_INT);

            $stmt->execute();

            // Insert invoice items
            foreach ($this->get('invoiceItems') as $item) {
                if ($item->description && !$item->name) {
                    API::sendResponse('Invoice item name have to be set.', 400);
                    return;
                }

                if (!$item->description && !$item->name) {
                    $item->price = 0;
                    $item->count = 0;
                }

                try {
                    $sql = "INSERT INTO invoiceItem (invoiceId, name, description, count, tax, price)" .
                        " VALUES (:invoiceId, :name, :description," .
                        " :count, :tax, :price);";

                    $stmt = $db->prepare($sql);
                    $stmt->bindParam(':invoiceId', $this->get('invoiceId'), PDO::PARAM_INT);
                    $stmt->bindParam(':name', $item->name, PDO::PARAM_STR);
                    $stmt->bindParam(':description', $item->description, PDO::PARAM_STR);
                    $stmt->bindParam(':count', $item->count, PDO::PARAM_INT);
                    $stmt->bindParam(':tax', $item->tax, PDO::PARAM_INT);
                    $stmt->bindParam(':price', $item->price, PDO::PARAM_INT);
                    $stmt->execute();

                    $sql = "SELECT * FROM product WHERE name = '{$item->name}'";
                    $stmt = $db->prepare($sql);
                    $stmt->execute();
                    $alreadyExists = $stmt->fetchColumn() > 0;

                    // Update products
                    if ($alreadyExists) {
                        $sql = "UPDATE product" .
                            " SET description = :description," .
                            " price = :price" .
                            " WHERE name = :name";

                        $stmt = $db->prepare($sql);
                        $stmt->bindParam(':description', $item->description, PDO::PARAM_STR);
                        $stmt->bindParam(':price', $item->price, PDO::PARAM_INT);
                        $stmt->bindParam(':name', $item->name, PDO::PARAM_STR);
                        $stmt->execute();
                    }
                    else if ($item->name) {
                        $sql = "INSERT INTO product (name, description, price)" .
                            " VALUES (:name, :description," .
                            " :price);";

                        $stmt = $db->prepare($sql);
                        $stmt->bindParam(':description', $item->description, PDO::PARAM_STR);
                        $stmt->bindParam(':price', $item->price, PDO::PARAM_INT);
                        $stmt->bindParam(':name', $item->name, PDO::PARAM_STR);
                        $stmt->execute();
                    }

                } catch(PDOException $e) {
                    API::sendResponse('{"error":{"text":'. $e->getMessage() .'}}', 400);
                    return;
                }
            }

            $stmt->invoiceId = $db->lastInsertId();
            $db = null;

            API::sendResponse(json_encode($stmt), 201);
            return;
        } catch(PDOException $e) {
            API::sendResponse('{"error":{"text":'. $e->getMessage() .'}}', 400);
            return;
        }
    }

    public function update($invoiceConfig, $targetInvoiceId) {
        $this->set($invoiceConfig);

        try {
            $db = $this->db;

            if ($this->missingAttributes(array('userId', 'invoiceId', 'dateCreated')) || !$this->validateData()) {
                return;
            }
            if ($this->alreadyExists(array(
                new StdObject(array(
                    'name' => 'invoiceId',
                    'value' => $this->get('invoiceId'),
                    'sanitizeType' => PDO::PARAM_INT
                )),
                new StdObject(array(
                    'name' => 'targetInvoiceId',
                    'columnName' => 'invoiceId',
                    'value' => $targetInvoiceId,
                    'sanitizeType' => PDO::PARAM_INT,
                    'equalSign' => '<>'
                ))
            ))
            ) {
                return;
            }

            // Clear all invoice items before puting new ones.
            try {
                $sql = "DELETE FROM invoiceItem WHERE invoiceId = :invoiceId";

                $stmt = $db->prepare($sql);
                $stmt->bindParam(':invoiceId', $targetInvoiceId, PDO::PARAM_INT);
                $stmt->execute();
            } catch(PDOException $e) {
                API::sendResponse($e->getMessage());
                return;
            }

            // Calc total
            $this->set(new StdObject(array(
                'total' => 0
            )));
            foreach ($this->get('invoiceItems') as $item) {
                $this->set(new StdObject(array(
                    'total' => $this->get('total') + $item->price * $item->count * (1.0 + $item->tax / 100)
                )));
            }

            // Update invoice
            try {
                $stmt = $db->prepare("UPDATE invoice" .
                    " SET invoiceId = :invoiceId, userId = :userId," .
                    " clientId = :clientId," .
                    " dateCreated = STR_TO_DATE(:dateCreated, '%m/%d/%Y'), " .
                    " total = :total" .
                    " WHERE invoiceId = :targetInvoiceId");

                $stmt->bindParam(':invoiceId', $this->get('invoiceId'), PDO::PARAM_INT);
                $stmt->bindParam(':userId', $this->get('userId'), PDO::PARAM_INT);
                $stmt->bindParam(':clientId', $this->get('clientId'), PDO::PARAM_INT);
                $stmt->bindParam(':dateCreated', $this->get('dateCreated'), PDO::PARAM_STR);
                $stmt->bindParam(':total', $this->get('total'), PDO::PARAM_INT);
                $stmt->bindParam(':targetInvoiceId', $targetInvoiceId, PDO::PARAM_INT);
                $stmt->execute();
            } catch(PDOException $e) {
                API::sendResponse($e->getMessage(), 500);
                return;
            }

            // Update invoice items
            foreach ($this->get('invoiceItems') as $item) {

                if ($item->description && !$item->name) {
                    API::sendResponse('Invoice item name have to be set.', 400);
                    return;
                }

                if (!$item->description && !$item->name) {
                    $item->price = 0;
                    $item->count = 0;
                }

                try {
                    $sql = "INSERT INTO invoiceItem (invoiceId, name, description, count, tax, price)" .
                        " VALUES (:invoiceId, :name, :description," .
                        " :count, :tax, :price);";

                    $stmt = $db->prepare($sql);
                    $stmt->bindParam(':invoiceId', $this->get('invoiceId'), PDO::PARAM_INT);
                    $stmt->bindParam(':name', $item->name, PDO::PARAM_STR);
                    $stmt->bindParam(':description', $item->description, PDO::PARAM_STR);
                    $stmt->bindParam(':count', $item->count, PDO::PARAM_INT);
                    $stmt->bindParam(':tax', $item->tax, PDO::PARAM_INT);
                    $stmt->bindParam(':price', $item->price, PDO::PARAM_INT);
                    $stmt->execute();

                    // Update products
                    $sql = "SELECT * FROM product WHERE name = '{$item->name}'";
                    $stmt = $db->prepare($sql);
                    $stmt->execute();
                    $alreadyExists = $stmt->fetchColumn() > 0;

                    if ($alreadyExists) {
                        $sql = "UPDATE product" .
                            " SET description = :description," .
                            " price = :price" .
                            " WHERE name = :name";

                        $stmt = $db->prepare($sql);
                        $stmt->bindParam(':description', $item->description, PDO::PARAM_STR);
                        $stmt->bindParam(':name', $item->name, PDO::PARAM_STR);
                        $stmt->bindParam(':price', $item->price, PDO::PARAM_INT);
                        $stmt->execute();
                    }
                    else if ($item->name) {
                        $sql = "INSERT INTO product (name, description, price)" .
                            " VALUES (:name, :description," .
                            " :price);";

                        $stmt = $db->prepare($sql);
                        $stmt->bindParam(':description', $item->description, PDO::PARAM_STR);
                        $stmt->bindParam(':name', $item->name, PDO::PARAM_STR);
                        $stmt->bindParam(':price', $item->price, PDO::PARAM_INT);
                        $stmt->execute();
                    }

                } catch(PDOException $e) {
                    API::sendResponse($e->getMessage(), 500);
                    return;
                }
            }

            $stmt->invoiceId = $db->lastInsertId();
            $db = null;

            API::sendResponse(json_encode($stmt), 201);
            return;
        } catch(PDOException $e) {
            API::sendResponse($e->getMessage(), 500);
            return;
        }
    }

    public function delete($id) {
        try {
            $db = $this->db;

            $sql = "DELETE FROM invoice WHERE invoiceId = :targetInvoiceId";

            $stmt = $db->prepare($sql);
            $stmt->bindParam(':targetInvoiceId', $id, PDO::PARAM_INT);
            $stmt->execute();
            $db = null;

            API::sendResponse('', 201);
            return;
        } catch(PDOException $e) {
            API::sendResponse($e->getMessage(), 500);
            return;
        }
    }

    private function validateData() {
        if (!Validator::getInstance()->isValidDate($this->get('dateCreated'))) {
            API::sendResponse('Invalid date format', 400);
            return false;
        }

        return true;
    }

}