<?php
/**
 * Created by PhpStorm.
 * User: yurii
 * Date: 09.09.15
 * Time: 17:39
 */

require_once __DIR__.'/../../core/Contrtoller.php';
require_once __DIR__.'/Model.php';

class ProductController extends Contrtoller
{

    protected function apiBeforeRoute($apiMethod, $args, $file)
    {
        $userId = $_SESSION["userId"];

        if (!isset($userId)) {
            API::sendResponse('You are not logged in.', 401);
            return;
        }

        $this->{$apiMethod}($args, $file);
    }

    protected function apiGet($args, $file, $request)
    {
        $model = new ProductModel();
        $model->retrieve($request);
    }
}