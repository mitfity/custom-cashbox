<?php
/**
 * Created by PhpStorm.
 * User: yurii
 * Date: 09.09.15
 * Time: 17:40
 */

require_once __DIR__.'/../../core/Model.php';
require_once __DIR__.'/../../utils/Utils.php';

class ProductModel extends Model
{

    public function __construct()
    {
        parent::__construct(strtolower(basename(__DIR__)));
    }

    public function retrieve($request)
    {
        $sql = "SELECT *, name AS label FROM product";
        $products = null;
        $term = null;

        if ($request) {
            $term = $request["term"];
            $term = mysql_real_escape_string($term);
            $sql = "SELECT *, name AS label FROM product WHERE name LIKE '%{$term}%' ";
        }

        try {
            $db = $this->db;
            $stmt = $db->prepare($sql);
            $stmt->bindParam(':term', $term, PDO::PARAM_STR);
            $stmt->execute();
            $products = $stmt->fetchAll(PDO::FETCH_OBJ);
            $db = null;
        } catch(PDOException $e) {
            API::sendResponse($e->getMessage(), 500);
            return;
        }

        API::sendResponse($products, 200);
    }

}