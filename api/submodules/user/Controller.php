<?php
/**
 * Created by PhpStorm.
 * User: yurii
 * Date: 04.09.15
 * Time: 15:24
 */

require_once __DIR__.'/../../core/Contrtoller.php';
require_once __DIR__.'/Model.php';

class UserController extends Contrtoller {

    protected function apiBeforeRoute($apiMethod, $args, $file) {
        $userId = $_SESSION["userId"];

        if (!isset($userId)) {
            API::sendResponse('You are not logged in.', 401);
            return;
        }

        $this->{$apiMethod}($args, $file);
    }

    protected function apiGet($args, $file) {
        $model = new UserModel();

        if ($args) {
            $id = $args[0];
            $model->set(new StdObject(array(
                'userId' => $id
            )));
            $model->fetch();
            return;
        }

        $model->retrieve();
    }

    protected function apiPost($args, $file) {
        if ($args) {
            API::sendResponse('POST method for this route is not allowed', 404);
            return;
        }

        $model = new UserModel();

        if ($file) {
            $model->create(json_decode($file));
        }
    }

    protected function apiPut($args, $file) {
        if (!$args) {
            API::sendResponse('PUT method for this route is not allowed', 404);
            return;
        }

        $model = new UserModel();
        $id = $args[0];
        $model->set(new StdObject(array(
            'userId' => $id
        )));

        if ($file) {
            $model->update(json_decode($file));
        }
    }

    protected function apiResetPost($args, $file) {
        $model = new UserModel();

        if ($file) {
            $model->reset(json_decode($file));
        }
    }

}