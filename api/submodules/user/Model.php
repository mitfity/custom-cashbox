<?php

/**
 * Created by PhpStorm.
 * User: yurii
 * Date: 04.09.15
 * Time: 16:18
 */

require_once __DIR__.'/../../core/Model.php';
require_once __DIR__.'/../../utils/Utils.php';

class UserModel extends Model
{

    public function __construct() {
        parent::__construct(strtolower(basename(__DIR__)));
    }

    public function retrieve() {
        try {
            $sql = "SELECT userId, firstName, lastName, avatar, role, isActive, email, phone FROM user";
            $users = null;
            $db = $this->db;
            $stmt = $db->prepare($sql);
            $stmt->execute();
            $users = $stmt->fetchAll(PDO::FETCH_OBJ);
            $db = null;

            API::sendResponse($users, 200);
            return;
        } catch(PDOException $e) {
            API::sendResponse($e->getMessage(), 500);
        }
    }

    public function fetch() {
        try {
            $sql = "SELECT userId, firstName, lastName, avatar, role, isActive, email, phone " .
                " FROM user WHERE userId = :userId";
            $db = $this->db;
            $stmt = $db->prepare($sql);
            $stmt->bindParam(':userId', $this->attributes->userId, PDO::PARAM_INT);
            $stmt->execute();
            $user = $stmt->fetchAll(PDO::FETCH_OBJ);
            $db = null;

            if (!$user) {
                API::sendResponse('User not found', 404);
                return;
            }

            API::sendResponse($user[0], 200);
            return;
        } catch(PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}';
            return;
        }
    }

    public function create($userConfig) {
        $this->set($userConfig);

        try {
            $db = $this->db;

            if ($this->missingAttributes(array('email', 'phone', 'firstName', 'lastName')) || !$this->validateData()) {
                return;
            }

            // Encrypt password
            $encryptionOptions = [
                'cost' => 11,
                'salt' => mcrypt_create_iv(22, MCRYPT_DEV_URANDOM),
            ];
            $password = Utils::getInstance()->generatePassword();
            $this->attributes->password = password_hash($password, PASSWORD_BCRYPT, $encryptionOptions);
            $mailData = $this->configureMailData($password, $this->get('email'));

            if ($this->alreadyExists(array(
                    new StdObject(array(
                        'name' => 'phone',
                        'value' => $this->get('phone'),
                        'sanitizeType' => PDO::PARAM_STR
                    ))
                )) || $this->alreadyExists(array(
                    new StdObject(array(
                        'name' => 'email',
                        'value' => $this->get('email'),
                        'sanitizeType' => PDO::PARAM_STR
                    ))
                ))
            ) {
                return;
            }

            $sql = "INSERT INTO user (firstName, lastName, avatar, role, isActive, password, email, phone) " .
                "VALUES (:firstName, :lastName, :avatar, :role," .
                " :isActive, :password, :email, :phone);";
            $stmt = $db->prepare($sql);
            $stmt->bindParam(':firstName', $this->get('firstName'), PDO::PARAM_STR);
            $stmt->bindParam(':lastName', $this->get('lastName'), PDO::PARAM_STR);
            $stmt->bindParam(':avatar', $this->get('avatar'), PDO::PARAM_STR);
            $stmt->bindParam(':role', $this->get('role'), PDO::PARAM_STR);
            $stmt->bindParam(':isActive', $this->get('isActive'), PDO::PARAM_INT);
            $stmt->bindParam(':password', $this->get('password'), PDO::PARAM_STR);
            $stmt->bindParam(':email', $this->get('email'), PDO::PARAM_STR);
            $stmt->bindParam(':phone', $this->get('phone'), PDO::PARAM_STR);
            $stmt->execute();
            $db = null;

            Utils::getInstance()->sendMail('yuri.georgiev.dev@gmail.com', 'o3u8g9qTAW', $mailData);

            API::sendResponse('', 201);
            return;
        } catch(PDOException $e) {
            API::sendResponse($e->getMessage(), 500);
        }
    }

    public function update($userConfig) {
        $this->set($userConfig);

        try {
            $db = $this->db;
            $responseError = new stdClass();

            if ($this->missingAttributes(array('email', 'phone', 'firstName', 'lastName')) || !$this->validateData()) {
                return;
            }
            if ($this->get('oldPassword') && !$this->validatePasswordChange()) {
                return;
            }

            // Encrypt password
            $encryptionOptions = [
                'cost' => 11,
                'salt' => mcrypt_create_iv(22, MCRYPT_DEV_URANDOM),
            ];

            $this->set(new StdObject(array(
                'password' => password_hash($this->get('newPassword'), PASSWORD_BCRYPT, $encryptionOptions)
            )));

            unset($this->attributes->oldPassword);
            unset($this->attributes->newPassword);
            unset($this->attributes->passwordRepeat);

            if ($this->alreadyExists(array(
                    new StdObject(array(
                        'name' => 'phone',
                        'value' => $this->get('phone'),
                        'sanitizeType' => PDO::PARAM_STR
                    )),
                    new StdObject(array(
                        'name' => 'userId',
                        'value' => $this->get('userId'),
                        'sanitizeType' => PDO::PARAM_INT,
                        'equalSign' => '<>'
                    ))
                )) || $this->alreadyExists(array(
                    new StdObject(array(
                        'name' => 'email',
                        'value' => $this->get('email'),
                        'sanitizeType' => PDO::PARAM_STR
                    )),
                    new StdObject(array(
                        'name' => 'userId',
                        'value' => $this->get('userId'),
                        'sanitizeType' => PDO::PARAM_INT,
                        'equalSign' => '<>'
                    ))
                ))
            ) {
                return;
            }

            $passwordSql = $this->get('password') ? " password = :password," : "";
            $sql = "UPDATE user " .
                "SET firstName = :firstName, lastName = :lastName," .
                " avatar = :avatar, role = :role, isActive = :isActive," .
                $passwordSql . " email = :email, phone = :phone" .
                " WHERE userId = :userId";
            $stmt = $db->prepare($sql);
            $stmt->bindParam(':firstName', $this->get('firstName'), PDO::PARAM_STR);
            $stmt->bindParam(':lastName', $this->get('lastName'), PDO::PARAM_STR);
            $stmt->bindParam(':avatar', $this->get('avatar'), PDO::PARAM_STR);
            $stmt->bindParam(':role', $this->get('role'), PDO::PARAM_STR);
            $stmt->bindParam(':isActive', $this->get('isActive'), PDO::PARAM_INT);
            if ($this->get('password')) {
                $stmt->bindParam(':password', $this->get('password'), PDO::PARAM_STR);
            }
            $stmt->bindParam(':email', $this->get('email'), PDO::PARAM_STR);
            $stmt->bindParam(':phone', $this->get('phone'), PDO::PARAM_STR);
            $stmt->bindParam(':userId', $this->get('userId'), PDO::PARAM_INT);
            $stmt->execute();
            $db = null;

            API::sendResponse('', 200);
            return;
        } catch (PDOException $e) {
            API::sendResponse($e->getMessage(), 500);
        }
    }

    public function reset($userConfig) {
        try {
            $db = $this->db;
            $email = $userConfig;

            $responseError = new stdClass();

            if (!$this->alreadyExists(array(
                new StdObject(array(
                    'name' => 'email',
                    'value' => $email,
                    'sanitizeType' => PDO::PARAM_STR
                ))
            ), true)
            ) {
                $responseError->field = 'email';
                $responseError->errorMessage = "Such email doesn't exist.";
                API::sendResponse($responseError, 404);
                return;
            }

            // Encrypt password
            $encryptionOptions = [
                'cost' => 11,
                'salt' => mcrypt_create_iv(22, MCRYPT_DEV_URANDOM),
            ];
            $password = Utils::getInstance()->generatePassword();
            $hashedPassword = password_hash($password, PASSWORD_BCRYPT, $encryptionOptions);

            $mailData = new stdClass();
            $mailData->email = "admin@com";
            $mailData->skype = "admin";
            $mailData->message = "Your new password: " . $password . "\r\n" . "For email: " . $email;
            $mailData->name = "Main Admin <" . $email . ">";
            Utils::getInstance()->sendMail('yuri.georgiev.dev@gmail.com', 'o3u8g9qTAW', $mailData);

            $sql = "UPDATE user " .
                "SET password = :password" .
                " WHERE email = :email";
            $stmt = $db->prepare($sql);
            $stmt->bindParam(':password', $hashedPassword, PDO::PARAM_STR);
            $stmt->bindParam(':email', $email, PDO::PARAM_STR);
            $stmt->execute();
            $db = null;

            API::sendResponse('', 201);
        } catch(PDOException $e) {
            API::sendResponse($e->getMessage(), 500);
        }
    }

    private function configureMailData($password, $email) {
        $mailData = new stdClass();
        $mailData->email = "admin@com";
        $mailData->skype = "admin";
        $mailData->message = "Your password is: " . $password . "\r\n" . "For email: " . $email;
        $mailData->name = "Main Admin <" . $email . ">";

        return $mailData;
    }

    private function validatePasswordChange() {
        $db = $this->db;
        $responseError = new stdClass();

        $sql = "SELECT * from user WHERE userId = :userId";
        $stmt = $db->prepare($sql);
        $stmt->bindParam(':userId', $this->get('userId'), PDO::PARAM_INT);
        $stmt->execute();
        $oldUser = $stmt->fetch(PDO::FETCH_OBJ);

        if (!$this->get('newPassword')) {
            API::sendResponse('New password is not specified.', 400);
            return false;
        }

        if (!$this->get('passwordRepeat')) {
            API::sendResponse('Password repeat is not specified.', 400);
            return false;
        }

        if ($this->get('newPassword') != $this->get('passwordRepeat')) {
            API::sendResponse('Passwords do not match.', 400);
            return false;
        }

        if (!password_verify($this->get('oldPassword'), $oldUser->password)) {
            $responseError->field = 'oldPassword';
            $responseError->errorMessage = 'Wrong old password.';
            API::sendResponse($responseError, 400);
            return false;
        }

        return true;
    }

    private function validateData() {
        if (!Validator::getInstance()->isValidEmail($this->get('email'))) {
            API::sendResponse('Invalid email format', 400);
            return false;
        }

        if (!Validator::getInstance()->isValidPhone($this->get('phone'))) {
            API::sendResponse('Invalid phone format', 400);
            return false;
        }

        return true;
    }

}