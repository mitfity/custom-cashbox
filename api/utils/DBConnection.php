<?php

/**
 * Created by PhpStorm.
 * User: yurii
 * Date: 02.09.15
 * Time: 15:23
 */

class DBConnection
{
    private $dbhost;
    private $dbuser;
    private $dbpass;
    private $dbname;

    private static $instance;

    private $connection;

    /**
     * Returns the *Singleton* instance of this class.
     */
    public static function getInstance()
    {
        if (null === static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    /**
     * Protected constructor to prevent creating a new instance of the
     * *Singleton* via the `new` operator from outside of this class.
     */
    protected function __construct() {
        $this->processConfig();
    }

    /**
     * Private clone method to prevent cloning of the instance of the
     * *Singleton* instance.
     */
    private function __clone()
    {
    }

    /**
     * Private unserialize method to prevent unserializing of the *Singleton*
     * instance.
     */
    private function __wakeup()
    {
    }

    /**
     * Gets configuration file from /../configs/db.php and configures db connection data by its content.
     */
    private function processConfig() {
        $configs = include(__DIR__ . '/../configs/db.php');
        $this->dbhost = $configs['host'];
        $this->dbuser = $configs['userName'];
        $this->dbpass = $configs['password'];
        $this->dbname = $configs['dbName'];
    }

    public function getConnection() {
        if ($this->connection === null) {
            $this->connection = new PDO("mysql:host=$this->dbhost; dbname=$this->dbname", $this->dbuser, $this->dbpass);
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }

        return $this->connection;
    }

}