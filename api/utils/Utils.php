<?php

require_once 'swiftmailer/lib/swift_required.php';

/**
 * Created by PhpStorm.
 * User: yurii
 * Date: 20.08.15
 * Time: 17:55
 */
class Utils
{

    private static $instance;

    /**
     * Returns the *Singleton* instance of this class.
     */
    public static function getInstance()
    {
        if (null === static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    /**
     * Protected constructor to prevent creating a new instance of the
     * *Singleton* via the `new` operator from outside of this class.
     */
    protected function __construct()
    {
    }

    /**
     * Private clone method to prevent cloning of the instance of the
     * *Singleton* instance.
     */
    private function __clone()
    {
    }

    /**
     * Private unserialize method to prevent unserializing of the *Singleton*
     * instance.
     */
    private function __wakeup()
    {
    }

    public function generatePassword( $length = 8 ) {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $password = substr( str_shuffle( $chars ), 0, $length );
        return $password;
    }

    public function sendMail($email, $passForEmail, $mailData) {
        // Create message
        $message =
            "You have feedback from " .
            $mailData->name . " (email: " . $mailData->email . " skype: " . $mailData->skype . ") \n\n" .
            $mailData->message;

        // Create the Transport
        $transport = Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, "ssl")
            ->setUsername($email)
            ->setPassword($passForEmail);

        // Create the Mailer using your created Transport
        $mailer = Swift_Mailer::newInstance($transport);

        // Create a message
        $message = Swift_Message::newInstance('Password recovery') // mail theme
            ->setFrom(array($mailData->email => $mailData->name))
            ->setTo(array('yuri.georgiev.dev@mail.ru', 'swordArtOnline@gmail.com' => 'Info'))
            ->setBody($message);

        $mail = $mailer->send($message);

        return $mail;
    }

}

?>