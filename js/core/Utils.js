/**
 * Global utilities for usage,
 * */
CJ.core.utils = {

    /**
     * Fills number with leading zeros.
     * @param {Number} num Number to be filled with zeros.
     * @param {Number} size Total number of digits.
     * @return {String} str The string containing number, filled with leading zeros.
     * */
    paddingWithZeros: function (num, size) {
        var str = num+"";

        while (str.length) {
            if (str[0] === '0') {
                str = str.substr(1);
            }
            else {
                break;
            }
        }

        while (str.length < size) {
            str = "0" + str;
        }

        return str;
    }

};

