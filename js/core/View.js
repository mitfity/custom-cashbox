/**
 * Created by yurii on 30.06.15.
 */

/**
 * This is default view class. It describes default view behaviour.
 * @extends Backbone.View
 * */
CJ.core.View = Backbone.View.extend({

    /**
     * @property {String} classType Type of class. Is needed to recognize backbone models/views etc.
     * */
    classType: 'CoreView',

    /**
     * @property {Object} el Jquery object of this view.
     * */
    el: null,

    /**
     * @cfg {String} renderTo The selector in which view will be rendered.
     * */
    renderTo: 'body',

    /**
     * @cfg {Function} tpl Underscore template function, that contains html template.
     * */
    tpl: _.template('<div></div>'),

    /**
     * @override Backbone.View
     * */
    initialize: function (cfg) {
        $.extend(this, cfg);

        this.render();
    },

    /**
     * @override Backbone.View
     * */
    render: function () {
        this.el = $( this.tpl(this.getTplData()) ).appendTo(this.renderTo);
    },

    /**
     * Specify data passed to tpl.
     * @return {Object} Object Data passed to template.
     * */
    getTplData: function () {
        return {};
    },

    /**
     * Added jquery element remove.
     * @override Backbone.View
     * */
    remove: function () {
        this.onRemove();
        this.unbind();
        this.el.remove();
        Backbone.View.prototype.remove.apply(this, arguments);
    },

    onRemove: function () {
    }

});