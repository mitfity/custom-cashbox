/**
 * Created by yurii on 09.07.15.
 */

/**
 * Collections manager. Holds collections.
 * */
CJ.core.collections.Collections = function () {
    this.initialize();
};

// todo associative array
CJ.core.collections.Collections.prototype = {

    /* Begin Definitions */

    usersCollection: null,

    invoicesCollection: null,

    clientsCollection: null,

    productsCollection: null,

    /* End Definitions */

    initialize: function (cfg) {
        $.extend(this, cfg);
    },

    /**
     * Returns fetched collection of specific type,
     * @param {String} collectionName Name of collection.
     * @param {function} callback Callback function after fetch success.
     * @return {Object} collection Specific collection.
     * */
    getCollection: function (collectionName, callback) {
        var collection = null;

        if (collectionName === 'users') {
            collection = this.getUsersCollection();
            this.usersCollection = collection;
        }
        else if (collectionName === 'invoices') {
            collection = this.getInvoicesCollection();
            this.invoicesCollection = collection;
        }
        else if (collectionName === 'clients') {
            collection = this.getClientsCollection();
            this.clientsCollection = collection;
        }
        else if (collectionName === 'products') {
            collection = this.getProductsCollection();
            this.productsCollection = collection;
        }

        if (collection) {
            collection.fetch({
                reset: true,
                success: function (model, response) {
                    if (callback) {
                        callback(collection);
                    }
                },
                error: function (model, error) {
                    console.log(error.responseText);
                }
            });
        }

        return collection;
    },

    /**
     * @return {Object} Users collection.
     * */
    getUsersCollection: function () {
        return this.usersCollection || new CJ.submodules.user.Collection();
    },

    /**
     * @return {Object} Invoices collection.
     * */
    getInvoicesCollection: function () {
        return this.invoicesCollection || new CJ.submodules.invoice.Collection();
    },

    /**
     * @return {Object} Clients collection.
     * */
    getClientsCollection: function () {
        return this.clientsCollection || new CJ.submodules.client.Collection();
    },

    /**
     * @return {Object} Products collection.
     * */
    getProductsCollection: function () {
        return this.productsCollection || new CJ.submodules.product.Collection();
    }

};