/**
 * Created by yurii on 13.07.15.
 */

CJ.core.configs.jqueryValidationConfig = {

    /**
     * Custom ui jquery validator methods.
     */
    initialize: function () {

        $.validator.addMethod('phone', function (value, element) {
            return this.optional(element) || /^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{1,100}$/.test(value);
        }, 'Please enter a valid phone number');

        $.validator.addMethod('zip', function (value, element) {
            return this.optional(element) || /^\s*\d{5}\s*$/.test(value);
        }, 'Please enter a valid zip number');

        $.validator.addMethod('realDate', function (value, element) {
            return this.optional(element) || /^((0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01])[- /.](19|20)?[0-9]{2})*$/.test(value);
        }, 'Please enter a valid date number');

        /**
         * Makes element validate itself on characters input. By default after submit, if input field was validated
         * and marked as valid, then it will not be validated on input/focusOut and if u need to do something then u
         * can't. This validation rule solves this problem.
         */
        $.validator.addMethod('reValidate', function (value, element) {
            var field = $(element);

            if (!field.data('reValidationInit')) {
                field.on('input propertychange', function (e) {
                    field.valid();
                });
                field.data('reValidationInit', true);
            }

            return true;
        }, '');

        $.validator.addMethod("require_from_group", function(value, element, options) {
            var validator = this;
            var selector = options[1];
            var validOrNot = $(selector, element.form).filter(function() {
                    return validator.elementValue(this);
                }).length >= options[0];

            if(!$(element).data('being_validated')) {
                var fields = $(selector, element.form);
                fields.data('being_validated', true);
                fields.valid();
                fields.data('being_validated', false);
            }

            return !!value;
        }, '');

        /**
         * This rule makes fields with this rule to become require_from_group if requiredGroupFlagElement is not empty.
         * Options are: {
         *      className {String} Name of requiredGroup
         *      flagElementClassName {String} Name of flag element
         * }.
         */
        $.validator.addMethod('requiredGroup', function (value, element, options) {
            var relationInputs = $('input.' + options.className),
                isEmpty = !value,
                areRelationInputsEmpty = true,
                flagElement = $('.' + options.flagElementClassName, element.form);

            relationInputs = $.grep(relationInputs, function (input, index) {
                return input !== element;
            });

            relationInputs.forEach(function (input, index, coll) {
                if (input.value) {
                    areRelationInputsEmpty = false;
                    return true;
                }
            });

            if (!isEmpty) {
                if (areRelationInputsEmpty) {
                    relationInputs.forEach(function (input, index, coll) {
                        $(input).rules('add', {
                            require_from_group: [5, '.' + options.className]
                        });

                        $('label[for=\"'+$(input).attr('id')+'\"].inputLabel').addClass('required');
                    });
                }

                $(element).rules('add', {
                    require_from_group: [5, '.' + options.className]
                });

                $('label[for=\"'+$(element).attr('id')+'\"].inputLabel').addClass('required');

                return true;
            }
            else {
                if (areRelationInputsEmpty && !flagElement.val()) {
                    relationInputs.forEach(function (input, index, coll) {
                        $(input).rules('remove', 'require_from_group');
                        $('label[for=\"'+$(input).attr('id')+'\"].inputLabel').removeClass('required');
                    });

                    $(element).rules('remove', 'require_from_group');
                    $('label[for=\"'+$(element).attr('id')+'\"].inputLabel').removeClass('required');

                    return true;
                }
                else {
                    return false;
                }


            }
        }, 'This field is required.');

        /**
         * Element on which requiredGroup will depend.
         */
        $.validator.addMethod('requiredGroupFlagElement', function (value, element, groupClassName) {
            var relationInputs = $('input.' + groupClassName),
                areRelationInputsEmpty = true;

            relationInputs.each(function (index, input) {
                if ($(input).val()) {
                    areRelationInputsEmpty = false;
                }

                $(input).rules('add', {
                    require_from_group: [5, '.' + groupClassName]
                });

                $('label[for=\"'+$(input).attr('id')+'\"].inputLabel').addClass('required');
            });

            if (!value && areRelationInputsEmpty) {
                relationInputs.each(function (index, input) {
                    $(input).rules('remove', 'require_from_group');
                    $('label[for=\"'+$(input).attr('id')+'\"].inputLabel').removeClass('required');
                    $(input).valid();
                });
            }

            return true;
        }, '');
    }

};

CJ.core.configs.jqueryValidationConfig.initialize();