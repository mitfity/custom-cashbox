/**
 * Created by yurii on 21.07.15.
 */

CJ.core.configs.slickgridValidationConfig = {

    /**
     * Custom slickgrid editors/formatters/validators.
     */
    initialize: function () {

        this.requiredFieldValidator = function (value) {
            if (value === null || value === undefined || !value.length) {
                return {
                    valid: false,
                    msg: "This is a required field"
                };
            }
            else {
                return {
                    valid: true,
                    msg: null
                };
            }
        };

        this.htmlFormatter = function (row, cell, value, columnDef, dataContext) {
            return value;
        };

        this.requiredFieldFormatter = function (row, cell, value, columnDef, dataContext) {
            if (!dataContext._invalid) {
                dataContext._invalid = [];
            }

            if (value) {
                dataContext._invalid = $.grep(dataContext._invalid, function (element) {
                    return element.cellIndex !== cell;
                });
                return "<span class='validInput'>" + value + "</span>";
            }

            if ($.grep(dataContext._invalid, function (element) {
                    return element.cellIndex === cell;
                }).length === 0) {
                dataContext._invalid.push({cellIndex: cell});
            }
            return "<span class='invalidInput' title='This field is required.'>" + value +
                "<span class='glyphicon glyphicon-exclamation-sign'></span></span>";
        };

        this.requiredNameFormatter = function (row, cell, value, columnDef, dataContext) {
            if (!dataContext._invalid) {
                dataContext._invalid = [];
            }

            if (value || !dataContext.description) {
                dataContext._invalid = $.grep(dataContext._invalid, function (element) {
                    return element.cellIndex !== cell;
                });
                return "<span class='validInput'>" + value + "</span>";
            }

            if ($.grep(dataContext._invalid, function (element) {
                    return element.cellIndex === cell;
                }).length === 0) {
                dataContext._invalid.push({cellIndex: cell});
            }
            return "<span class='invalidInput' title='This field is required.'>" + value +
                "<span class='glyphicon glyphicon-exclamation-sign'></span></span>";
        };

        var validateNumber = function (cell, value, dataContext) {
            if (!dataContext._invalid) {
                dataContext._invalid = [];
            }

            if (!value && value === undefined) {
                if ($.grep(dataContext._invalid, function (element) {
                        return element.cellIndex === cell;
                    }).length === 0) {
                    dataContext._invalid.push({ cellIndex: cell });
                }
                value = "<span class='invalidInput' title='This field is required.'>" + value +
                    "<span class='glyphicon glyphicon-exclamation-sign'></span></span>";
            }
            else if (!( !isNaN(parseFloat(value)) && isFinite(value) )) {
                if ($.grep(dataContext._invalid, function (element) {
                        return element.cellIndex === cell;
                    }).length === 0) {
                    dataContext._invalid.push({ cellIndex: cell });
                }
                value = "<span class='invalidInput' title='Has to be a number.'>NaN" +
                    "<span class='glyphicon glyphicon-exclamation-sign'></span></span>";
            }
            else {
                dataContext._invalid = $.grep(dataContext._invalid, function (element) {
                    return element.cellIndex !== cell;
                });
            }

            return {
                value: value,
                dataContext: dataContext
            };
        };

        this.numberFormatter = function (row, cell, value, columnDef, dataContext) {
            if (!dataContext._invalid) {
                dataContext._invalid = [];
            }

            var validatedNumber = validateNumber(cell, value, dataContext);

            if (validatedNumber.dataContext._invalid) {
                return validatedNumber.value;
            }

            dataContext._invalid = $.grep(dataContext._invalid, function (element) {
                return element.cellIndex !== cell;
            });
            return "<span class='validInput'>" + value.toFixed(2) + "</span>";
        };

        this.priceFormatter = function (row, cell, value, columnDef, dataContext) {
            if (!dataContext._invalid) {
                dataContext._invalid = [];
            }

            var validatedNumber = validateNumber(cell, value, dataContext);

            if (validatedNumber.dataContext._invalid) {
                return validatedNumber.value;
            }

            dataContext._invalid = $.grep(dataContext._invalid, function (element) {
                return element.cellIndex !== cell;
            });
            return "<span class='validInput'>$" + value.toFixed(2) + "</span>";
        };

        this.percentageFormatter = function (row, cell, value, columnDef, dataContext) {
            if (!dataContext._invalid) {
                dataContext._invalid = [];
            }

            var validatedNumber = validateNumber(cell, value, dataContext);

            if (validatedNumber.dataContext._invalid) {
                return validatedNumber.value;
            }

            dataContext._invalid = $.grep(dataContext._invalid, function (element) {
                return element.cellIndex !== cell;
            });
            return "<span class='validInput'>" + value.toFixed(2) + "%</span>";
        };

        this.positiveFloatEditor = function (args) {
            var $input;
            var defaultValue;
            var scope = this;

            this.init = function () {
                $input = $("<INPUT type=text class='editor-text' />");

                $input.bind("keydown", function (e) {
                    if (e.shiftKey) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                    }

                    var value = $input.val(),
                        isNumber = e.keyCode >= 48 && e.keyCode <= 57,
                        dotPos = value.indexOf("."),
                        isDot = (e.keyCode === 190 && dotPos === -1),
                        isControlButton = (e.keyCode < 48 || (e.keyCode > 90 && e.keyCode < 106));

                    if (!(isNumber || isDot || isControlButton)) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                    }
                    if (e.keyCode === $.ui.keyCode.LEFT || e.keyCode === $.ui.keyCode.RIGHT) {
                        e.stopImmediatePropagation();
                    }
                });

                $input.appendTo(args.container);
                $input.focus().select();
            };

            this.destroy = function () {
                $input.remove();
            };

            this.focus = function () {
                $input.focus();
            };

            this.loadValue = function (item) {
                defaultValue = item[args.column.field];
                $input.val(defaultValue);
                $input[0].defaultValue = defaultValue;
                $input.select();
            };

            this.serializeValue = function () {
                var val = +$input.val();
                if (val > 0) {
                    return val;
                }
                return 0;
            };

            this.applyValue = function (item, state) {
                item[args.column.field] = state;
            };

            this.isValueChanged = function () {
                return (!($input.val() === "" && defaultValue === null)) && ($input.val() !== defaultValue);
            };

            this.validate = function () {
                return {
                    valid: true,
                    msg: null
                };
            };

            this.init();
        };

        this.formatterLineTotal = function (row, cell, value, rowConfig, context) {
            if (context.count && context.price && context.tax !== undefined) {
                var sumPrice = (context.count * context.price),
                    total = sumPrice / 100 * (100 + context.tax);

                context.total = total;

                return "$" + total.toFixed(2);
            }
            else {
                context.total = 0;
                return "$" + 0 ;
            }
        };

        this.productsAutocompleteEditor = function (args) {
            var $input,
                defaultValue,
                scope = this,
                item = args.item,
                grid = args.grid,
                cell = grid.getActiveCell(),
                row = cell.row,
                dataView = grid.getData(),
                sameNameProducts;

            this.init = function () {
                $input = $("<INPUT id='tags' class='editor-text' />");
                $input.appendTo(args.container);
                $input.focus().select();
                $input.autocomplete({
                    minLength: 0,
                    source: 'http://cashbox.loc/api/product',
                    select: function (event, ui) {
                        $input.val(ui.item.productName);
                        item.name = ui.item.name;
                        item.description = ui.item.description;
                        item.price = ui.item.price;
                        item.tax = ui.item.tax || 10;

                        if (item.id === undefined) {
                            item.id = 'product' + grid.getData().getLength();
                            item.count = 1;
                            item.buttons = '<div class="removeItem"><span class="glyphicon glyphicon-trash' +
                            ' "></span></div>';

                            dataView.addItem(item);
                            return false;
                        }

                        dataView.updateItem(item.id, item);

                        return false;
                    }.bind(this)
                })
                    .autocomplete("instance")._renderItem = function (ul, item) {
                    return $("<li>")
                        .append("<a>" + item.name + " $" + item.price + "</a>")
                        .appendTo(ul);
                };
            };

            this.destroy = function () {
                $input.autocomplete("destroy");
            };

            this.focus = function () {
                $input.focus();
            };

            this.loadValue = function (item) {
                defaultValue = item[args.column.field];
                $input.val(defaultValue);
                $input[0].defaultValue = defaultValue;
                $input.select();
            };

            this.serializeValue = function () {
                return $input.val();
            };

            this.applyValue = function (item, state) {
                item[args.column.field] = state;
            };

            this.isValueChanged = function () {
                return (!($input.val() === "" && defaultValue === null)) && ($input.val() !== defaultValue);
            };

            this.validate = function () {
                return {
                    valid: true,
                    msg: null
                };
            };

            this.init();
        };

    }

};

CJ.core.configs.slickgridValidationConfig.initialize();