/**
 * Created by mitfity on 30.06.2015.
 */

// create the root namespace and make sure we're not overwriting it
var CJ = CJ || {};

/**
 * Creates namespace in CJ global namespace.
 *
 * @param {String} namespace Namespace string name.
 * @return {Object} parent Returns created namespace (Javascript object).
 * */
CJ.createNamespace = function (namespace) {
    var namespaceParts = namespace.split("."),
        parent = CJ;

    // we want to be able to include or exclude the root namespace
    // So we strip it if it's in the namespace
    if (namespaceParts[0] === "CJ") {
        namespaceParts = namespaceParts.slice(1);
    }

    // loop through the parts and create
    // a nested namespace if necessary
    for (var i = 0; i < namespaceParts.length; i++) {
        var partName = namespaceParts[i];
        // check if the current parent already has
        // the namespace declared, if not create it
        if (typeof parent[partName] === "undefined") {
            parent[partName] = {};
        }
        // get a reference to the deepest element
        // in the hierarchy so far
        parent = parent[partName];
    }
    // the parent is now completely constructed
    // with empty namespaces and can be used.
    return parent;
};

CJ.createNamespace('CJ.core');