/**
 * Created by yurii on 09.07.15.
 */

/**
 * This is application class. Here application starts, The application is about users, clients, invoices... how they
 * work together.
 * */
CJ.core.etudeCashboxApp.EtudeCashboxApp = function () {
    this.initialize();
};

CJ.core.etudeCashboxApp.EtudeCashboxApp.prototype = {

    /**
     * @property {Object} CJ.core.View
     * */
    currentView: null,

    /**
     * @property {Object} CJ.core.collections.Collections
     * */
    collections: null,

    /**
     * @property {Object} CJ.core.userIdentity.UserIdentity
     * */
    userIdentity: null,

    /**
     * @property {Object} CJ.core.router.Router
     * */
    router: null,

    /**
     * @property {String} The route, where user will be redirected if he is logged in.
     * */
    privateRoute: 'users/list',

    /**
     * @property {String} The route, where user will be redirected if he is not logged in.
     * */
    publicRoute: 'login',

    initialize: function (cfg) {
        $.extend(this, cfg);

        this.collections = CJ.core.collections.instance = new CJ.core.collections.Collections();
        this.userIdentity = new CJ.core.userIdentity.UserIdentity();
        this.userIdentity.downloadSession(function () {
            this.router = new CJ.core.router.Router();

            setTimeout(function () {
                Backbone.history.start();
            }, 16);

            this.initPopupsCentering();
        }.bind(this));
    },

    /**
     * Inits events on which modal windows will be centered.
     * */
    initPopupsCentering: function () {
        $(function () {
            function centerModal() {
                $(this).css('display', 'block');
                var $dialog = $(this).find('.modal-dialog'),
                    offset = ($(window).height() - $dialog.height()) / 2,
                    bottomMargin = parseInt($dialog.css('marginBottom'), 10);

                if(offset < bottomMargin) {
                    offset = bottomMargin;
                }

                $dialog.css('margin-top', offset);
            }

            $(document).on('show.bs.modal', '.modal', centerModal);
            $(window).on('resize', function () {
                $('.modal:visible').each(centerModal);
            });
        });
    },

    /**
     * @param {Object} classPrototype View class prototype to be created.
     * */
    setCurrentView: function (classPrototype) {
        if (this.currentView) {
            if (this.currentView.classType === classPrototype.prototype.classType) {
                return;
            }

            this.currentView.remove();
        }

        this.currentView = new classPrototype();
    },

    /**
     * Redirects to private route if user is login and to public if not.
     * */
    redirectToDefaultRoute: function () {
        if (this.userIdentity.isLoggedIn()) {
            this.router.navigate(this.privateRoute, {
                trigger: true
            });
            return;
        }

        this.router.navigate(this.publicRoute, {
            trigger: true
        });
    }

};