/**
 * Created by yurii on 06.07.15.
 */

CJ.core.form.Widget = CJ.core.View.extend({

    classType: 'FormWidget',

    /**
     * Jquery form object.
     */
    form: null,

    /**
     * Jquery validator.
     */
    validator: null,

    initialize: function () {
        CJ.core.View.prototype.initialize.apply(this, arguments);
        this.form = this.getForm();
        this.initValidation();
        this.initEvents();
    },

    /**
     * You have to override this method to return DOM form element or jquery form element.
     * Factory method.
     */
    getForm: function () {
    },

    /**
     * Initializes this.validator.
     */
    initValidation: function () {
        var validationOptions = this.getValidationOptions();

        _.extend(validationOptions, {
            onkeyup: function (element, e) {
                // Needed in case to show custom error on enter pressed. Bug was : error shows on enter down and
                // hides on enter up.
                if (e.keyCode === 13) {
                    if (this.lastElement !== element) {
                        this.lastElement = element;
                    }
                    return;
                }

                $.validator.defaults.onkeyup.apply(this, arguments);
            },

            errorClass: "invalid",

            validClass: "success"
        });

        if (!this.form) {
            throw new Error('initValidation: No form found. Need to specify form as a result of getForm method.');

        }
        this.validator = $(this.form).validate(validationOptions);
    },

    initEvents: function () {
        this.form.submit(function (e) {
            e.preventDefault();
            e.stopImmediatePropagation();

            this.onFormSubmit.apply(this, arguments);
            return false;
        }.bind(this));
    },

    /**
     * @return {Object} Options for validator (rules, messages...).
     */
    getValidationOptions: function () {
        return {};
    },

    /**
     * It will be called when form submits. Default submit event is stopped before calling this method.
     * @param e
     */
    onFormSubmit: function (e) {
    },

    /**
     * @returns {Object} JSON object of form input values. Uses jquery serializeArray method.
     */
    getValues: function () {
        var values = {};

        $(this.form).serializeArray().forEach(function (el, index, col) {
            values[el.name] = el.value;
        });

        return values;
    },

    /**
     * Sets form values by inputs names.
     * @param {Object} formValues
     */
    setValues: function (formValues) {
        for (var formValue in formValues) {
            if (formValues.hasOwnProperty(formValue)) {

                var element = $( 'input[name=\"' + formValue + '\"]', this.form );

                if (element) {
                    element.val(formValues[formValue]);
                }
            }
        }
    },

    /**
     * @returns {boolean} True if form is empty,
     */
    isEmpty: function () {
        var empty = true;

        $(':input', this.form).each(function() {
            if($(this).val() !== '') {
                empty = false;
            }
        });

        return empty;
    },

    /**
     * @returns {boolean} True if form inputs are valid.
     */
    isValid: function () {
        var formInputs = $('input', this.form),
            isValidForm = true;

        formInputs.each(function (index, input) {
            if (!$(input).valid()) {
                isValidForm = false;
            }
        });

        return isValidForm;
    },

    disableSubmitOnEnter: function () {
        this.form.on('keyup keypress', function(e) {
            var code = e.keyCode || e.which;
            if (code == 13) {
                e.preventDefault();
                return false;
            }
        });
    },

    /**
     * @param {Object} model Editable model.
     */
    setTargetModel: function (model) {
        this.targetModel = model;
        this.currentModel.set(this.targetModel.attributes);
    }

});