/**
 * Created by yurii on 30.06.15.
 */

CJ.core.grid.Widget = CJ.core.View.extend( {

    classType: 'GridWidget',

    /**
     * @property {Object} Slick.Grid
     */
    grid: null,

    /**
     * @property {Object} Slick.Data.DataView
     */
    gridDataView: null,

    initialize: function (cfg) {
        this.tpl = JST['js/core/grid/Grid.tpl.html'];
        CJ.core.View.prototype.initialize.apply(this, arguments);

        this.tplOptions = this.tplOptions || {};
        this.gridColumns = this.gridColumns || {};
        this.gridOptions = this.gridOptions || {};
        this.gridData = this.gridData || {};
        this.dataCollection = this.dataCollection || {};

        this.initGrid();
        this.initEvents();
    },

    initEvents: function () {
        var $viewPort = $('.viewport'),
            lastHeight = $viewPort.css('height'),
            timeOutBuff = null;
            checkForChanges = function () {
                if (!this.grid) {
                    clearTimeout(timeOutBuff);
                    return;
                }

                if ($viewPort.css('height') != lastHeight) {
                    this.grid.resizeCanvas();
                    lastHeight = $viewPort.css('height');
                }

                timeOutBuff = setTimeout(checkForChanges, 500);
            }.bind(this);
        checkForChanges();

        $viewPort.on('resize', this.onContainerResized.bind(this));
        $('.slick-viewport', this.el).on('blur', 'input.editor-text', function (e) {
            window.setTimeout(function(){
                Slick.GlobalEditorLock.commitCurrentEdit();
            },  0);
        });
    },

    initGrid: function () {
        this.gridDataView = new Slick.Data.DataView();

        var columns = this.gridColumns,
            options = this.gridOptions,
            data = this.gridData,
            gridContainer = this.renderTo + ' .renderGrid',
            dataView = this.gridDataView;


        options.rowHeight = 42;
        options.headerRowHeight = 42;
        options.syncColumnCellResize = true;
        options.enableTextSelectionOnCells = true;

        this.grid = new Slick.Grid(gridContainer, dataView, columns, options);
        this.grid.setSelectionModel(Slick.RowSelectionModel);

        this.grid.onCellChange.subscribe(function (e, args) {
            dataView.updateItem(args.item.id, args.item);
            this.onGridCellChange(e, args);
        }.bind(this));

        this.grid.onAddNewRow.subscribe(function (e, args) {
            this.onGridAddNewRow(e, args);
            this.validateRow(args.grid, args.grid.getActiveCell().row);
        }.bind(this));

        // Customize data view
        // wire up model events to drive the grid
        dataView.onRowCountChanged.subscribe(function (e, args) {
            this.grid.updateRowCount();
            this.grid.render();
        }.bind(this));

        dataView.onRowsChanged.subscribe(function (e, args) {
            this.grid.invalidateRows(args.rows);
            this.grid.render();
            this.onDataviewRowsChanged(e, args);
        }.bind(this));

        dataView.getItemMetadata = this.rowMetadata.bind(this);

        this.setDataViewItems(data, "id");
    },

    /**
     * @param {Event} e
     * @param {Object} args
     */
    onGridCellChange: function (e, args) {
    },

    /**
     * @param {Event} e
     * @param {Object} args
     */
    onGridAddNewRow: function (e, args) {
    },

    /**
     * @param {Event} e
     * @param {Object} args
     */
    onDataviewRowsChanged: function (e, args) {
    },

    /**
     * @param {Event} e
     */
    onContainerResized: function (e) {
        this.grid.resizeCanvas();
    },

    /**
     * @param {Array} columns
     */
    setColumnsResizable: function (columns) {
        columns.forEach(function (el, index, coll) {
            el.resizable = true;
        });

        this.grid.setColumns(columns);
    },

    /**
     * @param {Object} grid
     * @param {Number} rowIndex
     */
    validateRow: function (grid, rowIndex) {
        $.each(grid.getColumns(), function (columnIndex, column) {
            // iterate through editable cells
            var item = grid.getDataItem(rowIndex);

            if (column.editor && column.validator) {
                var validationResults = column.validator(item[column.field]);
                if (!validationResults.valid) {
                    // show editor
                    grid.gotoCell(rowIndex, columnIndex, true);

                    // validate (it will fail)
                    grid.getEditorLock().commitCurrentEdit();

                    // stop iteration
                    return false;
                }
            }
        });
    },

    /**
     * @private
     */
    rowMetadata: function (row) {
        var item = this.gridDataView.getItem(row);

        if (item && item._inactive) {
            return { cssClasses: 'inactive' };
        }

        return {};
    },

    /**
     * @param {Number} rowIndex
     */
    removeRow: function (rowIndex) {
        var item = this.gridDataView.getItem(rowIndex),
            rowId = item.id;

        this.gridDataView.deleteItem(rowId);
        this.grid.invalidate();
        this.grid.render();
    },

    removeActiveRow: function () {
        this.removeRow(this.grid.getActiveCell().row);
    },

    /**
     * @param {Number} rowIndex
     * @param {Object} data
     * @param {Object} jquery
     */
    setRowData: function (rowIndex, data, jquery) {
        var row = this.grid.getData().getItem(rowIndex);

        for (var property in data) {
            if (data.hasOwnProperty(property)) {
                row[property] = data[property];
            }
        }

        this.setDataViewItems(this.gridData, "id");
        this.grid.invalidateRow(rowIndex);
        this.grid.render();
    },

    /**
     * @param {Array} data
     * @param {String} idString
     */
    setDataViewItems: function (data, idString) {
        if (typeof data[0] === 'object') {
            this.gridDataView.setItems(data, idString);
            return;
        }

        var createdData = [];

        data.forEach(function (item, index, col) {
            createdData.push(new item(this.dataCollection, index, this.dataCollection[index]));
        }.bind(this));

        this.gridDataView.setItems(createdData, idString);
    },

    /**
     * @returns {boolean} True if there are no invalid grid cells.
     */
    isValid: function () {
        var dataView = this.gridDataView,
            rowsCount = dataView.getLength();

        for (var i = 0; i < rowsCount; i++) {
            if (dataView.getItem(i)._invalid.length) {
                return false;
            }
        }

        return true;
    },

    /**
     * @returns {Array} values
     */
    getValues: function () {
        var items = this.gridDataView.getItems(),
            values = [];

        items.forEach(function (el, index, coll) {
            var value = {};
            for (var property in el) {
                if (el.hasOwnProperty(property) && property !== '_invalid' && property !== 'id' &&
                    property !== 'buttons') {

                    value[property] = el[property];
                }
            }
            values.push(value);
        });

        return values;
    },

    /**
     * This method is called before remove(). It is used for removing references.
     * @private
     */
    onRemove: function () {
        $('.viewport').off('resize');
        this.grid.destroy();
        this.grid = null;
    }

});