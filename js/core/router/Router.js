/**
 * Created by yurii on 10.07.15.
 */

/**
 * Applications router. Manages routes.
 */
CJ.core.router.Router = Backbone.Router.extend({

    /**
     * @property {Object} CJ.core.window.Widget
     */
    currentWindow: null,

    /**
     * @override Backbone.Router
     */
    routes: {
        'login': 'login',
        'passwordRecovery': 'passwordRecovery',
        'logout': 'logout',
        'profile': 'profile',
        ':entity/list': 'list',
        ':entity/edit/:id': 'edit',
        ':entity/add': 'add',
        ':entity/remove/:id': 'remove',
        '*path': 'defaultRoute'
    },

    initialize: function () {
        _.extend(this, Backbone.Events);
    },

    /**
     * @override Backbone.Router
     * @param callback
     * @param args
     * @param name
     * @returns {boolean}
     */
    execute: function(callback, args, name) {
        if (CJ.app.userIdentity.isLoggedIn()) {
            if (name === 'login' || name === 'passwordRecovery') {
                CJ.app.redirectToDefaultRoute();
            }

            CJ.app.setCurrentView(CJ.submodules.viewport.Widget);

            if (callback) {
                callback.apply(this, args);
            }
        }
        else {
            if (name === 'login') {
                CJ.app.setCurrentView(CJ.submodules.login.Widget);
            }
            else if (name === 'passwordRecovery') {
                CJ.app.setCurrentView(CJ.submodules.login.passwordRecovery.Widget);
            }
            else {
                this.navigate('login', { trigger: true });
            }
        }

        return false;
    },

    login: function () {
    },

    passwordRecovery: function () {
    },

    /**
     * Logs user out.
     */
    logout: function () {
        CJ.app.userIdentity.logOut(function () {
            CJ.app.redirectToDefaultRoute();
        });
    },

    /**
     * Profile rout handler.
     */
    profile: function () {
        this.currentWindow = new CJ.core.window.formCrudWindow.Widget({
            title: 'User Profile',
            contentClass: CJ.submodules.user.FormWidget,
            contentConfig: {
                modelId: CJ.app.userIdentity.userId,
                isProfile: true
            },
            crudType: 'edit'
        });
        this.currentWindow.show();
    },

    /**
     * List rout handler.
     */
    list: function (entity) {
        this[entity+'List'](entity);
    },

    /**
     * Exact list rout handler.
     */
    usersList: function (entity) {
        this.trigger('viewport.changeContent', { contentPrototype: CJ.submodules.user.ListWidget, contentName: entity });
    },

    /**
     * Exact rout handler.
     */
    clientsList: function (entity) {
        this.trigger('viewport.changeContent', { contentPrototype: CJ.submodules.client.ListWidget, contentName: entity });
    },

    /**
     * Exact rout handler.
     */
    invoicesList: function (entity) {
        this.trigger('viewport.changeContent', { contentPrototype: CJ.submodules.invoice.ListWidget, contentName: entity });
    },

    /**
     * Edit rout handler.
     */
    edit: function (entity, id) {
        id = parseInt(id, 10);

        if (this.currentWindow) {
            this.currentWindow.close();
        }

        this[entity+'Edit'](entity, id);
    },

    /**
     * Exact edit rout handler.
     */
    usersEdit: function (entity, id) {
        this.currentWindow = new CJ.core.window.formCrudWindow.Widget({
            title: 'Edit User',
            contentClass: CJ.submodules.user.FormWidget,
            contentConfig: {
                modelId: id
            },
            crudType: 'edit',
            navigateOnClose: entity + '/list'
        });
        this.currentWindow.show();
    },

    /**
     * Exact edit rout handler.
     */
    clientsEdit: function (entity, id) {
        this.currentWindow = new CJ.core.window.formCrudWindow.Widget({
            title: 'Edit Client',
            contentClass: CJ.submodules.client.FormWidget,
            contentConfig: {
                modelId: id
            },
            crudType: 'edit',
            navigateOnClose: entity + '/list'
        });
        this.currentWindow.show();
    },

    /**
     * Exact edit rout handler.
     */
    invoicesEdit: function (entity, id) {
        this.trigger('viewport.changeContent', {
            contentPrototype: CJ.submodules.invoice.CrudWidget,
            contentName: entity,
            contentTitle: 'Edit Invoice',
            modelId: id
        });
    },

    /**
     * Add rout handler.
     */
    add: function (entity) {
        if (this.currentWindow) {
            this.currentWindow.close();
        }

        this[entity+'Add'](entity);
    },

    /**
     * Exact add rout handler.
     */
    usersAdd: function (entity) {
        this.currentWindow = new CJ.core.window.formCrudWindow.Widget({
            title: 'Add User',
            contentClass: CJ.submodules.user.FormWidget,
            contentConfig: {

            },
            crudType: 'add',
            navigateOnClose: entity + '/list'
        });
        this.currentWindow.show();
    },

    /**
     * Exact add rout handler.
     */
    clientsAdd: function (entity) {
        this.currentWindow = new CJ.core.window.formCrudWindow.Widget({
            title: 'Add Client',
            contentClass: CJ.submodules.client.FormWidget,
            contentConfig: {

            },
            crudType: 'add',
            navigateOnClose: entity + '/list'
        });
        this.currentWindow.show();
    },

    /**
     * Exact add rout handler.
     */
    invoicesAdd: function (entity) {
        this.trigger('viewport.changeContent', {
            contentPrototype: CJ.submodules.invoice.CrudWidget,
            contentName: entity,
            contentTitle: 'Create New Invoice'
        });
    },

    /**
     * Remove rout handler.
     */
    remove: function (entity, id) {
        id = parseInt(id, 10);

        if (this.currentWindow) {
            this.currentWindow.close();
        }

        try {
            this[entity+'Remove'](entity, id);
        }
        catch (err) {
            console.log(err.message);
            CJ.app.redirectToDefaultRoute();
        }
    },

    /**
     * Exact remove rout handler.
     */
    clientsRemove: function (entity, id) {
        this.currentWindow = new CJ.core.window.removeModelWindow.Widget({
            title: 'Remove Client',
            modelConfig: {
                modelCollectionName: entity,
                modelName: entity.slice(0, -1).toLowerCase(),
                modelId: id
            },
            navigateOnClose: entity + '/list'
        });
        this.currentWindow.show();
    },

    /**
     * Exact remove rout handler.
     */
    invoicesRemove: function (entity, id) {
        this.currentWindow = new CJ.core.window.removeModelWindow.Widget({
            title: 'Remove Invoice',
            modelConfig: {
                modelCollectionName: entity,
                modelName: entity.slice(0, -1).toLowerCase(),
                modelId: id
            },
            navigateOnClose: entity + '/list'
        });
        this.currentWindow.show();
    },

    /**
     * Default rout handler.
     */
    defaultRoute: function () {
        this.navigate('users/list', {
            trigger: true
        });
    }

});