/**
 * Created by yurii on 10.07.15.
 */

/**
 * User session.
 * */
CJ.core.userIdentity.UserIdentity = function () {
    this.initialize();
};

CJ.core.userIdentity.UserIdentity.prototype = {

    /**
     * Model of logged user.
     * @property {Object} Backbone,Model
     * */
    model: null,

    /**
     * Logged user id.
     * @property {Number}
     * */
    userId: undefined,

    initialize: function (cfg) {
        $.extend(this, cfg);
    },

    /**
     * Load session.
     * */
    downloadSession: function (callback) {
        $.ajax({
            method: "GET",
            url: "http://cashbox.loc/api/auth/login"
        })
            .done(function (user, msg, args) {
                this.model = new CJ.submodules.user.Model(user);
                this.userId = this.model.id;
                if (callback) {
                    callback();
                }
            }.bind(this))
            .fail(function () {
                if (callback) {
                    callback();
                }
            }.bind(this));
    },

    /**
     * @param {Object} loginData
     * @param {function} success Callback on login success.
     * @param {function} error Callback on login error,
     * */
    logIn: function (loginData, success, error) {
        $.ajax({
            method: "POST",
            url: "http://cashbox.loc/api/auth/login",
            data: JSON.stringify({
                email: loginData.email,
                password: loginData.password
            }),
            dataType: "json"
        })
            .done(function (user, msg, args) {
                this.model = new CJ.submodules.user.Model(user);
                this.userId = this.model.id;
                success();
            }.bind(this))
            .fail(function (msg, args) {
                error(msg, args);
            }.bind(this));
    },

    logOut: function (callback) {
        $.ajax({
            method: "POST",
            url: "http://cashbox.loc/api/auth/logout"
        })
            .done(function (user, msg, args) {
                this.model = null;
                this.userId = undefined;

                if (callback) {
                    callback();
                }
            }.bind(this)
        );
    },

    /**
     *
     * @returns {boolean}
     */
    isLoggedIn: function () {
        return (this.model) ? true : false;
    },

    /**
     *
     * @param {String} propertyName
     * @returns {Object} Whole model attributes or single property.
     */
    getData: function (propertyName) {
        if (typeof propertyName === 'string') {
            return this.model.get(propertyName);
        }

        return this.model.attributes;
    },

    /**
     *
     * @param {String} propertyName
     * @param propertyValue
     */
    setData: function (propertyName, propertyValue) {
        this.model.set({propertyName: propertyValue});
    },

    /**
     *
     * @returns {Object}
     */
    getModel: function () {
        return this.model;
    }

};