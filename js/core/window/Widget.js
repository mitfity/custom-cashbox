/**
 * Created by yurii on 13.07.15.
 */

CJ.core.window.Widget = CJ.core.View.extend({

    /**
     * @property {String}
     */
    classType: 'WindowWidget',

    /**
     * @cfg {String} Selector where to render.
     */
    contentRenderTo: '.modal-content',

    /**
     * @property {Object} CJ.core.View
     */
    content: null,

    initialize: function (cfg) {
        $.extend(this, cfg);

        this.initTemplate();
        this.title = this.title || 'Modal Window';

        CJ.core.View.prototype.initialize.apply(this, arguments);
        this.wrapContent(cfg.contentConfig);

        this.initEvents();
    },

    initEvents: function () {
        // Prevent modal window from closing on click outside.
        $('.modal', this.el).modal({
            backdrop: 'static',
            keyboard: false
        });
        $('.close', this.el).on('click', this.onCloseButtonClicked.bind(this));
    },

    initTemplate: function () {
        this.tpl = JST['js/core/window/Window.tpl.html'];
        this.contentRenderTo = this.renderTo + ' .modal-body';
    },

    /**
     * @override CJ.core.View
     */
    getTplData: function () {
        return { title: this.title };
    },

    /**
     * Close button click handler.
     * @param {Event} e
     */
    onCloseButtonClicked: function (e) {
        this.close();
    },

    /**
     * Creates content from config and stores it in this.content.
     * @param {Object} contentConfig
     */
    wrapContent: function (contentConfig) {
        var contentClass = this.contentClass,
            contentRenderTo = this.contentRenderTo;

        contentConfig = contentConfig || {};
        _.extend(contentConfig, { renderTo: contentRenderTo, parentWindow: this });

        if (contentClass) {
            try {
                this.content = new contentClass(contentConfig);
            }
            catch(err) {
                console.log('Error wrapContent: Wrong contentClass.', err.message);
            }
        }

        if (this.contentTpl) {
            this.content = new CJ.core.View({
                tpl: this.contentTpl,
                renderTo: contentRenderTo
            });
        }
    },

    show: function () {
        $('.modal', this.el).modal('show');
    },

    hide: function () {
        $('.modal', this.el).modal('hide');
    },

    /**
     * Hides window, destroys content and navigates to navigateOnClose or Backbone history back.
     */
    close: function () {
        this.hide();
        // Won't subscribe multiple times, because the view removes. So nothing to subscribe to.
        $('.modal').on('hidden.bs.modal', function () {
            this.remove();

            if (this.navigateOnClose) {
                CJ.app.router.navigate(this.navigateOnClose, {
                    trigger: true
                });
            }
            else {
                Backbone.history.history.back();
            }
        }.bind(this));
    }


});