/**
 * Created by yurii on 14.07.15.
 */


CJ.core.window.formCrudWindow.Widget = CJ.core.window.Widget.extend({

    /**
     * @property {String}
     */
    classType: 'FormCrudWindowWidget',

    /**
     * @cfg {String} Edit/add crud type.
     */
    crudType: 'add',

    initialize: function (cfg) {
        this.contentRenderTo = this.renderTo + ' .modal-body';

        CJ.core.window.Widget.prototype.initialize.apply(this, arguments);

        if (!this.crudType) {
            this.crudType = 'add';
        }

        this.showCrudButtons();
    },

    initTemplate: function () {
        this.tpl = JST['js/core/window/formCrudWindow/FormCrudWindow.tpl.html'];
    },

    initEvents: function () {
        CJ.core.window.Widget.prototype.initEvents.apply(this, arguments);

        $('.saveButton', this.el).on('click', this.onSaveButtonClicked.bind(this));
        $('.addButton', this.el).on('click', this.onAddButtonClicked.bind(this));
        $('.cancelButton', this.el).on('click', this.onCancelButtonClicked.bind(this));

        this.content.onFormSubmitSuccess = this.onFormSubmitSuccess.bind(this);
    },

    /**
     * Shows buttons according to crud type.
     */
    showCrudButtons: function () {
        var crudType = this.crudType;

        if (crudType === 'add') {
            $('.addButton', this.el).removeClass('hidden');
        }
        else if (crudType === 'edit') {
            $('.saveButton', this.el).removeClass('hidden');
        }
        else {
            throw new Error('showCrudButtons: wrong buttons type');
        }
    },

    /**
     * Save button click handler.
     * @param {Event} e
     */
    onSaveButtonClicked: function (e) {
        this.content.form.submit();
    },

    /**
     * Add button click handler.
     * @param {Event} e
     */
    onAddButtonClicked: function (e) {
        this.content.form.submit();
    },

    /**
     * Cancel button click handler.
     * @param {Event} e
     */
    onCancelButtonClicked: function (e) {
        this.close();
    },

    onFormSubmitSuccess: function () {
        this.close();
    }

});