/**
 * Created by yurii on 16.07.15.
 */

CJ.core.window.removeModelWindow.Widget = CJ.core.window.Widget.extend({

    classType: 'RemoveModelWindowWidget',

    /**
     * @cfg {Object} Backbone.Model
     */
    modelToRemove: null,

    initialize: function (cfg) {
        CJ.core.window.Widget.prototype.initialize.apply(this, arguments);

        this.modelToRemove = CJ.app.collections.getCollection(this.modelConfig.modelCollectionName, function (col) {
            this.modelToRemove = col.getModelById(this.modelConfig.modelId);
        }.bind(this));

    },

    initTemplate: function () {
        this.tpl = JST['js/core/window/removeModelWindow/RemoveModelWindow.tpl.html'];
    },

    getTplData: function () {
        return _.extend(CJ.core.window.Widget.prototype.getTplData.apply(this, arguments), {
            modelName: this.modelConfig.modelName
        });
    },

    initEvents: function () {
        CJ.core.window.Widget.prototype.initEvents.apply(this, arguments);

        $('.yesButton', this.el).on('click', this.onYesButtonClicked.bind(this));
        $('.cancelButton', this.el).on('click', this.onCancelButtonClicked.bind(this));
    },

    /**
     * Yes button click handler. Destroys model (if it is client than marks as removed).
     * @param {Event} e
     */
    onYesButtonClicked: function (e) {
        if (this.modelConfig.modelName === 'client') {
            this.modelToRemove.set({ removed: true });
            this.modelToRemove.save({}, {
                wait: true
            });
        }
        else {
            this.modelToRemove.destroy({
                success: function (model, response) {
                },
                error: function (model, error) {
                }
            });
        }

        this.close();
    },

    /**
     * Cancel button click handler.
     * @param {Event} e
     */
    onCancelButtonClicked: function (e) {
        this.close();
    }

});