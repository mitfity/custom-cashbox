/**
 * Created by mitfity on 30.06.2015.
 */

// create the root namespace and make sure we're not overwriting it
var CJ = CJ || {};

/**
 * Creates namespace in CJ global namespace.
 *
 * @param {String} namespace Namespace string name.
 * @return {Object} parent Returns created namespace (Javascript object).
 * */
CJ.createNamespace = function (namespace) {
    var namespaceParts = namespace.split("."),
        parent = CJ;

    // we want to be able to include or exclude the root namespace
    // So we strip it if it's in the namespace
    if (namespaceParts[0] === "CJ") {
        namespaceParts = namespaceParts.slice(1);
    }

    // loop through the parts and create
    // a nested namespace if necessary
    for (var i = 0; i < namespaceParts.length; i++) {
        var partName = namespaceParts[i];
        // check if the current parent already has
        // the namespace declared, if not create it
        if (typeof parent[partName] === "undefined") {
            parent[partName] = {};
        }
        // get a reference to the deepest element
        // in the hierarchy so far
        parent = parent[partName];
    }
    // the parent is now completely constructed
    // with empty namespaces and can be used.
    return parent;
};

CJ.createNamespace('CJ.core');
/**
 * Created by yurii on 09.07.15.
 */

CJ.createNamespace("CJ.core.collections");
/**
 * Created by yurii on 13.07.15.
 */

CJ.createNamespace('CJ.core.configs');
/**
 * Created by yurii on 10.07.15.
 */

CJ.createNamespace("CJ.core.etudeCashboxApp");
/**
 * Created by yurii on 06.07.15.
 */

// Create the namespace for form
CJ.createNamespace("CJ.core.form");
/**
 * Created by yurii on 06.07.15.
 */

CJ.createNamespace('CJ.core.grid');
/**
 * Created by yurii on 10.07.15.
 */

CJ.createNamespace("CJ.core.router");
/**
 * Created by yurii on 10.07.15.
 */

CJ.createNamespace('CJ.core.userIdentity');
/**
 * Created by yurii on 14.07.15.
 */

CJ.createNamespace('CJ.core.window.formCrudWindow');
/**
 * Created by yurii on 13.07.15.
 */

CJ.createNamespace('CJ.core.window');
/**
 * Created by yurii on 16.07.15.
 */

CJ.createNamespace('CJ.core.window.removeModelWindow');
/**
 * Global utilities for usage,
 * */
CJ.core.utils = {

    /**
     * Fills number with leading zeros.
     * @param {Number} num Number to be filled with zeros.
     * @param {Number} size Total number of digits.
     * @return {String} str The string containing number, filled with leading zeros.
     * */
    paddingWithZeros: function (num, size) {
        var str = num+"";

        while (str.length) {
            if (str[0] === '0') {
                str = str.substr(1);
            }
            else {
                break;
            }
        }

        while (str.length < size) {
            str = "0" + str;
        }

        return str;
    }

};


/**
 * Created by yurii on 30.06.15.
 */

/**
 * This is default view class. It describes default view behaviour.
 * @extends Backbone.View
 * */
CJ.core.View = Backbone.View.extend({

    /**
     * @property {String} classType Type of class. Is needed to recognize backbone models/views etc.
     * */
    classType: 'CoreView',

    /**
     * @property {Object} el Jquery object of this view.
     * */
    el: null,

    /**
     * @cfg {String} renderTo The selector in which view will be rendered.
     * */
    renderTo: 'body',

    /**
     * @cfg {Function} tpl Underscore template function, that contains html template.
     * */
    tpl: _.template('<div></div>'),

    /**
     * @override Backbone.View
     * */
    initialize: function (cfg) {
        $.extend(this, cfg);

        this.render();
    },

    /**
     * @override Backbone.View
     * */
    render: function () {
        this.el = $( this.tpl(this.getTplData()) ).appendTo(this.renderTo);
    },

    /**
     * Specify data passed to tpl.
     * @return {Object} Object Data passed to template.
     * */
    getTplData: function () {
        return {};
    },

    /**
     * Added jquery element remove.
     * @override Backbone.View
     * */
    remove: function () {
        this.onRemove();
        this.unbind();
        this.el.remove();
        Backbone.View.prototype.remove.apply(this, arguments);
    },

    onRemove: function () {
    }

});
/**
 * Created by yurii on 09.07.15.
 */

/**
 * Collections manager. Holds collections.
 * */
CJ.core.collections.Collections = function () {
    this.initialize();
};

// todo associative array
CJ.core.collections.Collections.prototype = {

    /* Begin Definitions */

    usersCollection: null,

    invoicesCollection: null,

    clientsCollection: null,

    productsCollection: null,

    /* End Definitions */

    initialize: function (cfg) {
        $.extend(this, cfg);
    },

    /**
     * Returns fetched collection of specific type,
     * @param {String} collectionName Name of collection.
     * @param {function} callback Callback function after fetch success.
     * @return {Object} collection Specific collection.
     * */
    getCollection: function (collectionName, callback) {
        var collection = null;

        if (collectionName === 'users') {
            collection = this.getUsersCollection();
            this.usersCollection = collection;
        }
        else if (collectionName === 'invoices') {
            collection = this.getInvoicesCollection();
            this.invoicesCollection = collection;
        }
        else if (collectionName === 'clients') {
            collection = this.getClientsCollection();
            this.clientsCollection = collection;
        }
        else if (collectionName === 'products') {
            collection = this.getProductsCollection();
            this.productsCollection = collection;
        }

        if (collection) {
            collection.fetch({
                reset: true,
                success: function (model, response) {
                    if (callback) {
                        callback(collection);
                    }
                },
                error: function (model, error) {
                    console.log(error.responseText);
                }
            });
        }

        return collection;
    },

    /**
     * @return {Object} Users collection.
     * */
    getUsersCollection: function () {
        return this.usersCollection || new CJ.submodules.user.Collection();
    },

    /**
     * @return {Object} Invoices collection.
     * */
    getInvoicesCollection: function () {
        return this.invoicesCollection || new CJ.submodules.invoice.Collection();
    },

    /**
     * @return {Object} Clients collection.
     * */
    getClientsCollection: function () {
        return this.clientsCollection || new CJ.submodules.client.Collection();
    },

    /**
     * @return {Object} Products collection.
     * */
    getProductsCollection: function () {
        return this.productsCollection || new CJ.submodules.product.Collection();
    }

};
/**
 * Created by yurii on 13.07.15.
 */

CJ.core.configs.jqueryValidationConfig = {

    /**
     * Custom ui jquery validator methods.
     */
    initialize: function () {

        $.validator.addMethod('phone', function (value, element) {
            return this.optional(element) || /^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{1,100}$/.test(value);
        }, 'Please enter a valid phone number');

        $.validator.addMethod('zip', function (value, element) {
            return this.optional(element) || /^\s*\d{5}\s*$/.test(value);
        }, 'Please enter a valid zip number');

        $.validator.addMethod('realDate', function (value, element) {
            return this.optional(element) || /^((0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01])[- /.](19|20)?[0-9]{2})*$/.test(value);
        }, 'Please enter a valid date number');

        /**
         * Makes element validate itself on characters input. By default after submit, if input field was validated
         * and marked as valid, then it will not be validated on input/focusOut and if u need to do something then u
         * can't. This validation rule solves this problem.
         */
        $.validator.addMethod('reValidate', function (value, element) {
            var field = $(element);

            if (!field.data('reValidationInit')) {
                field.on('input propertychange', function (e) {
                    field.valid();
                });
                field.data('reValidationInit', true);
            }

            return true;
        }, '');

        $.validator.addMethod("require_from_group", function(value, element, options) {
            var validator = this;
            var selector = options[1];
            var validOrNot = $(selector, element.form).filter(function() {
                    return validator.elementValue(this);
                }).length >= options[0];

            if(!$(element).data('being_validated')) {
                var fields = $(selector, element.form);
                fields.data('being_validated', true);
                fields.valid();
                fields.data('being_validated', false);
            }

            return !!value;
        }, '');

        /**
         * This rule makes fields with this rule to become require_from_group if requiredGroupFlagElement is not empty.
         * Options are: {
         *      className {String} Name of requiredGroup
         *      flagElementClassName {String} Name of flag element
         * }.
         */
        $.validator.addMethod('requiredGroup', function (value, element, options) {
            var relationInputs = $('input.' + options.className),
                isEmpty = !value,
                areRelationInputsEmpty = true,
                flagElement = $('.' + options.flagElementClassName, element.form);

            relationInputs = $.grep(relationInputs, function (input, index) {
                return input !== element;
            });

            relationInputs.forEach(function (input, index, coll) {
                if (input.value) {
                    areRelationInputsEmpty = false;
                    return true;
                }
            });

            if (!isEmpty) {
                if (areRelationInputsEmpty) {
                    relationInputs.forEach(function (input, index, coll) {
                        $(input).rules('add', {
                            require_from_group: [5, '.' + options.className]
                        });

                        $('label[for=\"'+$(input).attr('id')+'\"].inputLabel').addClass('required');
                    });
                }

                $(element).rules('add', {
                    require_from_group: [5, '.' + options.className]
                });

                $('label[for=\"'+$(element).attr('id')+'\"].inputLabel').addClass('required');

                return true;
            }
            else {
                if (areRelationInputsEmpty && !flagElement.val()) {
                    relationInputs.forEach(function (input, index, coll) {
                        $(input).rules('remove', 'require_from_group');
                        $('label[for=\"'+$(input).attr('id')+'\"].inputLabel').removeClass('required');
                    });

                    $(element).rules('remove', 'require_from_group');
                    $('label[for=\"'+$(element).attr('id')+'\"].inputLabel').removeClass('required');

                    return true;
                }
                else {
                    return false;
                }


            }
        }, 'This field is required.');

        /**
         * Element on which requiredGroup will depend.
         */
        $.validator.addMethod('requiredGroupFlagElement', function (value, element, groupClassName) {
            var relationInputs = $('input.' + groupClassName),
                areRelationInputsEmpty = true;

            relationInputs.each(function (index, input) {
                if ($(input).val()) {
                    areRelationInputsEmpty = false;
                }

                $(input).rules('add', {
                    require_from_group: [5, '.' + groupClassName]
                });

                $('label[for=\"'+$(input).attr('id')+'\"].inputLabel').addClass('required');
            });

            if (!value && areRelationInputsEmpty) {
                relationInputs.each(function (index, input) {
                    $(input).rules('remove', 'require_from_group');
                    $('label[for=\"'+$(input).attr('id')+'\"].inputLabel').removeClass('required');
                    $(input).valid();
                });
            }

            return true;
        }, '');
    }

};

CJ.core.configs.jqueryValidationConfig.initialize();
/**
 * Created by yurii on 21.07.15.
 */

CJ.core.configs.slickgridValidationConfig = {

    /**
     * Custom slickgrid editors/formatters/validators.
     */
    initialize: function () {

        this.requiredFieldValidator = function (value) {
            if (value === null || value === undefined || !value.length) {
                return {
                    valid: false,
                    msg: "This is a required field"
                };
            }
            else {
                return {
                    valid: true,
                    msg: null
                };
            }
        };

        this.htmlFormatter = function (row, cell, value, columnDef, dataContext) {
            return value;
        };

        this.requiredFieldFormatter = function (row, cell, value, columnDef, dataContext) {
            if (!dataContext._invalid) {
                dataContext._invalid = [];
            }

            if (value) {
                dataContext._invalid = $.grep(dataContext._invalid, function (element) {
                    return element.cellIndex !== cell;
                });
                return "<span class='validInput'>" + value + "</span>";
            }

            if ($.grep(dataContext._invalid, function (element) {
                    return element.cellIndex === cell;
                }).length === 0) {
                dataContext._invalid.push({cellIndex: cell});
            }
            return "<span class='invalidInput' title='This field is required.'>" + value +
                "<span class='glyphicon glyphicon-exclamation-sign'></span></span>";
        };

        this.requiredNameFormatter = function (row, cell, value, columnDef, dataContext) {
            if (!dataContext._invalid) {
                dataContext._invalid = [];
            }

            if (value || !dataContext.description) {
                dataContext._invalid = $.grep(dataContext._invalid, function (element) {
                    return element.cellIndex !== cell;
                });
                return "<span class='validInput'>" + value + "</span>";
            }

            if ($.grep(dataContext._invalid, function (element) {
                    return element.cellIndex === cell;
                }).length === 0) {
                dataContext._invalid.push({cellIndex: cell});
            }
            return "<span class='invalidInput' title='This field is required.'>" + value +
                "<span class='glyphicon glyphicon-exclamation-sign'></span></span>";
        };

        var validateNumber = function (cell, value, dataContext) {
            if (!dataContext._invalid) {
                dataContext._invalid = [];
            }

            if (!value && value === undefined) {
                if ($.grep(dataContext._invalid, function (element) {
                        return element.cellIndex === cell;
                    }).length === 0) {
                    dataContext._invalid.push({ cellIndex: cell });
                }
                value = "<span class='invalidInput' title='This field is required.'>" + value +
                    "<span class='glyphicon glyphicon-exclamation-sign'></span></span>";
            }
            else if (!( !isNaN(parseFloat(value)) && isFinite(value) )) {
                if ($.grep(dataContext._invalid, function (element) {
                        return element.cellIndex === cell;
                    }).length === 0) {
                    dataContext._invalid.push({ cellIndex: cell });
                }
                value = "<span class='invalidInput' title='Has to be a number.'>NaN" +
                    "<span class='glyphicon glyphicon-exclamation-sign'></span></span>";
            }
            else {
                dataContext._invalid = $.grep(dataContext._invalid, function (element) {
                    return element.cellIndex !== cell;
                });
            }

            return {
                value: value,
                dataContext: dataContext
            };
        };

        this.numberFormatter = function (row, cell, value, columnDef, dataContext) {
            if (!dataContext._invalid) {
                dataContext._invalid = [];
            }

            var validatedNumber = validateNumber(cell, value, dataContext);

            if (validatedNumber.dataContext._invalid) {
                return validatedNumber.value;
            }

            dataContext._invalid = $.grep(dataContext._invalid, function (element) {
                return element.cellIndex !== cell;
            });
            return "<span class='validInput'>" + value.toFixed(2) + "</span>";
        };

        this.priceFormatter = function (row, cell, value, columnDef, dataContext) {
            if (!dataContext._invalid) {
                dataContext._invalid = [];
            }

            var validatedNumber = validateNumber(cell, value, dataContext);

            if (validatedNumber.dataContext._invalid) {
                return validatedNumber.value;
            }

            dataContext._invalid = $.grep(dataContext._invalid, function (element) {
                return element.cellIndex !== cell;
            });
            return "<span class='validInput'>$" + value.toFixed(2) + "</span>";
        };

        this.percentageFormatter = function (row, cell, value, columnDef, dataContext) {
            if (!dataContext._invalid) {
                dataContext._invalid = [];
            }

            var validatedNumber = validateNumber(cell, value, dataContext);

            if (validatedNumber.dataContext._invalid) {
                return validatedNumber.value;
            }

            dataContext._invalid = $.grep(dataContext._invalid, function (element) {
                return element.cellIndex !== cell;
            });
            return "<span class='validInput'>" + value.toFixed(2) + "%</span>";
        };

        this.positiveFloatEditor = function (args) {
            var $input;
            var defaultValue;
            var scope = this;

            this.init = function () {
                $input = $("<INPUT type=text class='editor-text' />");

                $input.bind("keydown", function (e) {
                    if (e.shiftKey) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                    }

                    var value = $input.val(),
                        isNumber = e.keyCode >= 48 && e.keyCode <= 57,
                        dotPos = value.indexOf("."),
                        isDot = (e.keyCode === 190 && dotPos === -1),
                        isControlButton = (e.keyCode < 48 || (e.keyCode > 90 && e.keyCode < 106));

                    if (!(isNumber || isDot || isControlButton)) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                    }
                    if (e.keyCode === $.ui.keyCode.LEFT || e.keyCode === $.ui.keyCode.RIGHT) {
                        e.stopImmediatePropagation();
                    }
                });

                $input.appendTo(args.container);
                $input.focus().select();
            };

            this.destroy = function () {
                $input.remove();
            };

            this.focus = function () {
                $input.focus();
            };

            this.loadValue = function (item) {
                defaultValue = item[args.column.field];
                $input.val(defaultValue);
                $input[0].defaultValue = defaultValue;
                $input.select();
            };

            this.serializeValue = function () {
                var val = +$input.val();
                if (val > 0) {
                    return val;
                }
                return 0;
            };

            this.applyValue = function (item, state) {
                item[args.column.field] = state;
            };

            this.isValueChanged = function () {
                return (!($input.val() === "" && defaultValue === null)) && ($input.val() !== defaultValue);
            };

            this.validate = function () {
                return {
                    valid: true,
                    msg: null
                };
            };

            this.init();
        };

        this.formatterLineTotal = function (row, cell, value, rowConfig, context) {
            if (context.count && context.price && context.tax !== undefined) {
                var sumPrice = (context.count * context.price),
                    total = sumPrice / 100 * (100 + context.tax);

                context.total = total;

                return "$" + total.toFixed(2);
            }
            else {
                context.total = 0;
                return "$" + 0 ;
            }
        };

        this.productsAutocompleteEditor = function (args) {
            var $input,
                defaultValue,
                scope = this,
                item = args.item,
                grid = args.grid,
                cell = grid.getActiveCell(),
                row = cell.row,
                dataView = grid.getData(),
                sameNameProducts;

            this.init = function () {
                $input = $("<INPUT id='tags' class='editor-text' />");
                $input.appendTo(args.container);
                $input.focus().select();
                $input.autocomplete({
                    minLength: 0,
                    source: 'http://cashbox.loc/api/product',
                    select: function (event, ui) {
                        $input.val(ui.item.productName);
                        item.name = ui.item.name;
                        item.description = ui.item.description;
                        item.price = ui.item.price;
                        item.tax = ui.item.tax || 10;

                        if (item.id === undefined) {
                            item.id = 'product' + grid.getData().getLength();
                            item.count = 1;
                            item.buttons = '<div class="removeItem"><span class="glyphicon glyphicon-trash' +
                            ' "></span></div>';

                            dataView.addItem(item);
                            return false;
                        }

                        dataView.updateItem(item.id, item);

                        return false;
                    }.bind(this)
                })
                    .autocomplete("instance")._renderItem = function (ul, item) {
                    return $("<li>")
                        .append("<a>" + item.name + " $" + item.price + "</a>")
                        .appendTo(ul);
                };
            };

            this.destroy = function () {
                $input.autocomplete("destroy");
            };

            this.focus = function () {
                $input.focus();
            };

            this.loadValue = function (item) {
                defaultValue = item[args.column.field];
                $input.val(defaultValue);
                $input[0].defaultValue = defaultValue;
                $input.select();
            };

            this.serializeValue = function () {
                return $input.val();
            };

            this.applyValue = function (item, state) {
                item[args.column.field] = state;
            };

            this.isValueChanged = function () {
                return (!($input.val() === "" && defaultValue === null)) && ($input.val() !== defaultValue);
            };

            this.validate = function () {
                return {
                    valid: true,
                    msg: null
                };
            };

            this.init();
        };

    }

};

CJ.core.configs.slickgridValidationConfig.initialize();
/**
 * Created by yurii on 09.07.15.
 */

/**
 * This is application class. Here application starts, The application is about users, clients, invoices... how they
 * work together.
 * */
CJ.core.etudeCashboxApp.EtudeCashboxApp = function () {
    this.initialize();
};

CJ.core.etudeCashboxApp.EtudeCashboxApp.prototype = {

    /**
     * @property {Object} CJ.core.View
     * */
    currentView: null,

    /**
     * @property {Object} CJ.core.collections.Collections
     * */
    collections: null,

    /**
     * @property {Object} CJ.core.userIdentity.UserIdentity
     * */
    userIdentity: null,

    /**
     * @property {Object} CJ.core.router.Router
     * */
    router: null,

    /**
     * @property {String} The route, where user will be redirected if he is logged in.
     * */
    privateRoute: 'users/list',

    /**
     * @property {String} The route, where user will be redirected if he is not logged in.
     * */
    publicRoute: 'login',

    initialize: function (cfg) {
        $.extend(this, cfg);

        this.collections = CJ.core.collections.instance = new CJ.core.collections.Collections();
        this.userIdentity = new CJ.core.userIdentity.UserIdentity();
        this.userIdentity.downloadSession(function () {
            this.router = new CJ.core.router.Router();

            setTimeout(function () {
                Backbone.history.start();
            }, 16);

            this.initPopupsCentering();
        }.bind(this));
    },

    /**
     * Inits events on which modal windows will be centered.
     * */
    initPopupsCentering: function () {
        $(function () {
            function centerModal() {
                $(this).css('display', 'block');
                var $dialog = $(this).find('.modal-dialog'),
                    offset = ($(window).height() - $dialog.height()) / 2,
                    bottomMargin = parseInt($dialog.css('marginBottom'), 10);

                if(offset < bottomMargin) {
                    offset = bottomMargin;
                }

                $dialog.css('margin-top', offset);
            }

            $(document).on('show.bs.modal', '.modal', centerModal);
            $(window).on('resize', function () {
                $('.modal:visible').each(centerModal);
            });
        });
    },

    /**
     * @param {Object} classPrototype View class prototype to be created.
     * */
    setCurrentView: function (classPrototype) {
        if (this.currentView) {
            if (this.currentView.classType === classPrototype.prototype.classType) {
                return;
            }

            this.currentView.remove();
        }

        this.currentView = new classPrototype();
    },

    /**
     * Redirects to private route if user is login and to public if not.
     * */
    redirectToDefaultRoute: function () {
        if (this.userIdentity.isLoggedIn()) {
            this.router.navigate(this.privateRoute, {
                trigger: true
            });
            return;
        }

        this.router.navigate(this.publicRoute, {
            trigger: true
        });
    }

};
/**
 * Created by yurii on 06.07.15.
 */

CJ.core.form.Widget = CJ.core.View.extend({

    classType: 'FormWidget',

    /**
     * Jquery form object.
     */
    form: null,

    /**
     * Jquery validator.
     */
    validator: null,

    initialize: function () {
        CJ.core.View.prototype.initialize.apply(this, arguments);
        this.form = this.getForm();
        this.initValidation();
        this.initEvents();
    },

    /**
     * You have to override this method to return DOM form element or jquery form element.
     * Factory method.
     */
    getForm: function () {
    },

    /**
     * Initializes this.validator.
     */
    initValidation: function () {
        var validationOptions = this.getValidationOptions();

        _.extend(validationOptions, {
            onkeyup: function (element, e) {
                // Needed in case to show custom error on enter pressed. Bug was : error shows on enter down and
                // hides on enter up.
                if (e.keyCode === 13) {
                    if (this.lastElement !== element) {
                        this.lastElement = element;
                    }
                    return;
                }

                $.validator.defaults.onkeyup.apply(this, arguments);
            },

            errorClass: "invalid",

            validClass: "success"
        });

        if (!this.form) {
            throw new Error('initValidation: No form found. Need to specify form as a result of getForm method.');

        }
        this.validator = $(this.form).validate(validationOptions);
    },

    initEvents: function () {
        this.form.submit(function (e) {
            e.preventDefault();
            e.stopImmediatePropagation();

            this.onFormSubmit.apply(this, arguments);
            return false;
        }.bind(this));
    },

    /**
     * @return {Object} Options for validator (rules, messages...).
     */
    getValidationOptions: function () {
        return {};
    },

    /**
     * It will be called when form submits. Default submit event is stopped before calling this method.
     * @param e
     */
    onFormSubmit: function (e) {
    },

    /**
     * @returns {Object} JSON object of form input values. Uses jquery serializeArray method.
     */
    getValues: function () {
        var values = {};

        $(this.form).serializeArray().forEach(function (el, index, col) {
            values[el.name] = el.value;
        });

        return values;
    },

    /**
     * Sets form values by inputs names.
     * @param {Object} formValues
     */
    setValues: function (formValues) {
        for (var formValue in formValues) {
            if (formValues.hasOwnProperty(formValue)) {

                var element = $( 'input[name=\"' + formValue + '\"]', this.form );

                if (element) {
                    element.val(formValues[formValue]);
                }
            }
        }
    },

    /**
     * @returns {boolean} True if form is empty,
     */
    isEmpty: function () {
        var empty = true;

        $(':input', this.form).each(function() {
            if($(this).val() !== '') {
                empty = false;
            }
        });

        return empty;
    },

    /**
     * @returns {boolean} True if form inputs are valid.
     */
    isValid: function () {
        var formInputs = $('input', this.form),
            isValidForm = true;

        formInputs.each(function (index, input) {
            if (!$(input).valid()) {
                isValidForm = false;
            }
        });

        return isValidForm;
    },

    disableSubmitOnEnter: function () {
        this.form.on('keyup keypress', function(e) {
            var code = e.keyCode || e.which;
            if (code == 13) {
                e.preventDefault();
                return false;
            }
        });
    },

    /**
     * @param {Object} model Editable model.
     */
    setTargetModel: function (model) {
        this.targetModel = model;
        this.currentModel.set(this.targetModel.attributes);
    }

});
/**
 * Created by yurii on 30.06.15.
 */

CJ.core.grid.Widget = CJ.core.View.extend( {

    classType: 'GridWidget',

    /**
     * @property {Object} Slick.Grid
     */
    grid: null,

    /**
     * @property {Object} Slick.Data.DataView
     */
    gridDataView: null,

    initialize: function (cfg) {
        this.tpl = JST['js/core/grid/Grid.tpl.html'];
        CJ.core.View.prototype.initialize.apply(this, arguments);

        this.tplOptions = this.tplOptions || {};
        this.gridColumns = this.gridColumns || {};
        this.gridOptions = this.gridOptions || {};
        this.gridData = this.gridData || {};
        this.dataCollection = this.dataCollection || {};

        this.initGrid();
        this.initEvents();
    },

    initEvents: function () {
        var $viewPort = $('.viewport'),
            lastHeight = $viewPort.css('height'),
            timeOutBuff = null;
            checkForChanges = function () {
                if (!this.grid) {
                    clearTimeout(timeOutBuff);
                    return;
                }

                if ($viewPort.css('height') != lastHeight) {
                    this.grid.resizeCanvas();
                    lastHeight = $viewPort.css('height');
                }

                timeOutBuff = setTimeout(checkForChanges, 500);
            }.bind(this);
        checkForChanges();

        $viewPort.on('resize', this.onContainerResized.bind(this));
        $('.slick-viewport', this.el).on('blur', 'input.editor-text', function (e) {
            window.setTimeout(function(){
                Slick.GlobalEditorLock.commitCurrentEdit();
            },  0);
        });
    },

    initGrid: function () {
        this.gridDataView = new Slick.Data.DataView();

        var columns = this.gridColumns,
            options = this.gridOptions,
            data = this.gridData,
            gridContainer = this.renderTo + ' .renderGrid',
            dataView = this.gridDataView;


        options.rowHeight = 42;
        options.headerRowHeight = 42;
        options.syncColumnCellResize = true;
        options.enableTextSelectionOnCells = true;

        this.grid = new Slick.Grid(gridContainer, dataView, columns, options);
        this.grid.setSelectionModel(Slick.RowSelectionModel);

        this.grid.onCellChange.subscribe(function (e, args) {
            dataView.updateItem(args.item.id, args.item);
            this.onGridCellChange(e, args);
        }.bind(this));

        this.grid.onAddNewRow.subscribe(function (e, args) {
            this.onGridAddNewRow(e, args);
            this.validateRow(args.grid, args.grid.getActiveCell().row);
        }.bind(this));

        // Customize data view
        // wire up model events to drive the grid
        dataView.onRowCountChanged.subscribe(function (e, args) {
            this.grid.updateRowCount();
            this.grid.render();
        }.bind(this));

        dataView.onRowsChanged.subscribe(function (e, args) {
            this.grid.invalidateRows(args.rows);
            this.grid.render();
            this.onDataviewRowsChanged(e, args);
        }.bind(this));

        dataView.getItemMetadata = this.rowMetadata.bind(this);

        this.setDataViewItems(data, "id");
    },

    /**
     * @param {Event} e
     * @param {Object} args
     */
    onGridCellChange: function (e, args) {
    },

    /**
     * @param {Event} e
     * @param {Object} args
     */
    onGridAddNewRow: function (e, args) {
    },

    /**
     * @param {Event} e
     * @param {Object} args
     */
    onDataviewRowsChanged: function (e, args) {
    },

    /**
     * @param {Event} e
     */
    onContainerResized: function (e) {
        this.grid.resizeCanvas();
    },

    /**
     * @param {Array} columns
     */
    setColumnsResizable: function (columns) {
        columns.forEach(function (el, index, coll) {
            el.resizable = true;
        });

        this.grid.setColumns(columns);
    },

    /**
     * @param {Object} grid
     * @param {Number} rowIndex
     */
    validateRow: function (grid, rowIndex) {
        $.each(grid.getColumns(), function (columnIndex, column) {
            // iterate through editable cells
            var item = grid.getDataItem(rowIndex);

            if (column.editor && column.validator) {
                var validationResults = column.validator(item[column.field]);
                if (!validationResults.valid) {
                    // show editor
                    grid.gotoCell(rowIndex, columnIndex, true);

                    // validate (it will fail)
                    grid.getEditorLock().commitCurrentEdit();

                    // stop iteration
                    return false;
                }
            }
        });
    },

    /**
     * @private
     */
    rowMetadata: function (row) {
        var item = this.gridDataView.getItem(row);

        if (item && item._inactive) {
            return { cssClasses: 'inactive' };
        }

        return {};
    },

    /**
     * @param {Number} rowIndex
     */
    removeRow: function (rowIndex) {
        var item = this.gridDataView.getItem(rowIndex),
            rowId = item.id;

        this.gridDataView.deleteItem(rowId);
        this.grid.invalidate();
        this.grid.render();
    },

    removeActiveRow: function () {
        this.removeRow(this.grid.getActiveCell().row);
    },

    /**
     * @param {Number} rowIndex
     * @param {Object} data
     * @param {Object} jquery
     */
    setRowData: function (rowIndex, data, jquery) {
        var row = this.grid.getData().getItem(rowIndex);

        for (var property in data) {
            if (data.hasOwnProperty(property)) {
                row[property] = data[property];
            }
        }

        this.setDataViewItems(this.gridData, "id");
        this.grid.invalidateRow(rowIndex);
        this.grid.render();
    },

    /**
     * @param {Array} data
     * @param {String} idString
     */
    setDataViewItems: function (data, idString) {
        if (typeof data[0] === 'object') {
            this.gridDataView.setItems(data, idString);
            return;
        }

        var createdData = [];

        data.forEach(function (item, index, col) {
            createdData.push(new item(this.dataCollection, index, this.dataCollection[index]));
        }.bind(this));

        this.gridDataView.setItems(createdData, idString);
    },

    /**
     * @returns {boolean} True if there are no invalid grid cells.
     */
    isValid: function () {
        var dataView = this.gridDataView,
            rowsCount = dataView.getLength();

        for (var i = 0; i < rowsCount; i++) {
            if (dataView.getItem(i)._invalid.length) {
                return false;
            }
        }

        return true;
    },

    /**
     * @returns {Array} values
     */
    getValues: function () {
        var items = this.gridDataView.getItems(),
            values = [];

        items.forEach(function (el, index, coll) {
            var value = {};
            for (var property in el) {
                if (el.hasOwnProperty(property) && property !== '_invalid' && property !== 'id' &&
                    property !== 'buttons') {

                    value[property] = el[property];
                }
            }
            values.push(value);
        });

        return values;
    },

    /**
     * This method is called before remove(). It is used for removing references.
     * @private
     */
    onRemove: function () {
        $('.viewport').off('resize');
        this.grid.destroy();
        this.grid = null;
    }

});
/**
 * Created by yurii on 10.07.15.
 */

/**
 * Applications router. Manages routes.
 */
CJ.core.router.Router = Backbone.Router.extend({

    /**
     * @property {Object} CJ.core.window.Widget
     */
    currentWindow: null,

    /**
     * @override Backbone.Router
     */
    routes: {
        'login': 'login',
        'passwordRecovery': 'passwordRecovery',
        'logout': 'logout',
        'profile': 'profile',
        ':entity/list': 'list',
        ':entity/edit/:id': 'edit',
        ':entity/add': 'add',
        ':entity/remove/:id': 'remove',
        '*path': 'defaultRoute'
    },

    initialize: function () {
        _.extend(this, Backbone.Events);
    },

    /**
     * @override Backbone.Router
     * @param callback
     * @param args
     * @param name
     * @returns {boolean}
     */
    execute: function(callback, args, name) {
        if (CJ.app.userIdentity.isLoggedIn()) {
            if (name === 'login' || name === 'passwordRecovery') {
                CJ.app.redirectToDefaultRoute();
            }

            CJ.app.setCurrentView(CJ.submodules.viewport.Widget);

            if (callback) {
                callback.apply(this, args);
            }
        }
        else {
            if (name === 'login') {
                CJ.app.setCurrentView(CJ.submodules.login.Widget);
            }
            else if (name === 'passwordRecovery') {
                CJ.app.setCurrentView(CJ.submodules.login.passwordRecovery.Widget);
            }
            else {
                this.navigate('login', { trigger: true });
            }
        }

        return false;
    },

    login: function () {
    },

    passwordRecovery: function () {
    },

    /**
     * Logs user out.
     */
    logout: function () {
        CJ.app.userIdentity.logOut(function () {
            CJ.app.redirectToDefaultRoute();
        });
    },

    /**
     * Profile rout handler.
     */
    profile: function () {
        this.currentWindow = new CJ.core.window.formCrudWindow.Widget({
            title: 'User Profile',
            contentClass: CJ.submodules.user.FormWidget,
            contentConfig: {
                modelId: CJ.app.userIdentity.userId,
                isProfile: true
            },
            crudType: 'edit'
        });
        this.currentWindow.show();
    },

    /**
     * List rout handler.
     */
    list: function (entity) {
        this[entity+'List'](entity);
    },

    /**
     * Exact list rout handler.
     */
    usersList: function (entity) {
        this.trigger('viewport.changeContent', { contentPrototype: CJ.submodules.user.ListWidget, contentName: entity });
    },

    /**
     * Exact rout handler.
     */
    clientsList: function (entity) {
        this.trigger('viewport.changeContent', { contentPrototype: CJ.submodules.client.ListWidget, contentName: entity });
    },

    /**
     * Exact rout handler.
     */
    invoicesList: function (entity) {
        this.trigger('viewport.changeContent', { contentPrototype: CJ.submodules.invoice.ListWidget, contentName: entity });
    },

    /**
     * Edit rout handler.
     */
    edit: function (entity, id) {
        id = parseInt(id, 10);

        if (this.currentWindow) {
            this.currentWindow.close();
        }

        this[entity+'Edit'](entity, id);
    },

    /**
     * Exact edit rout handler.
     */
    usersEdit: function (entity, id) {
        this.currentWindow = new CJ.core.window.formCrudWindow.Widget({
            title: 'Edit User',
            contentClass: CJ.submodules.user.FormWidget,
            contentConfig: {
                modelId: id
            },
            crudType: 'edit',
            navigateOnClose: entity + '/list'
        });
        this.currentWindow.show();
    },

    /**
     * Exact edit rout handler.
     */
    clientsEdit: function (entity, id) {
        this.currentWindow = new CJ.core.window.formCrudWindow.Widget({
            title: 'Edit Client',
            contentClass: CJ.submodules.client.FormWidget,
            contentConfig: {
                modelId: id
            },
            crudType: 'edit',
            navigateOnClose: entity + '/list'
        });
        this.currentWindow.show();
    },

    /**
     * Exact edit rout handler.
     */
    invoicesEdit: function (entity, id) {
        this.trigger('viewport.changeContent', {
            contentPrototype: CJ.submodules.invoice.CrudWidget,
            contentName: entity,
            contentTitle: 'Edit Invoice',
            modelId: id
        });
    },

    /**
     * Add rout handler.
     */
    add: function (entity) {
        if (this.currentWindow) {
            this.currentWindow.close();
        }

        this[entity+'Add'](entity);
    },

    /**
     * Exact add rout handler.
     */
    usersAdd: function (entity) {
        this.currentWindow = new CJ.core.window.formCrudWindow.Widget({
            title: 'Add User',
            contentClass: CJ.submodules.user.FormWidget,
            contentConfig: {

            },
            crudType: 'add',
            navigateOnClose: entity + '/list'
        });
        this.currentWindow.show();
    },

    /**
     * Exact add rout handler.
     */
    clientsAdd: function (entity) {
        this.currentWindow = new CJ.core.window.formCrudWindow.Widget({
            title: 'Add Client',
            contentClass: CJ.submodules.client.FormWidget,
            contentConfig: {

            },
            crudType: 'add',
            navigateOnClose: entity + '/list'
        });
        this.currentWindow.show();
    },

    /**
     * Exact add rout handler.
     */
    invoicesAdd: function (entity) {
        this.trigger('viewport.changeContent', {
            contentPrototype: CJ.submodules.invoice.CrudWidget,
            contentName: entity,
            contentTitle: 'Create New Invoice'
        });
    },

    /**
     * Remove rout handler.
     */
    remove: function (entity, id) {
        id = parseInt(id, 10);

        if (this.currentWindow) {
            this.currentWindow.close();
        }

        try {
            this[entity+'Remove'](entity, id);
        }
        catch (err) {
            console.log(err.message);
            CJ.app.redirectToDefaultRoute();
        }
    },

    /**
     * Exact remove rout handler.
     */
    clientsRemove: function (entity, id) {
        this.currentWindow = new CJ.core.window.removeModelWindow.Widget({
            title: 'Remove Client',
            modelConfig: {
                modelCollectionName: entity,
                modelName: entity.slice(0, -1).toLowerCase(),
                modelId: id
            },
            navigateOnClose: entity + '/list'
        });
        this.currentWindow.show();
    },

    /**
     * Exact remove rout handler.
     */
    invoicesRemove: function (entity, id) {
        this.currentWindow = new CJ.core.window.removeModelWindow.Widget({
            title: 'Remove Invoice',
            modelConfig: {
                modelCollectionName: entity,
                modelName: entity.slice(0, -1).toLowerCase(),
                modelId: id
            },
            navigateOnClose: entity + '/list'
        });
        this.currentWindow.show();
    },

    /**
     * Default rout handler.
     */
    defaultRoute: function () {
        this.navigate('users/list', {
            trigger: true
        });
    }

});
/**
 * Created by yurii on 10.07.15.
 */

/**
 * User session.
 * */
CJ.core.userIdentity.UserIdentity = function () {
    this.initialize();
};

CJ.core.userIdentity.UserIdentity.prototype = {

    /**
     * Model of logged user.
     * @property {Object} Backbone,Model
     * */
    model: null,

    /**
     * Logged user id.
     * @property {Number}
     * */
    userId: undefined,

    initialize: function (cfg) {
        $.extend(this, cfg);
    },

    /**
     * Load session.
     * */
    downloadSession: function (callback) {
        $.ajax({
            method: "GET",
            url: "http://cashbox.loc/api/auth/login"
        })
            .done(function (user, msg, args) {
                this.model = new CJ.submodules.user.Model(user);
                this.userId = this.model.id;
                if (callback) {
                    callback();
                }
            }.bind(this))
            .fail(function () {
                if (callback) {
                    callback();
                }
            }.bind(this));
    },

    /**
     * @param {Object} loginData
     * @param {function} success Callback on login success.
     * @param {function} error Callback on login error,
     * */
    logIn: function (loginData, success, error) {
        $.ajax({
            method: "POST",
            url: "http://cashbox.loc/api/auth/login",
            data: JSON.stringify({
                email: loginData.email,
                password: loginData.password
            }),
            dataType: "json"
        })
            .done(function (user, msg, args) {
                this.model = new CJ.submodules.user.Model(user);
                this.userId = this.model.id;
                success();
            }.bind(this))
            .fail(function (msg, args) {
                error(msg, args);
            }.bind(this));
    },

    logOut: function (callback) {
        $.ajax({
            method: "POST",
            url: "http://cashbox.loc/api/auth/logout"
        })
            .done(function (user, msg, args) {
                this.model = null;
                this.userId = undefined;

                if (callback) {
                    callback();
                }
            }.bind(this)
        );
    },

    /**
     *
     * @returns {boolean}
     */
    isLoggedIn: function () {
        return (this.model) ? true : false;
    },

    /**
     *
     * @param {String} propertyName
     * @returns {Object} Whole model attributes or single property.
     */
    getData: function (propertyName) {
        if (typeof propertyName === 'string') {
            return this.model.get(propertyName);
        }

        return this.model.attributes;
    },

    /**
     *
     * @param {String} propertyName
     * @param propertyValue
     */
    setData: function (propertyName, propertyValue) {
        this.model.set({propertyName: propertyValue});
    },

    /**
     *
     * @returns {Object}
     */
    getModel: function () {
        return this.model;
    }

};
/**
 * Created by yurii on 13.07.15.
 */

CJ.core.window.Widget = CJ.core.View.extend({

    /**
     * @property {String}
     */
    classType: 'WindowWidget',

    /**
     * @cfg {String} Selector where to render.
     */
    contentRenderTo: '.modal-content',

    /**
     * @property {Object} CJ.core.View
     */
    content: null,

    initialize: function (cfg) {
        $.extend(this, cfg);

        this.initTemplate();
        this.title = this.title || 'Modal Window';

        CJ.core.View.prototype.initialize.apply(this, arguments);
        this.wrapContent(cfg.contentConfig);

        this.initEvents();
    },

    initEvents: function () {
        // Prevent modal window from closing on click outside.
        $('.modal', this.el).modal({
            backdrop: 'static',
            keyboard: false
        });
        $('.close', this.el).on('click', this.onCloseButtonClicked.bind(this));
    },

    initTemplate: function () {
        this.tpl = JST['js/core/window/Window.tpl.html'];
        this.contentRenderTo = this.renderTo + ' .modal-body';
    },

    /**
     * @override CJ.core.View
     */
    getTplData: function () {
        return { title: this.title };
    },

    /**
     * Close button click handler.
     * @param {Event} e
     */
    onCloseButtonClicked: function (e) {
        this.close();
    },

    /**
     * Creates content from config and stores it in this.content.
     * @param {Object} contentConfig
     */
    wrapContent: function (contentConfig) {
        var contentClass = this.contentClass,
            contentRenderTo = this.contentRenderTo;

        contentConfig = contentConfig || {};
        _.extend(contentConfig, { renderTo: contentRenderTo, parentWindow: this });

        if (contentClass) {
            try {
                this.content = new contentClass(contentConfig);
            }
            catch(err) {
                console.log('Error wrapContent: Wrong contentClass.', err.message);
            }
        }

        if (this.contentTpl) {
            this.content = new CJ.core.View({
                tpl: this.contentTpl,
                renderTo: contentRenderTo
            });
        }
    },

    show: function () {
        $('.modal', this.el).modal('show');
    },

    hide: function () {
        $('.modal', this.el).modal('hide');
    },

    /**
     * Hides window, destroys content and navigates to navigateOnClose or Backbone history back.
     */
    close: function () {
        this.hide();
        // Won't subscribe multiple times, because the view removes. So nothing to subscribe to.
        $('.modal').on('hidden.bs.modal', function () {
            this.remove();

            if (this.navigateOnClose) {
                CJ.app.router.navigate(this.navigateOnClose, {
                    trigger: true
                });
            }
            else {
                Backbone.history.history.back();
            }
        }.bind(this));
    }


});
/**
 * Created by yurii on 14.07.15.
 */


CJ.core.window.formCrudWindow.Widget = CJ.core.window.Widget.extend({

    /**
     * @property {String}
     */
    classType: 'FormCrudWindowWidget',

    /**
     * @cfg {String} Edit/add crud type.
     */
    crudType: 'add',

    initialize: function (cfg) {
        this.contentRenderTo = this.renderTo + ' .modal-body';

        CJ.core.window.Widget.prototype.initialize.apply(this, arguments);

        if (!this.crudType) {
            this.crudType = 'add';
        }

        this.showCrudButtons();
    },

    initTemplate: function () {
        this.tpl = JST['js/core/window/formCrudWindow/FormCrudWindow.tpl.html'];
    },

    initEvents: function () {
        CJ.core.window.Widget.prototype.initEvents.apply(this, arguments);

        $('.saveButton', this.el).on('click', this.onSaveButtonClicked.bind(this));
        $('.addButton', this.el).on('click', this.onAddButtonClicked.bind(this));
        $('.cancelButton', this.el).on('click', this.onCancelButtonClicked.bind(this));

        this.content.onFormSubmitSuccess = this.onFormSubmitSuccess.bind(this);
    },

    /**
     * Shows buttons according to crud type.
     */
    showCrudButtons: function () {
        var crudType = this.crudType;

        if (crudType === 'add') {
            $('.addButton', this.el).removeClass('hidden');
        }
        else if (crudType === 'edit') {
            $('.saveButton', this.el).removeClass('hidden');
        }
        else {
            throw new Error('showCrudButtons: wrong buttons type');
        }
    },

    /**
     * Save button click handler.
     * @param {Event} e
     */
    onSaveButtonClicked: function (e) {
        this.content.form.submit();
    },

    /**
     * Add button click handler.
     * @param {Event} e
     */
    onAddButtonClicked: function (e) {
        this.content.form.submit();
    },

    /**
     * Cancel button click handler.
     * @param {Event} e
     */
    onCancelButtonClicked: function (e) {
        this.close();
    },

    onFormSubmitSuccess: function () {
        this.close();
    }

});
/**
 * Created by yurii on 16.07.15.
 */

CJ.core.window.removeModelWindow.Widget = CJ.core.window.Widget.extend({

    classType: 'RemoveModelWindowWidget',

    /**
     * @cfg {Object} Backbone.Model
     */
    modelToRemove: null,

    initialize: function (cfg) {
        CJ.core.window.Widget.prototype.initialize.apply(this, arguments);

        this.modelToRemove = CJ.app.collections.getCollection(this.modelConfig.modelCollectionName, function (col) {
            this.modelToRemove = col.getModelById(this.modelConfig.modelId);
        }.bind(this));

    },

    initTemplate: function () {
        this.tpl = JST['js/core/window/removeModelWindow/RemoveModelWindow.tpl.html'];
    },

    getTplData: function () {
        return _.extend(CJ.core.window.Widget.prototype.getTplData.apply(this, arguments), {
            modelName: this.modelConfig.modelName
        });
    },

    initEvents: function () {
        CJ.core.window.Widget.prototype.initEvents.apply(this, arguments);

        $('.yesButton', this.el).on('click', this.onYesButtonClicked.bind(this));
        $('.cancelButton', this.el).on('click', this.onCancelButtonClicked.bind(this));
    },

    /**
     * Yes button click handler. Destroys model (if it is client than marks as removed).
     * @param {Event} e
     */
    onYesButtonClicked: function (e) {
        if (this.modelConfig.modelName === 'client') {
            this.modelToRemove.set({ removed: true });
            this.modelToRemove.save({}, {
                wait: true
            });
        }
        else {
            this.modelToRemove.destroy({
                success: function (model, response) {
                },
                error: function (model, error) {
                }
            });
        }

        this.close();
    },

    /**
     * Cancel button click handler.
     * @param {Event} e
     */
    onCancelButtonClicked: function (e) {
        this.close();
    }

});
/**
 * Created by yurii on 06.07.15.
 */

// Create the namespace for client
CJ.createNamespace("CJ.submodules.client");
/**
 * Created by yurii on 06.07.15.
 */

CJ.createNamespace('CJ.submodules');
/**
 * Created by yurii on 06.07.15.
 */

// Create the namespace
CJ.createNamespace("CJ.submodules.invoice");
/**
 * Created by yurii on 06.07.15.
 */

CJ.createNamespace('CJ.submodules.login');
/**
 * Created by yurii on 06.07.15.
 */

CJ.createNamespace('CJ.submodules.login.passwordRecovery');
/**
 * Created by yurii on 06.07.15.
 */

// Create the namespace for product
CJ.createNamespace("CJ.submodules.product");
/**
 * Created by yurii on 06.07.15.
 */

// Create the namespace for user
CJ.createNamespace("CJ.submodules.user");
/**
 * Created by yurii on 08.07.15.
 */

CJ.createNamespace("CJ.submodules.viewport");
/**
 * Created by mitfity on 29.06.2015.
 */

CJ.submodules.client.Model = Backbone.Model.extend({

    urlRoot: '/api/client',

    idAttribute: 'clientId',

    firstName: null,

    lastName: null,

    phone: null,

    email: null,

    organization: null,

    country: null,

    state: null,

    city: null,

    zip: null,

    firstAddress: null,

    secondAddress: null,

    defaults: {

        removed: false
    },

    initialize: function () {
    }

});
/**
 * Created by mitfity on 29.06.2015.
 */

CJ.submodules.invoice.Model = Backbone.Model.extend({

    urlRoot: '/api/invoice',

    idAttribute: 'invoiceId',

    userId: 0,

    clientId: 0,

    dateCreated: null,

    total: 0,

    initialize: function () {
    },

    // Was bug when u modify id and save model then route was /invoice/:newId. I need /invoice/:oldId.
    url: function () {
        if (this.isNew()) {
            return this.urlRoot;
        }

        if (!this.changed[this.idAttribute]) {
            return this.urlRoot + '/' + this.id;
        }
        else {
            return this.urlRoot + '/' + this._previousAttributes[this.idAttribute];
        }
    }

});
/**
 * Created by mitfity on 29.06.2015.
 */

CJ.submodules.product.Model = Backbone.Model.extend({

    invoiceId: undefined,

    productName: null,

    description: null,

    count: 0,

    price: 0,

    tax: 10,

    initialize: function () {
        this.set({ id: this.get('productId') });
        this.unset('productId');
    }

});
/**
 * Created by mitfity on 29.06.2015.
 */

CJ.submodules.user.Model = Backbone.Model.extend({

    urlRoot: '/api/user',

    idAttribute: 'userId',

    defaults: {

        email: null,

        firstName: null,

        lastName: null,

        avatar: 'https://s3.amazonaws.com/upload.uxpin/files/8669/12532/placeholder.png',

        phone: null,

        role: null,

        isActive: true
    },

    initialize: function () {
    }

});
/**
 * Created by yurii on 30.06.15.
 */

CJ.submodules.client.Collection = Backbone.Collection.extend({

    model: CJ.submodules.client.Model,

    url: '/api/client',

    initialize: function () {
        this.initEvents();
    },

    initEvents: function () {
        // Save model to local storage

        this.on('add', function (model, collection) {
            model.save({}, {
                success: function (model, response) {
                }
            });
        });

        this.on('change', function (model) {
            model.save(model.attributes, {
                wait: true,
                success: function (model, response) {
                },
                error: function (model, msg, args) {
                    console.log(msg);
                }
            });
        });
    },

    /**
     * @param {Number} id clientId
     * @returns {Object} Backbone.Model
     */
    getModelById: function (id) {
        return this.findWhere({ clientId: id });
    },

    /**
     * @param {String} email
     * @param {Number} modelId clientId of editting client
     * @returns {boolean}
     */
    hasModelWithEmail: function (email, modelId) {
        var models = this.filter(function (model) {
            return model.get('removed') === false && model.id !== modelId && model.get('email') === email;
        });

        return (models.length) ? true : false;
    },

    /**
     * @param {String} phone
     * @param {Number} modelId clientId of editting client
     * @returns {boolean}
     */
    hasModelWithPhone: function (phone, modelId) {
        var models = this.filter(function (model) {
            return model.get('removed') === false && model.id !== modelId && model.get('phone') === phone;
        });

        return (models.length) ? true : false;
    }

});
/**
 * Created by yurii on 30.06.15.
 */

CJ.submodules.invoice.Collection = Backbone.Collection.extend({

    model: CJ.submodules.invoice.Model,

    url: '/api/invoice',

    initialize: function () {
        this.initEvents();
    },

    initEvents: function () {
        // Save model to local storage

        this.on('add', function (model, collection) {
            model.save({}, {
                success: function (model, response) {
                }
            });
        });

        this.on('change', function (model) {
            model.save(model.attributes, {
                success: function (model, response) {
                },
                error: function (model, msg, args) {
                    console.log(msg);
                }
            });
        });
    },

    /**
     * @param {Number} id invoiceId
     * @returns {Object} Backbone.Model
     */
    getModelById: function (id) {
        return this.findWhere({ invoiceId: id });
    },

    /**
     * @param {Number} id invoiceId
     * @param {Number} modelId invoiceId of editting invoice
     * @returns {boolean}
     */
    hasModelWithId: function (id, modelId) {
        var models = this.filter(function (model) {
            return model.get('id') !== modelId && model.get('id') === id;
        });

        return (models.length) ? true : false;
    }

});
/**
 * Created by yurii on 30.06.15.
 */

CJ.submodules.product.Collection = Backbone.Collection.extend({

    model: CJ.submodules.product.Model,

    localStorage: new Backbone.LocalStorage("products-backbone"),

    initialize: function () {
        this.initEvents();
    },

    initEvents: function () {
        // Save model to local storage
        this.on('add change', function (model) {
            model.save();
        });
    },

    /**
     * @param {Nubmer} id productId
     * @returns {Object} Backbone,Model
     */
    getModelById: function (id) {
        return this.findWhere({ productId: id });
    },

    /**
     * Removes products of invoice by passed invoiceId.
     * @param invoiceId
     */
    removeModelsByInvoiceId: function (invoiceId) {
        this.where({ invoiceId: invoiceId }).forEach(function (el, index, array) {
            el.destroy();
        });
    }

});
/**
 * Created by mitfity on 30.06.2015.
 */

CJ.submodules.user.Collection = Backbone.Collection.extend({

    model: CJ.submodules.user.Model,

    url: '/api/user',

    initialize: function () {
        this.initEvents();
    },

    initEvents: function () {
        // Save model to local storage

        this.on('add', function (model, collection) {
            model.save({}, {
                success: function (model, response) {
                }
            });
        });

        this.on('change', function (model) {
            model.save(model.attributes, {
                wait: true,
                success: function (model, response) {
                },
                error: function (model, msg, args) {
                    console.log(msg);
                }
            });
        });
    },

    /**
     * @param {Number} id userId
     * @returns {Object} Backbone.Model
     */
    getModelById: function (id) {
        return this.findWhere({ userId: id });
    },

    /**
     * @param {String} email
     * @param {Number} modelId userId
     * @returns {boolean}
     */
    hasModelWithEmail: function (email, modelId) {
        var models = this.filter(function (model) {
            return model.id !== modelId && model.get('email') === email;
        });

        return (models.length) ? true : false;
    }

});
/**
 * Created by yurii on 15.07.15.
 */

CJ.submodules.client.FormWidget = CJ.core.form.Widget.extend({

    classType: 'ClientFormWidget',

    /**
     * @property {jQuery}
     */
    form: null,

    /**
     * @property {Object} editing model.
     */
    targetModel: null,

    /**
     * @property {Object} current form model.
     */
    currentModel: null,

    /**
     * @private
     */
    parentWindow: null,

    /**
     * @private
     */
    emailEl: null,

    /**
     * @private
     */
    phoneEl: null,

    /**
     * @cfg {boolean} True if u let form to be empty.
     */
    canBeEmpty: false,

    initialize: function (cfg) {
        $.extend(this, cfg);

        this.tpl = JST['js/submodules/client/Form.tpl.html'];
        this.initModels();
        CJ.core.form.Widget.prototype.initialize.apply(this, arguments);

        this.emailEl = $('input[name="email"]', this.form);
        this.phoneEl = $('input[name="phone"]', this.form);

        this.setValues(this.currentModel.attributes);
        if (this.canBeEmpty) {
            this.initOnDirtyEvents();
        }
        else {
            $('label[for=\"'+this.phoneEl.attr('id')+'\"].inputLabel').addClass('required');
            $('label[for=\"'+this.emailEl.attr('id')+'\"].inputLabel').addClass('required');
        }
    },

    initModels: function () {
        var modelId = this.modelId;

        this.currentModel = new CJ.submodules.client.Model();
        if (modelId !== undefined) {
            this.targetModel = new CJ.submodules.client.Model({
                clientId: modelId
            });

            this.targetModel.fetch({

                success: function (model, response) {
                    this.currentModel.set(model.attributes);
                    this.setValues(this.currentModel.attributes);
                }.bind(this),

                error: function (model, error) {
                    console.log('error');
                    console.log(error.responseText());
                    CJ.app.redirectToDefaultRoute();
                }

            });
        }
    },

    /**
     * This is custom jquery validation initialization.
     * @private
     */
    initOnDirtyEvents: function () {
        $('input', this.form).each(function (index, input) {
            $(input).on('input', function (e) {
                if ($(input).val()) {
                    this.phoneEl.rules('add', {
                        required: true
                    });
                    this.emailEl.rules('add', {
                        required: true
                    });

                    $('label[for=\"'+this.phoneEl.attr('id')+'\"].inputLabel').addClass('required');
                    $('label[for=\"'+this.emailEl.attr('id')+'\"].inputLabel').addClass('required');
                }
                else {
                    if (this.isEmpty()) {
                        this.phoneEl.rules('remove', 'required');
                        this.emailEl.rules('remove', 'required');

                        $('label[for=\"'+this.phoneEl.attr('id')+'\"].inputLabel').removeClass('required');
                        $('label[for=\"'+this.emailEl.attr('id')+'\"].inputLabel').removeClass('required');

                        this.phoneEl.valid();
                        this.emailEl.valid();
                    }
                }


            }.bind(this));
        }.bind(this));
    },

    /**
     * @private
     * @override CJ.core.form.Widget
     * @returns {*|jQuery|HTMLElement}
     */
    getForm: function () {
        return $('.editForm', this.el);
    },

    /**
     * @private
     * @override CJ.core.form.Widget
     * @returns {Object}
     */
    getValidationOptions: function () {
        var validationOptions = {

            rules: {

                firstName: {
                    required: false
                },

                lastName: {
                    required: false
                },

                phone: {
                    phone: true,
                    required: true
                },

                email: {
                    email: true,
                    required: true
                },

                organization: {
                    required: false
                },

                country: {
                    requiredGroup: {
                        className: 'addressGroup',
                        flagElementClassName: 'secondAddress'
                    },
                    required: false,
                    reValidate: true
                },

                state: {
                    requiredGroup: {
                        className: 'addressGroup',
                        flagElementClassName: 'secondAddress'
                    },
                    required: false,
                    reValidate: true
                },

                city: {
                    requiredGroup: {
                        className: 'addressGroup',
                        flagElementClassName: 'secondAddress'
                    },
                    required: false,
                    reValidate: true
                },

                zip:  {
                    requiredGroup: {
                        className: 'addressGroup',
                        flagElementClassName: 'secondAddress'
                    },
                    required: false,
                    reValidate: true
                },

                firstAddress: {
                    requiredGroup: {
                        className: 'addressGroup',
                        flagElementClassName: 'secondAddress'
                    },
                    required: false,
                    reValidate: true
                },

                secondAddress: {
                    requiredGroupFlagElement: 'addressGroup',
                    required: false,
                    reValidate: true
                }

            },
            messages: {

                phone: {
                    phone: 'Incorrect phone format.',
                    required: 'Enter your phone number.'
                },

                email: {
                    required: 'Enter your email.',
                    email: 'Enter valid email address.'
                },

                organization: {
                    required: 'Enter your organization.'
                },

                country: {
                    requiredGroup: 'Enter your country.'
                },

                state: {
                    requiredGroup: 'Enter your state.'
                },

                city: {
                    requiredGroup: 'Enter your city.'
                },

                zip: {
                    requiredGroup: 'Enter your zip code.'
                },

                firstAddress: {
                    requiredGroup: 'Enter your address.'
                }

            }
        };

        if (this.canBeEmpty) {
            _.extend(validationOptions.rules, {

                phone: {
                    phone: true,
                    required: false
                },

                email: {
                    email: true,
                    required: false
                }

            });
        }

        return validationOptions;
    },

    /**
     * @private
     * @override CJ.core.form.Widget
     * @param e
     * @returns {boolean}
     */
    onFormSubmit: function (e) {
        if (this.canBeEmpty) {
            if (!this.isEmpty()) {

            }
            else {
                this.onFormSubmitSuccess();
                return true;
            }
        }

        if (this.isValid()) {
            var responseError = null,
                validatorErrors = {},
                successFunc,
                errorFunc;

            this.currentModel.set(this.getValues());

            if (this.targetModel) {
                this.targetModel.set(this.currentModel.attributes);

                errorFunc = function (model, msg, args) {
                    responseError = JSON.parse(msg.responseText);
                    validatorErrors[responseError.field] = responseError.errorMessage;
                    this.validator.showErrors(validatorErrors);
                    this.onFormSubmitError();
                }.bind(this);

                successFunc = function (model, response) {
                    model.set(JSON.parse(response));
                    this.currentModel.set(model.attributes);
                    this.onFormSubmitSuccess(model);
                }.bind(this);

                this.targetModel.save({}, {
                    wait: true,
                    success: successFunc,
                    error: errorFunc
                });
            }
            else {
                errorFunc = function (model, msg, args) {
                    responseError = JSON.parse(msg.responseText);
                    validatorErrors[responseError.field] = responseError.errorMessage;
                    this.validator.showErrors(validatorErrors);
                    this.onFormSubmitError();
                }.bind(this);

                successFunc = function (model, response) {
                    model.set(JSON.parse(response));
                    this.currentModel = model;
                    this.onFormSubmitSuccess(model);
                }.bind(this);

                this.currentModel.save({}, {
                    wait: true,
                    success: successFunc,
                    error: errorFunc
                });
            }

        }

        this.onFormSubmitError();
        return false;
    },

    /**
     * This is called when form successfully submits.
     */
    onFormSubmitSuccess: function (model) {
    },

    /**
     * This is called when form submit is failed.
     */
    onFormSubmitError: function () {
    }

});
/**
 * Created by yurii on 03.07.15.
 */

CJ.submodules.client.ListWidget = CJ.core.View.extend( {

    classType: 'ClientListWidget',

    initialize: function (cfg) {
        this.tpl = JST['js/submodules/client/List.tpl.html'];

        CJ.core.View.prototype.initialize.apply(this, arguments);
        this.initGrid();
    },

    /**
     * @private
     */
    initGrid: function () {
        // Formatter for buttons column
        function buttonsFormatter(row, cell, value, columnDef, dataContext) {
            return value;
        }

        var columns = [
                {id: 'clientname', name: 'Client Name', field: 'clientname'},
                {id: 'phone', name: 'Phone', field: 'phone'},
                {id: 'email', name: 'E-mail', field: 'email', width: 100},
                {id: 'organization', name: 'Organization', field: 'organization', width: 50},
                {id: 'lastinvoice', name: 'Last Invoice', field: 'lastinvoice', width: 50},
                {id: 'totalspent', name: 'Total Spent Count', field: 'totalspent', width: 60},
                {id: 'buttons', name:'', field: 'buttons', formatter: buttonsFormatter, width: 45}
            ],
            options = {
                enableCellNavigation: true,
                enableColumnReorder: false,
                forceFitColumns: true
            },
            data = [],
            gridRenderTo = this.renderTo + ' .gridWrapper',
            clientsCollection = null,
            invoices,
            dateGreaterThan = function (firstDate, lastDate) {
                return new Date(firstDate) > new Date(lastDate);
            },
            clientInvoicesForEach;

        clientsCollection = CJ.app.collections.getCollection('clients', function (col) {
            var clients = col.models,
                clientInvoices,
                clientTotal = 0,
                clientLastInvoiceDate,
                dateCreatedBuffer;

            clientInvoicesForEach = function (el, index, coll) {
                clientTotal += el.get('total');

                if (!clientLastInvoiceDate) {
                    clientLastInvoiceDate = el.get('dateCreated');
                }
                else {
                    dateCreatedBuffer = el.get('dateCreated');

                    if (dateGreaterThan(dateCreatedBuffer, clientLastInvoiceDate)) {
                        clientLastInvoiceDate = dateCreatedBuffer;
                    }
                }
            };

            invoices = CJ.app.collections.getCollection('invoices', function (col) {
                invoices = col;

                // Fill data
                for (var i = 0; i < clients.length; i++) {
                    if (clients[i].get('removed') === true) {
                        continue;
                    }

                    clientTotal = 0;
                    clientLastInvoiceDate = null;
                    clientInvoices = invoices.where({ clientId: clients[i].id });
                    clientInvoices.forEach(clientInvoicesForEach);

                    data.push({

                        clientname: clients[i].get('firstName') + ' ' + clients[i].get('lastName'),

                        phone: clients[i].get('phone'),

                        email: clients[i].get('email'),

                        organization: clients[i].get('organization'),

                        lastinvoice: clientLastInvoiceDate,

                        totalspent: '$' + clientTotal.toFixed(2),

                        buttons: '<div class="buttons"><a href=\"#clients/edit/' + clients[i].id + '\"><span' +
                        ' class="glyphicon' +
                        ' glyphicon-pencil"></span></a>' +
                        '<a href=\"#clients/remove/' + clients[i].id  +'\"><span class="glyphicon' +
                        ' glyphicon-trash"></span></a></div>',

                        id: 'client' + clients[i].id

                    });
                }

                this.gridWidget = new CJ.core.grid.Widget({
                    gridData: data,
                    gridColumns: columns,
                    gridOptions: options,
                    renderTo: gridRenderTo
                });
            }.bind(this));

        }.bind(this));
    },

    /**
     * This method is called before remove(). It is used for removing references.
     * @private
     */
    onRemove: function () {
        if (this.gridWidget) {
            this.gridWidget.remove();
            this.gridWidget = null;
        }
    }

});
/**
 * Created by yurii on 17.07.15.
 */

CJ.submodules.invoice.CreationInfoFormWidget = CJ.core.form.Widget.extend({

    classType: 'CreationInfoFormWidget',

    /**
     * @property {jQuery}
     */
    form: null,

    /**
     * @private
     */
    parentWindow: null,

    /**
     * @private
     */
    dateFiled: null,

    initialize: function (cfg) {
        $.extend(this, cfg);

        this.tpl = JST['js/submodules/invoice/CreationInfoFormWidget.tpl.html'];
        this.initModels();
        CJ.core.form.Widget.prototype.initialize.apply(this, arguments);

        this.dateFiled = $('input[name="dateCreated"]', this.form);
        this.dateFiled.datepicker({
            onSelect: this.onDatePickerSelected.bind(this)
        });
    },

    initModels: function () {
        var modelId = this.modelId,
            userId = this.userId,
            dateCreated = this.dateCreated,
            invoices = CJ.app.collections.getCollection('invoices');

        this.currentModel = {};
        if (modelId !== undefined) {
            this.targetModel = {
                id: modelId,
                userId: userId,
                dateCreated: dateCreated
            };

            _.extend(this.currentModel, this.targetModel);
        }
        else {
            this.currentModel.id = invoices.length;
        }
    },

    /**
     * @private
     * @override CJ.core.View
     * @returns {Object}
     */
    getTplData: function () {
        var user = CJ.app.collections.getCollection('users').getModelById(this.userId),
            usename = user.get('firstName') + ' ' + user.get('lastName'),
            invoiceId = this.currentModel.id,
            date = new Date(),
            today,
            paddingWithZeros = CJ.core.utils.paddingWithZeros;

        invoiceId = paddingWithZeros(id, 10);

        today = paddingWithZeros(date.getMonth() + 1, 2) + '/' + paddingWithZeros(date.getDate(), 2) + '/' +
            date.getFullYear();

        return {
            username: usename || 'user name',
            invoiceId: invoiceId,
            dateCreated: this.dateCreated || today,
            userId: user.get('id')
        };
    },

    /**
     * @private
     * @override CJ.core.form.Widget
     * @returns {*|jQuery|HTMLElement}
     */
    getForm: function () {
        return $('.editForm', this.el);
    },

    /**
     * @private
     * @override CJ.core.form.Widget
     */
    getValidationOptions: function () {
        return {

            rules: {

                invoiceId: {
                    required: true,
                    number: true
                },

                dateCreated: {
                    required: true,
                    realDate: true
                }

            },
            messages: {

                invoiceId: {
                    required: 'Enter invoice ID.',
                    number: 'Invoice ID has to be a number.'
                },

                dateCreated: {
                    required: 'Enter date.',
                    realDate: 'Enter valid date.'
                }

            }
        };
    },

    /**
     * @private
     * @param date
     */
    onDatePickerSelected: function (date) {
        this.dateFiled.valid();
    },

    /**
     * @private
     * @override CJ.core.form.Widget
     * @param e
     * @returns {boolean}
     */
    onFormSubmit: function (e) {
        if (this.isValid()) {
            var invoices = CJ.app.collections.getCollection('invoices'),
                alreadyExists = false,
                formValues = this.getValues();

            _.extend(this.currentModel, formValues);
            this.currentModel.userId = parseInt(this.currentModel.userId, 10);
            this.currentModel.id = parseInt(this.currentModel.id, 10);

            if (this.targetModel) {
                if ( invoices.hasModelWithId(this.currentModel.id, this.targetModel.id) ) {
                    this.validator.showErrors({ invoiceId: 'Such id already exists.' });
                    alreadyExists = true;
                }
            }
            else {
                if ( invoices.hasModelWithId(this.currentModel.id) ) {
                    this.validator.showErrors({ invoiceId: 'Such id already exists.' });
                    alreadyExists = true;
                }
            }



            if (alreadyExists) {
                return false;
            }

            if (!this.targetModel) {
                this.targetModel = {};
            }
            _.extend(this.targetModel, this.currentModel);

            this.onFormSubmitSuccess();
            return true;
        }

        return false;
    },

    /**
     * This is called when form successfully submitted.
     */
    onFormSubmitSuccess: function () {
    }

});
/**
 * Created by yurii on 16.07.15.
 */

CJ.submodules.invoice.CrudWidget = CJ.core.form.Widget.extend( {

    classType: 'InvoiceCrudWidget',

    /**
     * @private
     */
    clientFormWidget: null,

    /**
     * @private
     */
    checkByEmail: false,

    /**
     * @private
     */
    checkByPhone: false,

    /**
     * @private
     */
    creationInfoFormWidget: null,

    /**
     * @private
     */
    productsGridWidget: null,

    /**
     * @property {Object} current form invoice model
     */
    currentModel: null,

    /**
     * @property {Object} editing invoice model
     */
    targetModel: null,

    /**
     * @property {jQuery}
     */
    form: null,

    initialize: function (cfg) {
        this.tpl = JST['js/submodules/invoice/Crud.tpl.html'];
        CJ.core.form.Widget.prototype.initialize.apply(this, arguments);

        this.checkByEmail = false;
        this.checkByPhone = false;

        this.initModels();
    },

    /**
     * @private
     * @override CJ.core.View
     * @returns {Object}
     */
    getTplData: function () {
        return {
            title: this.contentTitle || 'Title '
        };
    },

    initInvoiceEvents: function () {
        $('.saveButton', this.el).on('click', this.onSaveButtonClicked.bind(this));
        this.clientFormWidget.emailEl.on('input', this.onFormEmailInput.bind(this));
        this.clientFormWidget.phoneEl.on('input', this.onFormPhoneInput.bind(this));
        $(this.el).on('click', '.removeItem', this.onRemoveItemClicked.bind(this));
        $('.addLineButton', this.el).on('click', this.onAddLineButtonClicked.bind(this));

        this.dateFiled = $('input[name="dateCreated"]', this.form);
        this.dateFiled.datepicker({
            onSelect: this.onDatePickerSelected.bind(this)
        });
    },

    initModels: function () {
        var modelId = this.modelId,
            invoices,
            invoiceId,
            user,
            username,
            date = new Date(),
            today,
            paddingWithZeros = CJ.core.utils.paddingWithZeros,
            onModelFetchSuccess;

        this.currentModel = new CJ.submodules.invoice.Model();
        if (modelId !== undefined) {
            this.targetModel = new CJ.submodules.invoice.Model({
                invoiceId: modelId
            });

            this.targetModel.fetch({

                wait: true,

                success: function (model, response) {
                    this.targetModel = model;

                    if (!this.targetModel) {
                        CJ.app.redirectToDefaultRoute();
                    }

                    this.currentModel.set(this.targetModel.attributes);

                    onModelFetchSuccess();
                }.bind(this),

                error: function (model, response) {
                }.bind(this)

            });


        }
        else {
            invoices = CJ.app.collections.getCollection('invoices', function (col) {
                invoices = col;

                var maxInvoiceId = invoices.length + 1,
                    newInvoiceId = maxInvoiceId;
                if (invoices.findWhere({
                        invoiceId: maxInvoiceId
                    })) {
                    maxInvoiceId = 0;
                    invoices.forEach(function (el, index, col) {
                        if (el.get('invoiceId') > maxInvoiceId) {
                            maxInvoiceId = el.get('invoiceId');
                        }
                    });
                    newInvoiceId = maxInvoiceId + 1;
                }
                this.currentModel.set({ invoiceId: newInvoiceId });
                this.currentModel.set({ userId: CJ.app.userIdentity.userId });

                onModelFetchSuccess();
            }.bind(this));
        }

        onModelFetchSuccess = function () {
            invoiceId = this.currentModel.id;
            user = new CJ.submodules.user.Model({ userId: this.currentModel.get('userId') });

            user.fetch({

                wait: true,

                success: function (model, response) {
                    invoiceId = paddingWithZeros(invoiceId, 10);
                    this.currentModel.set({
                        invoiceId: invoiceId
                    });

                    today = paddingWithZeros(date.getMonth() + 1, 2) + '/' + paddingWithZeros(date.getDate(), 2) + '/' +
                        date.getFullYear();

                    username = model.get('firstName') + ' ' + model.get('lastName');

                    if (!this.currentModel.get('dateCreated')) {
                        this.currentModel.set({
                            dateCreated: today
                        });
                    }

                    this.setValues(this.currentModel.attributes);

                    $('span#username', this.el).text(username);

                    this.initWidgets();
                    this.initInvoiceEvents();
                    this.initAutocomplete();
                }.bind(this),

                error: function (model, response) {

                }.bind(this)

            });
        }.bind(this);

    },

    getValues: function () {
        var values = CJ.core.form.Widget.prototype.getValues.apply(this, arguments);
        $.extend(values, {

            invoiceId: parseInt(CJ.core.utils.paddingWithZeros(values.invoiceId, 0))

        });

        return values;
    },

    /**
     * @private
     * @override CJ.core.form.Widget
     * @returns {*|jQuery|HTMLElement}
     */
    getForm: function () {
        return $('.invoiceInfoForm', this.el);
    },

    /**
     * @private
     * @override CJ.core.form.Widget
     */
    getValidationOptions: function () {
        return {

            rules: {

                invoiceId: {
                    required: true,
                    number: true
                },

                dateCreated: {
                    required: true,
                    realDate: true
                }

            },
            messages: {

                invoiceId: {
                    required: 'Enter invoice ID.',
                    number: 'Invoice ID has to be a number.'
                },

                dateCreated: {
                    required: 'Enter date.',
                    realDate: 'Enter valid date.'
                }

            }
        };
    },

    /**
     * @private
     * @param date
     */
    onDatePickerSelected: function (date) {
        this.dateFiled.valid();
    },

    initWidgets: function () {
        var clientRenderTo = this.renderTo + ' .clientForm';

        this.clientFormWidget = new CJ.submodules.client.FormWidget({
            renderTo: clientRenderTo,
            modelId: this.currentModel.get('clientId'),
            canBeEmpty: true
        });

        this.clientFormWidget.disableSubmitOnEnter();

        this.initProductsGrid();
    },

    initProductsGrid: function () {
        var requiredNameFormatter = CJ.core.configs.slickgridValidationConfig.requiredNameFormatter,
            numberFormatter = CJ.core.configs.slickgridValidationConfig.numberFormatter,
            priceFormatter = CJ.core.configs.slickgridValidationConfig.priceFormatter,
            percentageFormatter = CJ.core.configs.slickgridValidationConfig.percentageFormatter,
            positiveFloatEditor = CJ.core.configs.slickgridValidationConfig.positiveFloatEditor,
            htmlFormatter = CJ.core.configs.slickgridValidationConfig.htmlFormatter,
            formatterLineTotal = CJ.core.configs.slickgridValidationConfig.formatterLineTotal,
            productsAutocompleteEditor = CJ.core.configs.slickgridValidationConfig.productsAutocompleteEditor,
            columns = [
                {id: 'name', name: 'Name', field: 'name', width: 50, editor: productsAutocompleteEditor,
                    formatter: requiredNameFormatter, resizable: false},
                {id: 'description', name: 'Description', field: 'description', width: 100, editor: Slick.Editors.Text,
                    resizable: false},
                {id: 'price', name: 'Price', field: 'price', width: 50, editor: positiveFloatEditor,
                    formatter: priceFormatter, resizable: false},
                {id: 'count', name: 'Qnt', field: 'count', width: 10, editor: positiveFloatEditor,
                    formatter: numberFormatter, resizable: false},
                {id: 'tax', name:'Tax', field: 'tax', minWidth: 200, maxWidth: 200, width: 200,
                    editor: positiveFloatEditor, formatter: percentageFormatter, resizable: false},
                {id: 'total', name:'Line Total', field: 'total', minWidth: 200, maxWidth: 200, width: 50,
                    formatter: formatterLineTotal, resizable: false},
                {id: 'buttons', name: '', field: 'buttons', minWidth: 70, maxWidth: 70, width: 70,
                    formatter: htmlFormatter, resizable: false}
            ],
            options = {
                editable: true,
                enableAddRow: true,
                asyncEditorLoading: true,
                enableCellNavigation: true,
                enableColumnReorder: false,
                forceFitColumns: true,
                autoHeight: true
            },
            data = [],
            gridRenderTo = this.renderTo + ' .productsGridWrapper',
            productsFetch,
            products = this.currentModel.get('invoiceItems') || [];

            if (products.length) {
                // Fill data
                for (var i = 0; i < products.length; i++) {
                    data[i] = {
                        name: products[i].name,
                        description: products[i].description,
                        price: products[i].price,
                        count: products[i].count,
                        tax: products[i].tax,
                        buttons: '<div class="removeItem"><span class="glyphicon glyphicon-trash "></span></div>',
                        id: 'product' + products[i].invoiceItemId
                    };
                }
            }

            this.productsGridWidget = new CJ.core.grid.Widget({
                gridData: data,
                gridColumns: columns,
                gridOptions: options,
                renderTo: gridRenderTo
            });

            this.productsGridWidget.onGridAddNewRow = this.onAddNewRow.bind(this);

            this.productsGridWidget.onGridCellChange = this.onCellChange.bind(this);

            this.productsGridWidget.onDataviewRowsChanged = this.onRowsChanged.bind(this);

            this.updateInvoiceTotal();
    },

    /**
     * @private
     * @param e
     * @param args
     */
    onRemoveItemClicked: function (e, args) {
        this.productsGridWidget.removeActiveRow();
        this.updateInvoiceTotal();
    },

    /**
     * @private
     * @param e
     * @param args
     */
    onRowsChanged: function (e, args) {
        this.updateInvoiceTotal();
    },

    /**
     * @private
     * @param e
     */
    onAddLineButtonClicked: function (e) {
        var dataView = this.productsGridWidget.gridDataView,
            rowsCount = dataView.getLength(),
            item = {
                name: '',
                price: 0,
                count: 1,
                tax: 10.0,
                total: 0,
                buttons: '<div class="removeItem"><span class="glyphicon glyphicon-trash "></span></div>',
                id: 'product' + rowsCount
            };

        dataView.addItem(item);
        this.updateInvoiceTotal();

        $('html, body').animate({
            scrollTop: $('.addLineButton', this.el).offset().top
        }, 0);
    },

    /**
     * @private
     * @param e
     * @param args
     */
    onAddNewRow: function (e, args) {
        var rowsCount = this.productsGridWidget.gridDataView.getLength(),
            item = {
                name: '',
                price: 0,
                count: 1,
                tax: 10.0,
                total: 0,
                buttons: '<div class="removeItem"><span class="glyphicon glyphicon-trash "></span></div>',
                id: 'product' + rowsCount
            };

        $.extend(item, args.item);
        this.productsGridWidget.gridDataView.addItem(item);
        this.updateInvoiceTotal();
    },

    /**
     * @private
     * @param e
     * @param args
     */
    onCellChange: function (e, args) {
        this.updateInvoiceTotal();
    },

    /**
     * @private
     */
    updateInvoiceTotal: function () {
        $('.totalValue', this.el).text("$" + this.calcInvoiceTotal());
    },

    /**
     * @private
     * @returns {string}
     */
    calcInvoiceTotal: function () {
        var rows = this.productsGridWidget.gridDataView.getItems(),
            total = 0;

        for (var i = 0; i < rows.length; i++) {
            total += rows[i].total;
        }

        return total.toFixed(2);
    },

    /**
     * @private
     */
    initAutocomplete: function () {
        var clients,
            emailEl = this.clientFormWidget.emailEl,
            phoneEl = this.clientFormWidget.phoneEl,
            clientsEmailAutocomplete,
            clientsPhoneAutocomplete;

        clients = CJ.app.collections.getCollection('clients', function (col) {
            // Prevent autocomplete from banned users
            clients = new CJ.submodules.client.Collection(col.where({ removed: 0 }));

            // Set up collection for autocomplete usage.
            clientsEmailAutocomplete = clients.toJSON();
            clientsEmailAutocomplete.forEach(function (el, index, col) {
                el.label = el.email;
            });

            emailEl.autocomplete({
                minLength: 0,
                source: clientsEmailAutocomplete,
                select: function (event, ui) {
                    emailEl.val(ui.item.email);
                    this.autocompleteByEmail(clients, ui.item.email);
                    this.clientFormWidget.form.valid();
                    return false;
                }.bind(this)
            })
                .autocomplete("instance")._renderItem = function (ul, item) {
                return $("<li>")
                    .append("<a>" + item.email + "<br>" + item.firstName + ' ' + item.lastName +
                    "</a>")
                    .appendTo(ul);
            };

            // Set up collection for autocomplete usage.
            clientsPhoneAutocomplete = clients.toJSON();
            clientsPhoneAutocomplete.forEach(function (el, index, col) {
                el.label = el.phone;
            });

            phoneEl.autocomplete({
                minLength: 0,
                source: clientsPhoneAutocomplete,
                select: function (event, ui) {
                    phoneEl.val(ui.item.phone);
                    this.autocompleteByPhone(clients, ui.item.phone);
                    this.clientFormWidget.form.valid();
                    return false;
                }.bind(this)
            })
                .autocomplete("instance")._renderItem = function (ul, item) {
                return $("<li>")
                    .append("<a>" + item.phone + "<br>" + item.firstName + ' ' + item.lastName +
                    "</a>")
                    .appendTo(ul);
            };
        }.bind(this));
    },

    /**
     * @private
     * @param collection
     * @param email
     */
    autocompleteByEmail: function (collection, email) {
        var model = collection.findWhere({ email: email });

        if (model) {
            this.clientFormWidget.setValues(model.attributes);
            this.checkByPhone = true;
            this.checkByEmail = true;
        }
    },

    /**
     * @private
     * @param collection
     * @param phone
     */
    autocompleteByPhone: function (collection, phone) {
        var model = collection.findWhere({ phone: phone });

        if (model) {
            this.clientFormWidget.setValues(model.attributes);
            this.checkByEmail = true;
            this.checkByPhone = true;
        }
    },

    /**
     * @private
     * @param e
     */
    onSaveButtonClicked: function (e) {
        var clientForm = this.clientFormWidget,
            productsValues,
            products = [],
            clientId = null,
            successFunc,
            errorFunc,
            validatorErrors = {},
            responseError,
            targetModelAttributesBuffer;

        if (this.productsGridWidget.isValid()) {

            this.submitClientForm(function (clientModel) {
                productsValues = this.productsGridWidget.getValues();

                productsValues.forEach(function (item, index, coll) {
                    _.extend(item, {
                        invoiceId: this.currentModel.get('id')
                    });

                    products.push(new CJ.submodules.product.Model(item));
                }.bind(this));

                clientId = clientModel ? clientModel.id : null;
                this.currentModel.set(this.getValues());
                this.currentModel.set({
                    clientId: clientId,
                    invoiceItems: products,
                    invoiceId: parseInt(this.currentModel.get('invoiceId'))
                });

                if (this.targetModel) {
                    targetModelAttributesBuffer = {};
                    $.extend(targetModelAttributesBuffer, this.targetModel.attributes);
                    this.targetModel.set(this.currentModel.attributes);

                    errorFunc = function (model, msg, args) {
                        responseError = JSON.parse(msg.responseText);
                        validatorErrors[responseError.field] = responseError.errorMessage;
                        this.validator.showErrors(validatorErrors);

                        // error
                        if ($('input.invalid', this.el)[0]) {
                            $('html, body').animate({
                                scrollTop: $('input.invalid', this.el).offset().top - 100
                            }, 0);
                        }

                        // Prevent bug when submiting second time it submits on updating invoice with
                        // target invoice id rather then with current, cause it thinks we didn't change invoice id.
                        this.targetModel.set(targetModelAttributesBuffer);
                        targetModelAttributesBuffer = null;
                    }.bind(this);

                    successFunc = function (model, response) {
                        this.currentModel.set(this.targetModel.attributes);

                        CJ.app.router.navigate('invoices/list', {
                            trigger: true
                        });
                    }.bind(this);

                    this.targetModel.save({}, {
                        wait: true,
                        success: successFunc,
                        error: errorFunc
                    });
                }
                else {
                    // Let backbone send post requests
                    this.currentModel.id = undefined;
                    this.currentModel.idAttribute = null;

                    errorFunc = function (model, msg, args) {
                        responseError = JSON.parse(msg.responseText);
                        validatorErrors[responseError.field] = responseError.errorMessage;
                        this.validator.showErrors(validatorErrors);

                        // error
                        if ($('input.invalid', this.el)[0]) {
                            $('html, body').animate({
                                scrollTop: $('input.invalid', this.el).offset().top - 100
                            }, 0);
                        }

                    }.bind(this);

                    successFunc = function (model, response) {
                        this.currentModel = model;

                        CJ.app.router.navigate('invoices/list', {
                            trigger: true
                        });
                    }.bind(this);

                    this.currentModel.save({}, {
                        wait: true,
                        success: successFunc,
                        error: errorFunc
                    });

                    // Let backbone send put requests
                    this.currentModel.idAttribute = 'invoiceId';
                }


            }.bind(this), function () {
                // error
                if ($('input.invalid', this.el)[0]) {
                    $('html, body').animate({
                        scrollTop: $('input.invalid', this.el).offset().top - 100
                    }, 0);
                }
            }.bind(this));

        }
        else {
            // error
            if ($('.invalidInput', this.el)[0]) {
                $('html, body').animate({
                    scrollTop: $('.invalidInput', this.el).offset().top - 100
                }, 0);
            }
        }
    },

    /**
     * @private
     * @param e
     */
    onFormEmailInput: function (e) {
        this.checkByEmail = false;
    },

    /**
     * @private
     * @param e
     */
    onFormPhoneInput: function (e) {
        this.checkByPhone = false;
    },

    /**
     * @private
     * @param success
     * @param error
     */
    submitClientForm: function (success, error) {
        var clients,
            targetModel,
            formValues = this.clientFormWidget.getValues();

        clients = CJ.app.collections.getCollection('clients', function (col) {
            clients = col;

            if (this.checkByEmail && this.checkByPhone) {
                if (formValues.email && formValues.phone) {
                    targetModel = clients.findWhere({ email: formValues.email, phone: formValues.phone, removed: 0 });
                }
            }
            else if (this.checkByEmail) {
                if (formValues.email) {
                    targetModel = clients.findWhere({ email: formValues.email, removed: 0 });
                }
            }
            else if (this.checkByPhone) {
                if (formValues.phone) {
                    targetModel = clients.findWhere({ phone: formValues.phone, removed: 0 });
                }
            }

            if (targetModel) {
                this.clientFormWidget.setTargetModel(targetModel);
            }

            this.clientFormWidget.onFormSubmitSuccess = success;
            this.clientFormWidget.onFormSubmitError = error;
            this.clientFormWidget.form.submit();
        }.bind(this));

    }

});
/**
 * Created by yurii on 03.07.15.
 */

CJ.submodules.invoice.ListWidget = CJ.core.View.extend( {

    classType: 'InvoiceListWidget',

    initialize: function (cfg) {
        this.tpl = JST['js/submodules/invoice/List.tpl.html'];

        CJ.core.View.prototype.initialize.apply(this, arguments);
        this.initGrid();
    },

    /**
     * @private
     */
    initGrid: function () {
        // Formatter for buttons column
        function buttonsFormatter(row, cell, value, columnDef, dataContext) {
            return value;
        }

        var columns = [
                {id: 'invoiceid', name: 'Invoice #', field: 'invoiceid', width: 50},
                {id: 'clientname', name: 'Client Name', field: 'clientname'},
                {id: 'datecreated', name: 'Date', field: 'datecreated', width: 55},
                {id: 'totalprice', name: 'Total', field: 'totalprice', width: 60},
                {id: 'buttons', name:'', field: 'buttons', formatter: buttonsFormatter, width: 25}
            ],
            options = {
                enableCellNavigation: true,
                enableColumnReorder: false,
                forceFitColumns: true
            },
            data = [],
            gridRenderTo = this.renderTo + ' .gridWrapper',
            invoicesCollection;

        invoicesCollection = CJ.app.collections.getCollection('invoices', function (col) {
            var invoices = col.models,
                clients;

            clients = CJ.app.collections.getCollection('clients', function (clientsCollection) {

                // Fill data
                for (var i = 0; i < invoices.length; i++) {
                    var client = clientsCollection.findWhere({ clientId: invoices[i].get('clientId') });

                    data[i] = {

                        invoiceid: CJ.core.utils.paddingWithZeros(invoices[i].id, 8),

                        datecreated: invoices[i].get('dateCreated'),

                        totalprice: '$' + invoices[i].get('total').toFixed(2),

                        buttons: '<div class="buttons"><a href=\"#invoices/edit/' + invoices[i].id + '\"><span' +
                        ' class="glyphicon glyphicon-edit"></span></a>' +
                        '<a href=\"#invoices/remove/' + invoices[i].id + '\"><span' +
                        ' class="glyphicon' +
                        ' glyphicon-trash"></span></a></div>',

                        id: 'invoice' + invoices[i].id

                    };

                    if (client) {
                        data[i].clientname = client.get('firstName') + ' ' + client.get('lastName');
                    }
                    else {
                        data[i].clientname = '';
                    }
                }

                this.gridWidget = new CJ.core.grid.Widget({
                    gridData: data,
                    gridColumns: columns,
                    gridOptions: options,
                    renderTo: gridRenderTo
                });
            }.bind(this));
        }.bind(this));
    },

    /**
     * This method is called before remove(). It is used for removing references.
     * @private
     */
    onRemove: function () {
        if (this.gridWidget) {
            this.gridWidget.remove();
            this.gridWidget = null;
        }
    }

});
/**
 * Created by yurii on 06.07.15.
 */

CJ.submodules.login.Widget = CJ.core.form.Widget.extend({

    classType: 'LoginWidget',

    initialize: function () {
        this.tpl = JST['js/submodules/login/Login.tpl.html'];
        CJ.core.form.Widget.prototype.initialize.apply(this, arguments);
    },

    /**
     * @override CJ.core.form.Widget
     */
    getForm: function () {
        return $('.loginForm', this.el);
    },

    /**
     * @override CJ.core.form.Widget
     */
    getValidationOptions: function () {
        return {
            rules: {

                email: {
                    required: true,
                    email: true
                },

                password: 'required'

            },

            messages: {
                email: {
                    required: 'Please enter your email.',
                    email: 'Please enter valid email address.'
                },
                password: 'Please enter your password.'
            }
        };
    },

    /**
     * @override CJ.core.form.Widget
     */
    onFormSubmit: function (e) {
        var validatorErrors = {},
            responseError;

        if (this.isValid()) {
            var success = function () {
                    CJ.app.redirectToDefaultRoute();
                },
                error = function (msg, args) {
                    responseError = JSON.parse(msg.responseText);

                    validatorErrors[responseError.field] = responseError.errorMessage;
                    this.validator.showErrors(validatorErrors);

                    $('input[name="password"]', this.el).val('');
                };

            CJ.app.userIdentity.logIn(this.getValues(), success.bind(this), error.bind(this));
        }
    }

});
/**
 * Created by yurii on 06.07.15.
 */

CJ.submodules.login.passwordRecovery.Widget = CJ.core.form.Widget.extend({

    classType: 'PasswordRecoveryWidget',

    initialize: function () {
        this.tpl = JST['js/submodules/login/passwordRecovery/PasswordRecovery.tpl.html'];

        CJ.core.form.Widget.prototype.initialize.apply(this, arguments);
    },

    /**
     * @override CJ.core.form.Widget
     */
    getForm: function () {
        return $('.passwordRecoveryForm', this.el);
    },

    /**
     * @override CJ.core.form.Widget
     */
    getValidationOptions: function () {
        return {
            rules: {

                email: {
                    required: true,
                    email: true
                }

            },

            messages: {
                email: {
                    required: 'Please enter your email.',
                    email: 'Please enter valid email address.'
                }
            }
        };
    },

    /**
     * @override CJ.core.form.Widget
     */
    onFormSubmit: function (e) {
        var email = this.getValues().email,
            validatorErrors = {},
            responseError;

        if (this.isValid()) {
            $.ajax({
                method: "POST",
                url: "http://cashbox.loc/api/user/reset",
                data: JSON.stringify(email),
                dataType: "json"
            })
                .done(function (user, msg, args) {
                    CJ.app.redirectToDefaultRoute();
                }.bind(this))
                .fail(function (msg, args) {
                    responseError = JSON.parse(msg.responseText);

                    validatorErrors[responseError.field] = responseError.errorMessage;
                    this.validator.showErrors(validatorErrors);
                }.bind(this));
        }
    }

});
/**
 * Created by yurii on 13.07.15.
 */

CJ.submodules.user.FormWidget = CJ.core.form.Widget.extend({

    classType: 'UserFormWidget',

    /**
     * @property {jQuery}
     */
    form: null,

    /**
     * @property {Object} Editing model.
     */
    targetModel: null,

    /**
     * @property {Object} Current model of the form. It acts as a buffer.
     */
    currentModel: null,

    /**
     * @private
     */
    parentWindow: null,

    initialize: function (cfg) {
        $.extend(this, cfg);

        this.tpl = JST['js/submodules/user/Form.tpl.html'];
        CJ.core.form.Widget.prototype.initialize.apply(this, arguments);
        this.initModels();

        this.manageInputsDisplay();
    },

    initEvents: function () {
        CJ.core.form.Widget.prototype.initEvents.apply(this, arguments);

        $('.btn-file', this.el).on('change', this.onFileSelected.bind(this));

        $('#phone').keypress( function(e) {
            var chr = String.fromCharCode(e.which);
            if ("0123456789-() ".indexOf(chr) < 0)
                return false;
        });

        $('.passwordGroup input', this.form).each(function (index, input) {
            var arePasswordsEmpty = true;

            $(input).on('input', function (e) {
                if ($(input).val()) {
                    $('.passwordGroup input', this.form).each(function (relativeIndex, relativeInput) {
                        $(relativeInput).rules('add', {
                            required: true
                        });

                        $('label[for=\"'+$(relativeInput).attr('id')+'\"].inputLabel').addClass('required');
                    });
                }
                else {
                    $('.passwordGroup input', this.form).each(function (relativeIndex, relativeInput) {
                        if ($(relativeInput).val()) {
                            arePasswordsEmpty = false;
                            return false;
                        }
                    });

                    if (arePasswordsEmpty) {
                        $('.passwordGroup input', this.form).each(function (relativeIndex, relativeInput) {
                            $(relativeInput).rules('remove', 'required');
                            $('label[for=\"'+$(relativeInput).attr('id')+'\"].inputLabel').removeClass('required');
                            $(relativeInput).valid();
                        });
                    }
                }


            }.bind(this));
        }.bind(this));
    },

    initModels: function () {
        var modelId = this.modelId;

        this.currentModel = new CJ.submodules.user.Model();
        if (modelId !== undefined) {
            this.targetModel = new CJ.submodules.user.Model({
                userId: modelId
            });
            this.targetModel.fetch({

                success: function (model, response) {
                    this.currentModel.set(model.attributes);
                    this.setValues(this.currentModel.attributes);

                    $('.userAvatar img', this.el).attr('src', this.currentModel.get('avatar'));
                }.bind(this),

                error: function (model, error) {
                    console.log('error');
                    console.log(error.responseText());
                    CJ.app.redirectToDefaultRoute();
                }

            });
        }
        else {
            $('.userAvatar img', this.el).attr('src', this.currentModel.get('avatar'));
        }
    },

    /**
     * Shows some hidden inputs according to if it is profile or not.
     * @private
     */
    manageInputsDisplay: function () {
        if (this.isProfile) {
            $('.roleFormGroup', this.el).remove();
            $('.passwordGroup', this.el).removeAttr('hidden');
            $('.password', this.el).val('');
            $('.passwordRepeat', this.el).val('');
        }
    },

    /**
     * @private
     * @param {Event} e
     */
    onFileSelected: function (e) {
        var files = e.target.files;
        var file = files[0];

        if (files && file) {
            var reader = new FileReader();

            reader.onload = function(readerEvt) {
                var img = new Image();

                img.src = readerEvt.target.result;
                $('.userAvatar img', this.el).prop('src', img.src);
                this.currentModel.set({
                    avatar: img.src
                });
            }.bind(this);

            reader.readAsDataURL(file);
        }
    },

    /**
     * @private
     * @override CJ.core.form.Widget
     * @returns {*|jQuery|HTMLElement}
     */
    getForm: function () {
        return $('.editForm', this.el);
    },

    /**
     * @private
     * @override CJ.core.form.Widget
     * @returns {Object}
     */
    getValidationOptions: function () {
        var validationOptions = {
            rules: {

                firstName: 'required',

                lastName: 'required',

                phone: {
                    required: true
                },

                email: {
                    required: true,
                    email: true
                }

            },
            messages: {

                firstName: {
                    required: 'Please enter your first name'
                },

                lastName: {
                    required: 'Please enter your last name'
                },

                phone: {
                    required: 'Please enter your phone number.'
                },

                email: {
                    required: 'Please enter your email.',
                    email: 'Please enter valid email address.'
                }
            }
        };

        if (this.isProfile) {
            _.extend(validationOptions.rules, {

                oldPassword: {
                    required: false
                },

                newPassword: {
                    required: false
                },

                passwordRepeat: {
                    required: false,
                    equalTo: '#newPassword'
                }

            });

            _.extend(validationOptions.messages, {

                oldPassword: {
                    required: 'Please enter old password.'
                },

                newPassword: {
                    required: 'Please enter new password.'
                },

                passwordRepeat: {
                    required: 'Please repeat your password.',
                    equalTo: 'Passwords have to be equal.'
                }

            });
        }

        return validationOptions;
    },

    /**
     * @private
     * @ovarride CJ.core.form.Widget
     * @param e
     * @returns {boolean}
     */
    onFormSubmit: function (e) {
        if (this.isValid()) {
            var responseError = null,
                validatorErrors = {},
                successFunc,
                errorFunc;

            this.currentModel.set(this.getValues());

            if (this.targetModel) {
                this.targetModel.set(this.currentModel.attributes);

                errorFunc = function (model, msg, args) {
                    responseError = JSON.parse(msg.responseText);
                    validatorErrors[responseError.field] = responseError.errorMessage;
                    this.validator.showErrors(validatorErrors);
                    this.onFormSubmitError();
                }.bind(this);

                successFunc = function (model, response) {
                    this.onFormSubmitSuccess();
                }.bind(this);

                this.targetModel.save({}, {
                    wait: true,
                    success: successFunc,
                    error: errorFunc
                });
            }
            else {
                errorFunc = function (model, msg, args) {
                    responseError = JSON.parse(msg.responseText);
                    validatorErrors[responseError.field] = responseError.errorMessage;
                    this.validator.showErrors(validatorErrors);
                    this.onFormSubmitError();
                }.bind(this);

                successFunc = function (model, response) {
                    var notifyPasswordSent = new CJ.core.window.Widget({
                        title: 'Password',
                        contentTpl: _.template('<p>Password has been sent to your email.</p>')
                    });
                    notifyPasswordSent.show();

                    setTimeout(function () {
                        notifyPasswordSent.close();
                        this.onFormSubmitSuccess();
                    }.bind(this), 3500);
                }.bind(this);

                this.currentModel.save({}, {
                    wait: true,
                    success: successFunc,
                    error: errorFunc
                });
            }

            return true;


        }

        return false;
    },

    /**
     * It is called when form is submitted successfully.
     */
    onFormSubmitSuccess: function () {
    },

    /**
     * This is called when form submit is failed.
     */
    onFormSubmitError: function () {
    }

});
/**
 * Created by yurii on 02.07.15.
 */

CJ.submodules.user.ListWidget = CJ.core.View.extend({

    classType: 'UserListWidget',

    /**
     * @private
     */
    gridWidget: null,

    /**
     * @private
     */
    users: null,

    initialize: function (cfg) {
        this.tpl = JST['js/submodules/user/List.tpl.html'];

        CJ.core.View.prototype.initialize.apply(this, arguments);
        this.initGrid();
    },

    initGrid: function () {
        // Formatter for title column
        function usernameFormatter(row, cell, value, columnDef, dataContext) {
            return value;
        }

        var columns = [
                {id: 'username', name: 'User Name', field: 'username', formatter: usernameFormatter},
                {id: 'phone', name: 'Phone', field: 'phone'},
                {id: 'email', name: 'E-mail', field: 'email'},
                {id: 'role', name: 'Role', field: 'role', width: 50},
                {id: 'invoicecount', name: 'Total Invoice Count', field: 'invoicecount'},
                {id: 'buttons', name: '', field: 'buttons', formatter: usernameFormatter, width: 45}
            ],
            options = {
                enableCellNavigation: true,
                enableColumnReorder: false,
                forceFitColumns: true
            },
            data = [],
            gridRenderTo = this.renderTo + ' .gridWrapper',
            users,
            invoices,
            totalInvoiceCount = 0,
            totalInvoiceCountForEach = function (el, index, coll) {
                totalInvoiceCount += 1;
            };

        // After we got collection from storage
        var onUsersFetchSuccess = function (col) {
                users = this.users = col.models;
                invoices = CJ.app.collections.getCollection('invoices', onInvoicesFetchSuccess);
        }.bind(this),
            onInvoicesFetchSuccess = function (col) {
                // Fill data
                for (var i = 0; i < this.users.length; i++) {
                    // Some variables depend on other, so they are not just passed to new object parameters.
                    // They are configured first.
                    var usernameColumn = '<div class="cellImage"><img src="' + users[i].get('avatar') + '"></div>' +
                        users[i].get('firstName') + ' ' + users[i].get('lastName'),
                        phoneColumn = users[i].get('phone'),
                        emailColumn = users[i].get('email'),
                        roleColumn = users[i].get('role'),
                        invoicecountColumn,
                        _inactiveColumn = !users[i].get('isActive'),
                        activeClassColumn,
                        buttonsColumn,
                        idColumn = 'user' + users[i].id,
                        userInvoices = invoices.where({ userId: users[i].get('userId') });

                    totalInvoiceCount = 0;
                    userInvoices.forEach(totalInvoiceCountForEach);

                    invoicecountColumn = totalInvoiceCount;
                    activeClassColumn = _inactiveColumn ? 'inactive' : '';
                    buttonsColumn = '<div class="buttons"><a href=\"#users/edit/' + users[i].id + '\"><span' +
                        ' class="glyphicon' +
                        ' glyphicon-pencil"></span>' +
                        '</a><a href="javascript:void(0)"><span class="glyphicon glyphicon-off ' + activeClassColumn +
                        '"></span></a></div>';

                    data[i] = {
                        username: usernameColumn,
                        phone: phoneColumn,
                        email: emailColumn,
                        role: roleColumn,
                        invoicecount: invoicecountColumn,
                        _inactive: _inactiveColumn,
                        activeClass: activeClassColumn,
                        buttons: buttonsColumn,
                        id: idColumn
                    };
                }

                this.gridWidget = new CJ.core.grid.Widget({
                    gridData: data,
                    gridColumns: columns,
                    gridOptions: options,
                    renderTo: gridRenderTo,
                    dataCollection: users
                });

                // Should be after grid initialization
                this.initEvents();
            }.bind(this);

        CJ.app.collections.getCollection('users', onUsersFetchSuccess);
    },

    initEvents: function () {
        $('.grid', this.tpl).on('click', 'a .glyphicon-off',this.onUserBanClick.bind(this));
    },

    /**
     * User ban click handler.
     * @private
     * @param e
     */
    onUserBanClick: function (e) {
        var cell = this.gridWidget.grid.getCellFromEvent(e),
            row = cell.row,
            user = this.users[row],
            _inactiveColumn = !user.get('isActive'),
            activeClassColumn = !_inactiveColumn ? 'inactive' : '';

        user.set({
            isActive: _inactiveColumn
        });
        this.gridWidget.setRowData(row, {

            _inactive: !user.get('isActive'),

            buttons: '<div class="buttons"><a href=\"#users/edit/' + user.id + '\"><span' +
            ' class="glyphicon' +
            ' glyphicon-pencil"></span>' +
            '</a><a href="javascript:void(0)"><span class="glyphicon glyphicon-off ' + activeClassColumn +
            '"></span></a></div>'

        }, $(e.currentTarget));
    },

    /**
     * This method is called before remove(). It is used for removing references.
     * @private
     */
    onRemove: function () {
        if (this.gridWidget) {
            this.gridWidget.remove();
            this.gridWidget = null;
        }
    }

});
/**
 * Created by yurii on 08.07.15.
 */

CJ.submodules.viewport.Widget = CJ.core.View.extend({

    classType: 'ViewportWidget',

    currentContent: null,

    initialize: function () {
        this.tpl = JST['js/submodules/viewport/Viewport.tpl.html'];
        CJ.core.View.prototype.initialize.apply(this, arguments);

        this.initEvents();
        this.setNavigation();
    },

    initEvents: function () {
        $('.showUserActions', this.el).on('click', this.onShowUserActionsClick);
        $('.showUserActions', this.el).on('focusout', this.onShowUserActionsFocusOut);
        $('.showMenu .btn', this.el).on('click', this.onShowMenuClick.bind(this));
        $('.viewport').on('transitionend webkitTransitionEnd oTransitionEnd', this.onViewportTransitionEnd);

        this.listenTo(CJ.app.router, 'viewport.changeContent', this.onChangeContent.bind(this));

        CJ.app.userIdentity.model.on('change', function (e) {
            console.log('changed');
            console.log(arguments);
        });
    },

    /**
     * Toggles navigation item active.
     * @param {String} contentName Name of navigation item to activate.
     */
    setNavigation: function (contentName) {
        var navSelector,
            navItems = $('.navItems', this.el);

        navItems.children('li').each(function (index, el) {
            $('a', el).toggleClass('active', false);
        });

        if (!contentName) {
            contentName = 'users';
        }

        navSelector = '.' + contentName + 'Link';
        $(navSelector, this.el).toggleClass('active', true);
    },

    /**
     * Show menu click handler.
     * @param {Event} e
     */
    onShowMenuClick: function (e) {
        $('.viewport').toggleClass('slide-menu-push-toright');
        $('.slide-menu').toggleClass('slide-menu-open');
    },

    /**
     * Viewport transition end handler. Triggers 'resize' event.
     * @param e
     */
    onViewportTransitionEnd: function (e) {
        if (e.originalEvent.propertyName === 'width' &&
                e.eventPhase === Event.BUBBLING_PHASE) {
            $(this).trigger($.Event('resize'));
        }
    },

    /**
     * Show user actions click handler.
     * @param {Event} e
     */
    onShowUserActionsClick: function (e) {
        $(this).toggleClass('expand');
    },

    /**
     * Show user actions focus out. Resets arrow key to default state.
     * @param e
     */
    onShowUserActionsFocusOut: function (e) {
        $(this).toggleClass('expand', false);
    },

    getTplData: function () {
        var user = CJ.app.userIdentity.getModel();
        return { username: user.get('firstName') + user.get('lastName'), userAvatar: user.get('avatar') };
    },

    /**
     * @param {Object} cfg Config of content to create inside viewport.
     */
    setContent: function(cfg) {
        if (this.currentContent) {
            this.currentContent.remove();
        }

        if (!cfg) {
            cfg = {};
        }

        cfg.renderTo = '.contentContainer';
        this.currentContent = new cfg.contentPrototype(cfg);
    },

    /**
     * Change content handler.
     * @param {Object} cfg Event config.
     */
    onChangeContent: function (cfg) {
        this.setNavigation(cfg.contentName);
        this.setContent(cfg);
    },

    /**
     * This method is called before remove(). It is used for removing references.
     * @private
     */
    onRemove: function () {
        if (this.currentContent) {
            this.currentContent.remove();
        }
    }

});


$(function() {
    CJ.app = new CJ.core.etudeCashboxApp.EtudeCashboxApp();
});

