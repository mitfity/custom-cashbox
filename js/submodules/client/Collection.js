/**
 * Created by yurii on 30.06.15.
 */

CJ.submodules.client.Collection = Backbone.Collection.extend({

    model: CJ.submodules.client.Model,

    url: '/api/client',

    initialize: function () {
        this.initEvents();
    },

    initEvents: function () {
        // Save model to local storage

        this.on('add', function (model, collection) {
            model.save({}, {
                success: function (model, response) {
                }
            });
        });

        this.on('change', function (model) {
            model.save(model.attributes, {
                wait: true,
                success: function (model, response) {
                },
                error: function (model, msg, args) {
                    console.log(msg);
                }
            });
        });
    },

    /**
     * @param {Number} id clientId
     * @returns {Object} Backbone.Model
     */
    getModelById: function (id) {
        return this.findWhere({ clientId: id });
    },

    /**
     * @param {String} email
     * @param {Number} modelId clientId of editting client
     * @returns {boolean}
     */
    hasModelWithEmail: function (email, modelId) {
        var models = this.filter(function (model) {
            return model.get('removed') === false && model.id !== modelId && model.get('email') === email;
        });

        return (models.length) ? true : false;
    },

    /**
     * @param {String} phone
     * @param {Number} modelId clientId of editting client
     * @returns {boolean}
     */
    hasModelWithPhone: function (phone, modelId) {
        var models = this.filter(function (model) {
            return model.get('removed') === false && model.id !== modelId && model.get('phone') === phone;
        });

        return (models.length) ? true : false;
    }

});