/**
 * Created by yurii on 15.07.15.
 */

CJ.submodules.client.FormWidget = CJ.core.form.Widget.extend({

    classType: 'ClientFormWidget',

    /**
     * @property {jQuery}
     */
    form: null,

    /**
     * @property {Object} editing model.
     */
    targetModel: null,

    /**
     * @property {Object} current form model.
     */
    currentModel: null,

    /**
     * @private
     */
    parentWindow: null,

    /**
     * @private
     */
    emailEl: null,

    /**
     * @private
     */
    phoneEl: null,

    /**
     * @cfg {boolean} True if u let form to be empty.
     */
    canBeEmpty: false,

    initialize: function (cfg) {
        $.extend(this, cfg);

        this.tpl = JST['js/submodules/client/Form.tpl.html'];
        this.initModels();
        CJ.core.form.Widget.prototype.initialize.apply(this, arguments);

        this.emailEl = $('input[name="email"]', this.form);
        this.phoneEl = $('input[name="phone"]', this.form);

        this.setValues(this.currentModel.attributes);
        if (this.canBeEmpty) {
            this.initOnDirtyEvents();
        }
        else {
            $('label[for=\"'+this.phoneEl.attr('id')+'\"].inputLabel').addClass('required');
            $('label[for=\"'+this.emailEl.attr('id')+'\"].inputLabel').addClass('required');
        }
    },

    initModels: function () {
        var modelId = this.modelId;

        this.currentModel = new CJ.submodules.client.Model();
        if (modelId !== undefined) {
            this.targetModel = new CJ.submodules.client.Model({
                clientId: modelId
            });

            this.targetModel.fetch({

                success: function (model, response) {
                    this.currentModel.set(model.attributes);
                    this.setValues(this.currentModel.attributes);
                }.bind(this),

                error: function (model, error) {
                    console.log('error');
                    console.log(error.responseText());
                    CJ.app.redirectToDefaultRoute();
                }

            });
        }
    },

    /**
     * This is custom jquery validation initialization.
     * @private
     */
    initOnDirtyEvents: function () {
        $('input', this.form).each(function (index, input) {
            $(input).on('input', function (e) {
                if ($(input).val()) {
                    this.phoneEl.rules('add', {
                        required: true
                    });
                    this.emailEl.rules('add', {
                        required: true
                    });

                    $('label[for=\"'+this.phoneEl.attr('id')+'\"].inputLabel').addClass('required');
                    $('label[for=\"'+this.emailEl.attr('id')+'\"].inputLabel').addClass('required');
                }
                else {
                    if (this.isEmpty()) {
                        this.phoneEl.rules('remove', 'required');
                        this.emailEl.rules('remove', 'required');

                        $('label[for=\"'+this.phoneEl.attr('id')+'\"].inputLabel').removeClass('required');
                        $('label[for=\"'+this.emailEl.attr('id')+'\"].inputLabel').removeClass('required');

                        this.phoneEl.valid();
                        this.emailEl.valid();
                    }
                }


            }.bind(this));
        }.bind(this));
    },

    /**
     * @private
     * @override CJ.core.form.Widget
     * @returns {*|jQuery|HTMLElement}
     */
    getForm: function () {
        return $('.editForm', this.el);
    },

    /**
     * @private
     * @override CJ.core.form.Widget
     * @returns {Object}
     */
    getValidationOptions: function () {
        var validationOptions = {

            rules: {

                firstName: {
                    required: false
                },

                lastName: {
                    required: false
                },

                phone: {
                    phone: true,
                    required: true
                },

                email: {
                    email: true,
                    required: true
                },

                organization: {
                    required: false
                },

                country: {
                    requiredGroup: {
                        className: 'addressGroup',
                        flagElementClassName: 'secondAddress'
                    },
                    required: false,
                    reValidate: true
                },

                state: {
                    requiredGroup: {
                        className: 'addressGroup',
                        flagElementClassName: 'secondAddress'
                    },
                    required: false,
                    reValidate: true
                },

                city: {
                    requiredGroup: {
                        className: 'addressGroup',
                        flagElementClassName: 'secondAddress'
                    },
                    required: false,
                    reValidate: true
                },

                zip:  {
                    requiredGroup: {
                        className: 'addressGroup',
                        flagElementClassName: 'secondAddress'
                    },
                    required: false,
                    reValidate: true
                },

                firstAddress: {
                    requiredGroup: {
                        className: 'addressGroup',
                        flagElementClassName: 'secondAddress'
                    },
                    required: false,
                    reValidate: true
                },

                secondAddress: {
                    requiredGroupFlagElement: 'addressGroup',
                    required: false,
                    reValidate: true
                }

            },
            messages: {

                phone: {
                    phone: 'Incorrect phone format.',
                    required: 'Enter your phone number.'
                },

                email: {
                    required: 'Enter your email.',
                    email: 'Enter valid email address.'
                },

                organization: {
                    required: 'Enter your organization.'
                },

                country: {
                    requiredGroup: 'Enter your country.'
                },

                state: {
                    requiredGroup: 'Enter your state.'
                },

                city: {
                    requiredGroup: 'Enter your city.'
                },

                zip: {
                    requiredGroup: 'Enter your zip code.'
                },

                firstAddress: {
                    requiredGroup: 'Enter your address.'
                }

            }
        };

        if (this.canBeEmpty) {
            _.extend(validationOptions.rules, {

                phone: {
                    phone: true,
                    required: false
                },

                email: {
                    email: true,
                    required: false
                }

            });
        }

        return validationOptions;
    },

    /**
     * @private
     * @override CJ.core.form.Widget
     * @param e
     * @returns {boolean}
     */
    onFormSubmit: function (e) {
        if (this.canBeEmpty) {
            if (!this.isEmpty()) {

            }
            else {
                this.onFormSubmitSuccess();
                return true;
            }
        }

        if (this.isValid()) {
            var responseError = null,
                validatorErrors = {},
                successFunc,
                errorFunc;

            this.currentModel.set(this.getValues());

            if (this.targetModel) {
                this.targetModel.set(this.currentModel.attributes);

                errorFunc = function (model, msg, args) {
                    responseError = JSON.parse(msg.responseText);
                    validatorErrors[responseError.field] = responseError.errorMessage;
                    this.validator.showErrors(validatorErrors);
                    this.onFormSubmitError();
                }.bind(this);

                successFunc = function (model, response) {
                    model.set(JSON.parse(response));
                    this.currentModel.set(model.attributes);
                    this.onFormSubmitSuccess(model);
                }.bind(this);

                this.targetModel.save({}, {
                    wait: true,
                    success: successFunc,
                    error: errorFunc
                });
            }
            else {
                errorFunc = function (model, msg, args) {
                    responseError = JSON.parse(msg.responseText);
                    validatorErrors[responseError.field] = responseError.errorMessage;
                    this.validator.showErrors(validatorErrors);
                    this.onFormSubmitError();
                }.bind(this);

                successFunc = function (model, response) {
                    model.set(JSON.parse(response));
                    this.currentModel = model;
                    this.onFormSubmitSuccess(model);
                }.bind(this);

                this.currentModel.save({}, {
                    wait: true,
                    success: successFunc,
                    error: errorFunc
                });
            }

        }

        this.onFormSubmitError();
        return false;
    },

    /**
     * This is called when form successfully submits.
     */
    onFormSubmitSuccess: function (model) {
    },

    /**
     * This is called when form submit is failed.
     */
    onFormSubmitError: function () {
    }

});