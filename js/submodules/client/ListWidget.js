/**
 * Created by yurii on 03.07.15.
 */

CJ.submodules.client.ListWidget = CJ.core.View.extend( {

    classType: 'ClientListWidget',

    initialize: function (cfg) {
        this.tpl = JST['js/submodules/client/List.tpl.html'];

        CJ.core.View.prototype.initialize.apply(this, arguments);
        this.initGrid();
    },

    /**
     * @private
     */
    initGrid: function () {
        // Formatter for buttons column
        function buttonsFormatter(row, cell, value, columnDef, dataContext) {
            return value;
        }

        var columns = [
                {id: 'clientname', name: 'Client Name', field: 'clientname'},
                {id: 'phone', name: 'Phone', field: 'phone'},
                {id: 'email', name: 'E-mail', field: 'email', width: 100},
                {id: 'organization', name: 'Organization', field: 'organization', width: 50},
                {id: 'lastinvoice', name: 'Last Invoice', field: 'lastinvoice', width: 50},
                {id: 'totalspent', name: 'Total Spent Count', field: 'totalspent', width: 60},
                {id: 'buttons', name:'', field: 'buttons', formatter: buttonsFormatter, width: 45}
            ],
            options = {
                enableCellNavigation: true,
                enableColumnReorder: false,
                forceFitColumns: true
            },
            data = [],
            gridRenderTo = this.renderTo + ' .gridWrapper',
            clientsCollection = null,
            invoices,
            dateGreaterThan = function (firstDate, lastDate) {
                return new Date(firstDate) > new Date(lastDate);
            },
            clientInvoicesForEach;

        clientsCollection = CJ.app.collections.getCollection('clients', function (col) {
            var clients = col.models,
                clientInvoices,
                clientTotal = 0,
                clientLastInvoiceDate,
                dateCreatedBuffer;

            clientInvoicesForEach = function (el, index, coll) {
                clientTotal += el.get('total');

                if (!clientLastInvoiceDate) {
                    clientLastInvoiceDate = el.get('dateCreated');
                }
                else {
                    dateCreatedBuffer = el.get('dateCreated');

                    if (dateGreaterThan(dateCreatedBuffer, clientLastInvoiceDate)) {
                        clientLastInvoiceDate = dateCreatedBuffer;
                    }
                }
            };

            invoices = CJ.app.collections.getCollection('invoices', function (col) {
                invoices = col;

                // Fill data
                for (var i = 0; i < clients.length; i++) {
                    if (clients[i].get('removed') === true) {
                        continue;
                    }

                    clientTotal = 0;
                    clientLastInvoiceDate = null;
                    clientInvoices = invoices.where({ clientId: clients[i].id });
                    clientInvoices.forEach(clientInvoicesForEach);

                    data.push({

                        clientname: clients[i].get('firstName') + ' ' + clients[i].get('lastName'),

                        phone: clients[i].get('phone'),

                        email: clients[i].get('email'),

                        organization: clients[i].get('organization'),

                        lastinvoice: clientLastInvoiceDate,

                        totalspent: '$' + clientTotal.toFixed(2),

                        buttons: '<div class="buttons"><a href=\"#clients/edit/' + clients[i].id + '\"><span' +
                        ' class="glyphicon' +
                        ' glyphicon-pencil"></span></a>' +
                        '<a href=\"#clients/remove/' + clients[i].id  +'\"><span class="glyphicon' +
                        ' glyphicon-trash"></span></a></div>',

                        id: 'client' + clients[i].id

                    });
                }

                this.gridWidget = new CJ.core.grid.Widget({
                    gridData: data,
                    gridColumns: columns,
                    gridOptions: options,
                    renderTo: gridRenderTo
                });
            }.bind(this));

        }.bind(this));
    },

    /**
     * This method is called before remove(). It is used for removing references.
     * @private
     */
    onRemove: function () {
        if (this.gridWidget) {
            this.gridWidget.remove();
            this.gridWidget = null;
        }
    }

});