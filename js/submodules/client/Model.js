/**
 * Created by mitfity on 29.06.2015.
 */

CJ.submodules.client.Model = Backbone.Model.extend({

    urlRoot: '/api/client',

    idAttribute: 'clientId',

    firstName: null,

    lastName: null,

    phone: null,

    email: null,

    organization: null,

    country: null,

    state: null,

    city: null,

    zip: null,

    firstAddress: null,

    secondAddress: null,

    defaults: {

        removed: false
    },

    initialize: function () {
    }

});