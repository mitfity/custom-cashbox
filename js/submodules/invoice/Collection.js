/**
 * Created by yurii on 30.06.15.
 */

CJ.submodules.invoice.Collection = Backbone.Collection.extend({

    model: CJ.submodules.invoice.Model,

    url: '/api/invoice',

    initialize: function () {
        this.initEvents();
    },

    initEvents: function () {
        // Save model to local storage

        this.on('add', function (model, collection) {
            model.save({}, {
                success: function (model, response) {
                }
            });
        });

        this.on('change', function (model) {
            model.save(model.attributes, {
                success: function (model, response) {
                },
                error: function (model, msg, args) {
                    console.log(msg);
                }
            });
        });
    },

    /**
     * @param {Number} id invoiceId
     * @returns {Object} Backbone.Model
     */
    getModelById: function (id) {
        return this.findWhere({ invoiceId: id });
    },

    /**
     * @param {Number} id invoiceId
     * @param {Number} modelId invoiceId of editting invoice
     * @returns {boolean}
     */
    hasModelWithId: function (id, modelId) {
        var models = this.filter(function (model) {
            return model.get('id') !== modelId && model.get('id') === id;
        });

        return (models.length) ? true : false;
    }

});