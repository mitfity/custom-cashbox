/**
 * Created by yurii on 17.07.15.
 */

CJ.submodules.invoice.CreationInfoFormWidget = CJ.core.form.Widget.extend({

    classType: 'CreationInfoFormWidget',

    /**
     * @property {jQuery}
     */
    form: null,

    /**
     * @private
     */
    parentWindow: null,

    /**
     * @private
     */
    dateFiled: null,

    initialize: function (cfg) {
        $.extend(this, cfg);

        this.tpl = JST['js/submodules/invoice/CreationInfoFormWidget.tpl.html'];
        this.initModels();
        CJ.core.form.Widget.prototype.initialize.apply(this, arguments);

        this.dateFiled = $('input[name="dateCreated"]', this.form);
        this.dateFiled.datepicker({
            onSelect: this.onDatePickerSelected.bind(this)
        });
    },

    initModels: function () {
        var modelId = this.modelId,
            userId = this.userId,
            dateCreated = this.dateCreated,
            invoices = CJ.app.collections.getCollection('invoices');

        this.currentModel = {};
        if (modelId !== undefined) {
            this.targetModel = {
                id: modelId,
                userId: userId,
                dateCreated: dateCreated
            };

            _.extend(this.currentModel, this.targetModel);
        }
        else {
            this.currentModel.id = invoices.length;
        }
    },

    /**
     * @private
     * @override CJ.core.View
     * @returns {Object}
     */
    getTplData: function () {
        var user = CJ.app.collections.getCollection('users').getModelById(this.userId),
            usename = user.get('firstName') + ' ' + user.get('lastName'),
            invoiceId = this.currentModel.id,
            date = new Date(),
            today,
            paddingWithZeros = CJ.core.utils.paddingWithZeros;

        invoiceId = paddingWithZeros(id, 10);

        today = paddingWithZeros(date.getMonth() + 1, 2) + '/' + paddingWithZeros(date.getDate(), 2) + '/' +
            date.getFullYear();

        return {
            username: usename || 'user name',
            invoiceId: invoiceId,
            dateCreated: this.dateCreated || today,
            userId: user.get('id')
        };
    },

    /**
     * @private
     * @override CJ.core.form.Widget
     * @returns {*|jQuery|HTMLElement}
     */
    getForm: function () {
        return $('.editForm', this.el);
    },

    /**
     * @private
     * @override CJ.core.form.Widget
     */
    getValidationOptions: function () {
        return {

            rules: {

                invoiceId: {
                    required: true,
                    number: true
                },

                dateCreated: {
                    required: true,
                    realDate: true
                }

            },
            messages: {

                invoiceId: {
                    required: 'Enter invoice ID.',
                    number: 'Invoice ID has to be a number.'
                },

                dateCreated: {
                    required: 'Enter date.',
                    realDate: 'Enter valid date.'
                }

            }
        };
    },

    /**
     * @private
     * @param date
     */
    onDatePickerSelected: function (date) {
        this.dateFiled.valid();
    },

    /**
     * @private
     * @override CJ.core.form.Widget
     * @param e
     * @returns {boolean}
     */
    onFormSubmit: function (e) {
        if (this.isValid()) {
            var invoices = CJ.app.collections.getCollection('invoices'),
                alreadyExists = false,
                formValues = this.getValues();

            _.extend(this.currentModel, formValues);
            this.currentModel.userId = parseInt(this.currentModel.userId, 10);
            this.currentModel.id = parseInt(this.currentModel.id, 10);

            if (this.targetModel) {
                if ( invoices.hasModelWithId(this.currentModel.id, this.targetModel.id) ) {
                    this.validator.showErrors({ invoiceId: 'Such id already exists.' });
                    alreadyExists = true;
                }
            }
            else {
                if ( invoices.hasModelWithId(this.currentModel.id) ) {
                    this.validator.showErrors({ invoiceId: 'Such id already exists.' });
                    alreadyExists = true;
                }
            }



            if (alreadyExists) {
                return false;
            }

            if (!this.targetModel) {
                this.targetModel = {};
            }
            _.extend(this.targetModel, this.currentModel);

            this.onFormSubmitSuccess();
            return true;
        }

        return false;
    },

    /**
     * This is called when form successfully submitted.
     */
    onFormSubmitSuccess: function () {
    }

});