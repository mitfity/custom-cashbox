/**
 * Created by yurii on 16.07.15.
 */

CJ.submodules.invoice.CrudWidget = CJ.core.form.Widget.extend( {

    classType: 'InvoiceCrudWidget',

    /**
     * @private
     */
    clientFormWidget: null,

    /**
     * @private
     */
    checkByEmail: false,

    /**
     * @private
     */
    checkByPhone: false,

    /**
     * @private
     */
    creationInfoFormWidget: null,

    /**
     * @private
     */
    productsGridWidget: null,

    /**
     * @property {Object} current form invoice model
     */
    currentModel: null,

    /**
     * @property {Object} editing invoice model
     */
    targetModel: null,

    /**
     * @property {jQuery}
     */
    form: null,

    initialize: function (cfg) {
        this.tpl = JST['js/submodules/invoice/Crud.tpl.html'];
        CJ.core.form.Widget.prototype.initialize.apply(this, arguments);

        this.checkByEmail = false;
        this.checkByPhone = false;

        this.initModels();
    },

    /**
     * @private
     * @override CJ.core.View
     * @returns {Object}
     */
    getTplData: function () {
        return {
            title: this.contentTitle || 'Title '
        };
    },

    initInvoiceEvents: function () {
        $('.saveButton', this.el).on('click', this.onSaveButtonClicked.bind(this));
        this.clientFormWidget.emailEl.on('input', this.onFormEmailInput.bind(this));
        this.clientFormWidget.phoneEl.on('input', this.onFormPhoneInput.bind(this));
        $(this.el).on('click', '.removeItem', this.onRemoveItemClicked.bind(this));
        $('.addLineButton', this.el).on('click', this.onAddLineButtonClicked.bind(this));

        this.dateFiled = $('input[name="dateCreated"]', this.form);
        this.dateFiled.datepicker({
            onSelect: this.onDatePickerSelected.bind(this)
        });
    },

    initModels: function () {
        var modelId = this.modelId,
            invoices,
            invoiceId,
            user,
            username,
            date = new Date(),
            today,
            paddingWithZeros = CJ.core.utils.paddingWithZeros,
            onModelFetchSuccess;

        this.currentModel = new CJ.submodules.invoice.Model();
        if (modelId !== undefined) {
            this.targetModel = new CJ.submodules.invoice.Model({
                invoiceId: modelId
            });

            this.targetModel.fetch({

                wait: true,

                success: function (model, response) {
                    this.targetModel = model;

                    if (!this.targetModel) {
                        CJ.app.redirectToDefaultRoute();
                    }

                    this.currentModel.set(this.targetModel.attributes);

                    onModelFetchSuccess();
                }.bind(this),

                error: function (model, response) {
                }.bind(this)

            });


        }
        else {
            invoices = CJ.app.collections.getCollection('invoices', function (col) {
                invoices = col;

                var maxInvoiceId = invoices.length + 1,
                    newInvoiceId = maxInvoiceId;
                if (invoices.findWhere({
                        invoiceId: maxInvoiceId
                    })) {
                    maxInvoiceId = 0;
                    invoices.forEach(function (el, index, col) {
                        if (el.get('invoiceId') > maxInvoiceId) {
                            maxInvoiceId = el.get('invoiceId');
                        }
                    });
                    newInvoiceId = maxInvoiceId + 1;
                }
                this.currentModel.set({ invoiceId: newInvoiceId });
                this.currentModel.set({ userId: CJ.app.userIdentity.userId });

                onModelFetchSuccess();
            }.bind(this));
        }

        onModelFetchSuccess = function () {
            invoiceId = this.currentModel.id;
            user = new CJ.submodules.user.Model({ userId: this.currentModel.get('userId') });

            user.fetch({

                wait: true,

                success: function (model, response) {
                    invoiceId = paddingWithZeros(invoiceId, 10);
                    this.currentModel.set({
                        invoiceId: invoiceId
                    });

                    today = paddingWithZeros(date.getMonth() + 1, 2) + '/' + paddingWithZeros(date.getDate(), 2) + '/' +
                        date.getFullYear();

                    username = model.get('firstName') + ' ' + model.get('lastName');

                    if (!this.currentModel.get('dateCreated')) {
                        this.currentModel.set({
                            dateCreated: today
                        });
                    }

                    this.setValues(this.currentModel.attributes);

                    $('span#username', this.el).text(username);

                    this.initWidgets();
                    this.initInvoiceEvents();
                    this.initAutocomplete();
                }.bind(this),

                error: function (model, response) {

                }.bind(this)

            });
        }.bind(this);

    },

    getValues: function () {
        var values = CJ.core.form.Widget.prototype.getValues.apply(this, arguments);
        $.extend(values, {

            invoiceId: parseInt(CJ.core.utils.paddingWithZeros(values.invoiceId, 0))

        });

        return values;
    },

    /**
     * @private
     * @override CJ.core.form.Widget
     * @returns {*|jQuery|HTMLElement}
     */
    getForm: function () {
        return $('.invoiceInfoForm', this.el);
    },

    /**
     * @private
     * @override CJ.core.form.Widget
     */
    getValidationOptions: function () {
        return {

            rules: {

                invoiceId: {
                    required: true,
                    number: true
                },

                dateCreated: {
                    required: true,
                    realDate: true
                }

            },
            messages: {

                invoiceId: {
                    required: 'Enter invoice ID.',
                    number: 'Invoice ID has to be a number.'
                },

                dateCreated: {
                    required: 'Enter date.',
                    realDate: 'Enter valid date.'
                }

            }
        };
    },

    /**
     * @private
     * @param date
     */
    onDatePickerSelected: function (date) {
        this.dateFiled.valid();
    },

    initWidgets: function () {
        var clientRenderTo = this.renderTo + ' .clientForm';

        this.clientFormWidget = new CJ.submodules.client.FormWidget({
            renderTo: clientRenderTo,
            modelId: this.currentModel.get('clientId'),
            canBeEmpty: true
        });

        this.clientFormWidget.disableSubmitOnEnter();

        this.initProductsGrid();
    },

    initProductsGrid: function () {
        var requiredNameFormatter = CJ.core.configs.slickgridValidationConfig.requiredNameFormatter,
            numberFormatter = CJ.core.configs.slickgridValidationConfig.numberFormatter,
            priceFormatter = CJ.core.configs.slickgridValidationConfig.priceFormatter,
            percentageFormatter = CJ.core.configs.slickgridValidationConfig.percentageFormatter,
            positiveFloatEditor = CJ.core.configs.slickgridValidationConfig.positiveFloatEditor,
            htmlFormatter = CJ.core.configs.slickgridValidationConfig.htmlFormatter,
            formatterLineTotal = CJ.core.configs.slickgridValidationConfig.formatterLineTotal,
            productsAutocompleteEditor = CJ.core.configs.slickgridValidationConfig.productsAutocompleteEditor,
            columns = [
                {id: 'name', name: 'Name', field: 'name', width: 50, editor: productsAutocompleteEditor,
                    formatter: requiredNameFormatter, resizable: false},
                {id: 'description', name: 'Description', field: 'description', width: 100, editor: Slick.Editors.Text,
                    resizable: false},
                {id: 'price', name: 'Price', field: 'price', width: 50, editor: positiveFloatEditor,
                    formatter: priceFormatter, resizable: false},
                {id: 'count', name: 'Qnt', field: 'count', width: 10, editor: positiveFloatEditor,
                    formatter: numberFormatter, resizable: false},
                {id: 'tax', name:'Tax', field: 'tax', minWidth: 200, maxWidth: 200, width: 200,
                    editor: positiveFloatEditor, formatter: percentageFormatter, resizable: false},
                {id: 'total', name:'Line Total', field: 'total', minWidth: 200, maxWidth: 200, width: 50,
                    formatter: formatterLineTotal, resizable: false},
                {id: 'buttons', name: '', field: 'buttons', minWidth: 70, maxWidth: 70, width: 70,
                    formatter: htmlFormatter, resizable: false}
            ],
            options = {
                editable: true,
                enableAddRow: true,
                asyncEditorLoading: true,
                enableCellNavigation: true,
                enableColumnReorder: false,
                forceFitColumns: true,
                autoHeight: true
            },
            data = [],
            gridRenderTo = this.renderTo + ' .productsGridWrapper',
            productsFetch,
            products = this.currentModel.get('invoiceItems') || [];

            if (products.length) {
                // Fill data
                for (var i = 0; i < products.length; i++) {
                    data[i] = {
                        name: products[i].name,
                        description: products[i].description,
                        price: products[i].price,
                        count: products[i].count,
                        tax: products[i].tax,
                        buttons: '<div class="removeItem"><span class="glyphicon glyphicon-trash "></span></div>',
                        id: 'product' + products[i].invoiceItemId
                    };
                }
            }

            this.productsGridWidget = new CJ.core.grid.Widget({
                gridData: data,
                gridColumns: columns,
                gridOptions: options,
                renderTo: gridRenderTo
            });

            this.productsGridWidget.onGridAddNewRow = this.onAddNewRow.bind(this);

            this.productsGridWidget.onGridCellChange = this.onCellChange.bind(this);

            this.productsGridWidget.onDataviewRowsChanged = this.onRowsChanged.bind(this);

            this.updateInvoiceTotal();
    },

    /**
     * @private
     * @param e
     * @param args
     */
    onRemoveItemClicked: function (e, args) {
        this.productsGridWidget.removeActiveRow();
        this.updateInvoiceTotal();
    },

    /**
     * @private
     * @param e
     * @param args
     */
    onRowsChanged: function (e, args) {
        this.updateInvoiceTotal();
    },

    /**
     * @private
     * @param e
     */
    onAddLineButtonClicked: function (e) {
        var dataView = this.productsGridWidget.gridDataView,
            rowsCount = dataView.getLength(),
            item = {
                name: '',
                price: 0,
                count: 1,
                tax: 10.0,
                total: 0,
                buttons: '<div class="removeItem"><span class="glyphicon glyphicon-trash "></span></div>',
                id: 'product' + rowsCount
            };

        dataView.addItem(item);
        this.updateInvoiceTotal();

        $('html, body').animate({
            scrollTop: $('.addLineButton', this.el).offset().top
        }, 0);
    },

    /**
     * @private
     * @param e
     * @param args
     */
    onAddNewRow: function (e, args) {
        var rowsCount = this.productsGridWidget.gridDataView.getLength(),
            item = {
                name: '',
                price: 0,
                count: 1,
                tax: 10.0,
                total: 0,
                buttons: '<div class="removeItem"><span class="glyphicon glyphicon-trash "></span></div>',
                id: 'product' + rowsCount
            };

        $.extend(item, args.item);
        this.productsGridWidget.gridDataView.addItem(item);
        this.updateInvoiceTotal();
    },

    /**
     * @private
     * @param e
     * @param args
     */
    onCellChange: function (e, args) {
        this.updateInvoiceTotal();
    },

    /**
     * @private
     */
    updateInvoiceTotal: function () {
        $('.totalValue', this.el).text("$" + this.calcInvoiceTotal());
    },

    /**
     * @private
     * @returns {string}
     */
    calcInvoiceTotal: function () {
        var rows = this.productsGridWidget.gridDataView.getItems(),
            total = 0;

        for (var i = 0; i < rows.length; i++) {
            total += rows[i].total;
        }

        return total.toFixed(2);
    },

    /**
     * @private
     */
    initAutocomplete: function () {
        var clients,
            emailEl = this.clientFormWidget.emailEl,
            phoneEl = this.clientFormWidget.phoneEl,
            clientsEmailAutocomplete,
            clientsPhoneAutocomplete;

        clients = CJ.app.collections.getCollection('clients', function (col) {
            // Prevent autocomplete from banned users
            clients = new CJ.submodules.client.Collection(col.where({ removed: 0 }));

            // Set up collection for autocomplete usage.
            clientsEmailAutocomplete = clients.toJSON();
            clientsEmailAutocomplete.forEach(function (el, index, col) {
                el.label = el.email;
            });

            emailEl.autocomplete({
                minLength: 0,
                source: clientsEmailAutocomplete,
                select: function (event, ui) {
                    emailEl.val(ui.item.email);
                    this.autocompleteByEmail(clients, ui.item.email);
                    this.clientFormWidget.form.valid();
                    return false;
                }.bind(this)
            })
                .autocomplete("instance")._renderItem = function (ul, item) {
                return $("<li>")
                    .append("<a>" + item.email + "<br>" + item.firstName + ' ' + item.lastName +
                    "</a>")
                    .appendTo(ul);
            };

            // Set up collection for autocomplete usage.
            clientsPhoneAutocomplete = clients.toJSON();
            clientsPhoneAutocomplete.forEach(function (el, index, col) {
                el.label = el.phone;
            });

            phoneEl.autocomplete({
                minLength: 0,
                source: clientsPhoneAutocomplete,
                select: function (event, ui) {
                    phoneEl.val(ui.item.phone);
                    this.autocompleteByPhone(clients, ui.item.phone);
                    this.clientFormWidget.form.valid();
                    return false;
                }.bind(this)
            })
                .autocomplete("instance")._renderItem = function (ul, item) {
                return $("<li>")
                    .append("<a>" + item.phone + "<br>" + item.firstName + ' ' + item.lastName +
                    "</a>")
                    .appendTo(ul);
            };
        }.bind(this));
    },

    /**
     * @private
     * @param collection
     * @param email
     */
    autocompleteByEmail: function (collection, email) {
        var model = collection.findWhere({ email: email });

        if (model) {
            this.clientFormWidget.setValues(model.attributes);
            this.checkByPhone = true;
            this.checkByEmail = true;
        }
    },

    /**
     * @private
     * @param collection
     * @param phone
     */
    autocompleteByPhone: function (collection, phone) {
        var model = collection.findWhere({ phone: phone });

        if (model) {
            this.clientFormWidget.setValues(model.attributes);
            this.checkByEmail = true;
            this.checkByPhone = true;
        }
    },

    /**
     * @private
     * @param e
     */
    onSaveButtonClicked: function (e) {
        var clientForm = this.clientFormWidget,
            productsValues,
            products = [],
            clientId = null,
            successFunc,
            errorFunc,
            validatorErrors = {},
            responseError,
            targetModelAttributesBuffer;

        if (this.productsGridWidget.isValid()) {

            this.submitClientForm(function (clientModel) {
                productsValues = this.productsGridWidget.getValues();

                productsValues.forEach(function (item, index, coll) {
                    _.extend(item, {
                        invoiceId: this.currentModel.get('id')
                    });

                    products.push(new CJ.submodules.product.Model(item));
                }.bind(this));

                clientId = clientModel ? clientModel.id : null;
                this.currentModel.set(this.getValues());
                this.currentModel.set({
                    clientId: clientId,
                    invoiceItems: products,
                    invoiceId: parseInt(this.currentModel.get('invoiceId'))
                });

                if (this.targetModel) {
                    targetModelAttributesBuffer = {};
                    $.extend(targetModelAttributesBuffer, this.targetModel.attributes);
                    this.targetModel.set(this.currentModel.attributes);

                    errorFunc = function (model, msg, args) {
                        responseError = JSON.parse(msg.responseText);
                        validatorErrors[responseError.field] = responseError.errorMessage;
                        this.validator.showErrors(validatorErrors);

                        // error
                        if ($('input.invalid', this.el)[0]) {
                            $('html, body').animate({
                                scrollTop: $('input.invalid', this.el).offset().top - 100
                            }, 0);
                        }

                        // Prevent bug when submiting second time it submits on updating invoice with
                        // target invoice id rather then with current, cause it thinks we didn't change invoice id.
                        this.targetModel.set(targetModelAttributesBuffer);
                        targetModelAttributesBuffer = null;
                    }.bind(this);

                    successFunc = function (model, response) {
                        this.currentModel.set(this.targetModel.attributes);

                        CJ.app.router.navigate('invoices/list', {
                            trigger: true
                        });
                    }.bind(this);

                    this.targetModel.save({}, {
                        wait: true,
                        success: successFunc,
                        error: errorFunc
                    });
                }
                else {
                    // Let backbone send post requests
                    this.currentModel.id = undefined;
                    this.currentModel.idAttribute = null;

                    errorFunc = function (model, msg, args) {
                        responseError = JSON.parse(msg.responseText);
                        validatorErrors[responseError.field] = responseError.errorMessage;
                        this.validator.showErrors(validatorErrors);

                        // error
                        if ($('input.invalid', this.el)[0]) {
                            $('html, body').animate({
                                scrollTop: $('input.invalid', this.el).offset().top - 100
                            }, 0);
                        }

                    }.bind(this);

                    successFunc = function (model, response) {
                        this.currentModel = model;

                        CJ.app.router.navigate('invoices/list', {
                            trigger: true
                        });
                    }.bind(this);

                    this.currentModel.save({}, {
                        wait: true,
                        success: successFunc,
                        error: errorFunc
                    });

                    // Let backbone send put requests
                    this.currentModel.idAttribute = 'invoiceId';
                }


            }.bind(this), function () {
                // error
                if ($('input.invalid', this.el)[0]) {
                    $('html, body').animate({
                        scrollTop: $('input.invalid', this.el).offset().top - 100
                    }, 0);
                }
            }.bind(this));

        }
        else {
            // error
            if ($('.invalidInput', this.el)[0]) {
                $('html, body').animate({
                    scrollTop: $('.invalidInput', this.el).offset().top - 100
                }, 0);
            }
        }
    },

    /**
     * @private
     * @param e
     */
    onFormEmailInput: function (e) {
        this.checkByEmail = false;
    },

    /**
     * @private
     * @param e
     */
    onFormPhoneInput: function (e) {
        this.checkByPhone = false;
    },

    /**
     * @private
     * @param success
     * @param error
     */
    submitClientForm: function (success, error) {
        var clients,
            targetModel,
            formValues = this.clientFormWidget.getValues();

        clients = CJ.app.collections.getCollection('clients', function (col) {
            clients = col;

            if (this.checkByEmail && this.checkByPhone) {
                if (formValues.email && formValues.phone) {
                    targetModel = clients.findWhere({ email: formValues.email, phone: formValues.phone, removed: 0 });
                }
            }
            else if (this.checkByEmail) {
                if (formValues.email) {
                    targetModel = clients.findWhere({ email: formValues.email, removed: 0 });
                }
            }
            else if (this.checkByPhone) {
                if (formValues.phone) {
                    targetModel = clients.findWhere({ phone: formValues.phone, removed: 0 });
                }
            }

            if (targetModel) {
                this.clientFormWidget.setTargetModel(targetModel);
            }

            this.clientFormWidget.onFormSubmitSuccess = success;
            this.clientFormWidget.onFormSubmitError = error;
            this.clientFormWidget.form.submit();
        }.bind(this));

    }

});