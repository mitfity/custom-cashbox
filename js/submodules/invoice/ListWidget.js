/**
 * Created by yurii on 03.07.15.
 */

CJ.submodules.invoice.ListWidget = CJ.core.View.extend( {

    classType: 'InvoiceListWidget',

    initialize: function (cfg) {
        this.tpl = JST['js/submodules/invoice/List.tpl.html'];

        CJ.core.View.prototype.initialize.apply(this, arguments);
        this.initGrid();
    },

    /**
     * @private
     */
    initGrid: function () {
        // Formatter for buttons column
        function buttonsFormatter(row, cell, value, columnDef, dataContext) {
            return value;
        }

        var columns = [
                {id: 'invoiceid', name: 'Invoice #', field: 'invoiceid', width: 50},
                {id: 'clientname', name: 'Client Name', field: 'clientname'},
                {id: 'datecreated', name: 'Date', field: 'datecreated', width: 55},
                {id: 'totalprice', name: 'Total', field: 'totalprice', width: 60},
                {id: 'buttons', name:'', field: 'buttons', formatter: buttonsFormatter, width: 25}
            ],
            options = {
                enableCellNavigation: true,
                enableColumnReorder: false,
                forceFitColumns: true
            },
            data = [],
            gridRenderTo = this.renderTo + ' .gridWrapper',
            invoicesCollection;

        invoicesCollection = CJ.app.collections.getCollection('invoices', function (col) {
            var invoices = col.models,
                clients;

            clients = CJ.app.collections.getCollection('clients', function (clientsCollection) {

                // Fill data
                for (var i = 0; i < invoices.length; i++) {
                    var client = clientsCollection.findWhere({ clientId: invoices[i].get('clientId') });

                    data[i] = {

                        invoiceid: CJ.core.utils.paddingWithZeros(invoices[i].id, 8),

                        datecreated: invoices[i].get('dateCreated'),

                        totalprice: '$' + invoices[i].get('total').toFixed(2),

                        buttons: '<div class="buttons"><a href=\"#invoices/edit/' + invoices[i].id + '\"><span' +
                        ' class="glyphicon glyphicon-edit"></span></a>' +
                        '<a href=\"#invoices/remove/' + invoices[i].id + '\"><span' +
                        ' class="glyphicon' +
                        ' glyphicon-trash"></span></a></div>',

                        id: 'invoice' + invoices[i].id

                    };

                    if (client) {
                        data[i].clientname = client.get('firstName') + ' ' + client.get('lastName');
                    }
                    else {
                        data[i].clientname = '';
                    }
                }

                this.gridWidget = new CJ.core.grid.Widget({
                    gridData: data,
                    gridColumns: columns,
                    gridOptions: options,
                    renderTo: gridRenderTo
                });
            }.bind(this));
        }.bind(this));
    },

    /**
     * This method is called before remove(). It is used for removing references.
     * @private
     */
    onRemove: function () {
        if (this.gridWidget) {
            this.gridWidget.remove();
            this.gridWidget = null;
        }
    }

});