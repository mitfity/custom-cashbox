/**
 * Created by mitfity on 29.06.2015.
 */

CJ.submodules.invoice.Model = Backbone.Model.extend({

    urlRoot: '/api/invoice',

    idAttribute: 'invoiceId',

    userId: 0,

    clientId: 0,

    dateCreated: null,

    total: 0,

    initialize: function () {
    },

    // Was bug when u modify id and save model then route was /invoice/:newId. I need /invoice/:oldId.
    url: function () {
        if (this.isNew()) {
            return this.urlRoot;
        }

        if (!this.changed[this.idAttribute]) {
            return this.urlRoot + '/' + this.id;
        }
        else {
            return this.urlRoot + '/' + this._previousAttributes[this.idAttribute];
        }
    }

});