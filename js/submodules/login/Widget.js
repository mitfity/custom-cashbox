/**
 * Created by yurii on 06.07.15.
 */

CJ.submodules.login.Widget = CJ.core.form.Widget.extend({

    classType: 'LoginWidget',

    initialize: function () {
        this.tpl = JST['js/submodules/login/Login.tpl.html'];
        CJ.core.form.Widget.prototype.initialize.apply(this, arguments);
    },

    /**
     * @override CJ.core.form.Widget
     */
    getForm: function () {
        return $('.loginForm', this.el);
    },

    /**
     * @override CJ.core.form.Widget
     */
    getValidationOptions: function () {
        return {
            rules: {

                email: {
                    required: true,
                    email: true
                },

                password: 'required'

            },

            messages: {
                email: {
                    required: 'Please enter your email.',
                    email: 'Please enter valid email address.'
                },
                password: 'Please enter your password.'
            }
        };
    },

    /**
     * @override CJ.core.form.Widget
     */
    onFormSubmit: function (e) {
        var validatorErrors = {},
            responseError;

        if (this.isValid()) {
            var success = function () {
                    CJ.app.redirectToDefaultRoute();
                },
                error = function (msg, args) {
                    responseError = JSON.parse(msg.responseText);

                    validatorErrors[responseError.field] = responseError.errorMessage;
                    this.validator.showErrors(validatorErrors);

                    $('input[name="password"]', this.el).val('');
                };

            CJ.app.userIdentity.logIn(this.getValues(), success.bind(this), error.bind(this));
        }
    }

});