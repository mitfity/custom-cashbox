/**
 * Created by yurii on 06.07.15.
 */

CJ.submodules.login.passwordRecovery.Widget = CJ.core.form.Widget.extend({

    classType: 'PasswordRecoveryWidget',

    initialize: function () {
        this.tpl = JST['js/submodules/login/passwordRecovery/PasswordRecovery.tpl.html'];

        CJ.core.form.Widget.prototype.initialize.apply(this, arguments);
    },

    /**
     * @override CJ.core.form.Widget
     */
    getForm: function () {
        return $('.passwordRecoveryForm', this.el);
    },

    /**
     * @override CJ.core.form.Widget
     */
    getValidationOptions: function () {
        return {
            rules: {

                email: {
                    required: true,
                    email: true
                }

            },

            messages: {
                email: {
                    required: 'Please enter your email.',
                    email: 'Please enter valid email address.'
                }
            }
        };
    },

    /**
     * @override CJ.core.form.Widget
     */
    onFormSubmit: function (e) {
        var email = this.getValues().email,
            validatorErrors = {},
            responseError;

        if (this.isValid()) {
            $.ajax({
                method: "POST",
                url: "http://cashbox.loc/api/user/reset",
                data: JSON.stringify(email),
                dataType: "json"
            })
                .done(function (user, msg, args) {
                    CJ.app.redirectToDefaultRoute();
                }.bind(this))
                .fail(function (msg, args) {
                    responseError = JSON.parse(msg.responseText);

                    validatorErrors[responseError.field] = responseError.errorMessage;
                    this.validator.showErrors(validatorErrors);
                }.bind(this));
        }
    }

});