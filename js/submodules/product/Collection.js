/**
 * Created by yurii on 30.06.15.
 */

CJ.submodules.product.Collection = Backbone.Collection.extend({

    model: CJ.submodules.product.Model,

    localStorage: new Backbone.LocalStorage("products-backbone"),

    initialize: function () {
        this.initEvents();
    },

    initEvents: function () {
        // Save model to local storage
        this.on('add change', function (model) {
            model.save();
        });
    },

    /**
     * @param {Nubmer} id productId
     * @returns {Object} Backbone,Model
     */
    getModelById: function (id) {
        return this.findWhere({ productId: id });
    },

    /**
     * Removes products of invoice by passed invoiceId.
     * @param invoiceId
     */
    removeModelsByInvoiceId: function (invoiceId) {
        this.where({ invoiceId: invoiceId }).forEach(function (el, index, array) {
            el.destroy();
        });
    }

});