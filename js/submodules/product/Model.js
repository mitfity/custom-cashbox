/**
 * Created by mitfity on 29.06.2015.
 */

CJ.submodules.product.Model = Backbone.Model.extend({

    invoiceId: undefined,

    productName: null,

    description: null,

    count: 0,

    price: 0,

    tax: 10,

    initialize: function () {
        this.set({ id: this.get('productId') });
        this.unset('productId');
    }

});