/**
 * Created by mitfity on 30.06.2015.
 */

CJ.submodules.user.Collection = Backbone.Collection.extend({

    model: CJ.submodules.user.Model,

    url: '/api/user',

    initialize: function () {
        this.initEvents();
    },

    initEvents: function () {
        // Save model to local storage

        this.on('add', function (model, collection) {
            model.save({}, {
                success: function (model, response) {
                }
            });
        });

        this.on('change', function (model) {
            model.save(model.attributes, {
                wait: true,
                success: function (model, response) {
                },
                error: function (model, msg, args) {
                    console.log(msg);
                }
            });
        });
    },

    /**
     * @param {Number} id userId
     * @returns {Object} Backbone.Model
     */
    getModelById: function (id) {
        return this.findWhere({ userId: id });
    },

    /**
     * @param {String} email
     * @param {Number} modelId userId
     * @returns {boolean}
     */
    hasModelWithEmail: function (email, modelId) {
        var models = this.filter(function (model) {
            return model.id !== modelId && model.get('email') === email;
        });

        return (models.length) ? true : false;
    }

});