/**
 * Created by yurii on 13.07.15.
 */

CJ.submodules.user.FormWidget = CJ.core.form.Widget.extend({

    classType: 'UserFormWidget',

    /**
     * @property {jQuery}
     */
    form: null,

    /**
     * @property {Object} Editing model.
     */
    targetModel: null,

    /**
     * @property {Object} Current model of the form. It acts as a buffer.
     */
    currentModel: null,

    /**
     * @private
     */
    parentWindow: null,

    initialize: function (cfg) {
        $.extend(this, cfg);

        this.tpl = JST['js/submodules/user/Form.tpl.html'];
        CJ.core.form.Widget.prototype.initialize.apply(this, arguments);
        this.initModels();

        this.manageInputsDisplay();
    },

    initEvents: function () {
        CJ.core.form.Widget.prototype.initEvents.apply(this, arguments);

        $('.btn-file', this.el).on('change', this.onFileSelected.bind(this));

        $('#phone').keypress( function(e) {
            var chr = String.fromCharCode(e.which);
            if ("0123456789-() ".indexOf(chr) < 0)
                return false;
        });

        $('.passwordGroup input', this.form).each(function (index, input) {
            var arePasswordsEmpty = true;

            $(input).on('input', function (e) {
                if ($(input).val()) {
                    $('.passwordGroup input', this.form).each(function (relativeIndex, relativeInput) {
                        $(relativeInput).rules('add', {
                            required: true
                        });

                        $('label[for=\"'+$(relativeInput).attr('id')+'\"].inputLabel').addClass('required');
                    });
                }
                else {
                    $('.passwordGroup input', this.form).each(function (relativeIndex, relativeInput) {
                        if ($(relativeInput).val()) {
                            arePasswordsEmpty = false;
                            return false;
                        }
                    });

                    if (arePasswordsEmpty) {
                        $('.passwordGroup input', this.form).each(function (relativeIndex, relativeInput) {
                            $(relativeInput).rules('remove', 'required');
                            $('label[for=\"'+$(relativeInput).attr('id')+'\"].inputLabel').removeClass('required');
                            $(relativeInput).valid();
                        });
                    }
                }


            }.bind(this));
        }.bind(this));
    },

    initModels: function () {
        var modelId = this.modelId;

        this.currentModel = new CJ.submodules.user.Model();
        if (modelId !== undefined) {
            this.targetModel = new CJ.submodules.user.Model({
                userId: modelId
            });
            this.targetModel.fetch({

                success: function (model, response) {
                    this.currentModel.set(model.attributes);
                    this.setValues(this.currentModel.attributes);

                    $('.userAvatar img', this.el).attr('src', this.currentModel.get('avatar'));
                }.bind(this),

                error: function (model, error) {
                    console.log('error');
                    console.log(error.responseText());
                    CJ.app.redirectToDefaultRoute();
                }

            });
        }
        else {
            $('.userAvatar img', this.el).attr('src', this.currentModel.get('avatar'));
        }
    },

    /**
     * Shows some hidden inputs according to if it is profile or not.
     * @private
     */
    manageInputsDisplay: function () {
        if (this.isProfile) {
            $('.roleFormGroup', this.el).remove();
            $('.passwordGroup', this.el).removeAttr('hidden');
            $('.password', this.el).val('');
            $('.passwordRepeat', this.el).val('');
        }
    },

    /**
     * @private
     * @param {Event} e
     */
    onFileSelected: function (e) {
        var files = e.target.files;
        var file = files[0];

        if (files && file) {
            var reader = new FileReader();

            reader.onload = function(readerEvt) {
                var img = new Image();

                img.src = readerEvt.target.result;
                $('.userAvatar img', this.el).prop('src', img.src);
                this.currentModel.set({
                    avatar: img.src
                });
            }.bind(this);

            reader.readAsDataURL(file);
        }
    },

    /**
     * @private
     * @override CJ.core.form.Widget
     * @returns {*|jQuery|HTMLElement}
     */
    getForm: function () {
        return $('.editForm', this.el);
    },

    /**
     * @private
     * @override CJ.core.form.Widget
     * @returns {Object}
     */
    getValidationOptions: function () {
        var validationOptions = {
            rules: {

                firstName: 'required',

                lastName: 'required',

                phone: {
                    required: true
                },

                email: {
                    required: true,
                    email: true
                }

            },
            messages: {

                firstName: {
                    required: 'Please enter your first name'
                },

                lastName: {
                    required: 'Please enter your last name'
                },

                phone: {
                    required: 'Please enter your phone number.'
                },

                email: {
                    required: 'Please enter your email.',
                    email: 'Please enter valid email address.'
                }
            }
        };

        if (this.isProfile) {
            _.extend(validationOptions.rules, {

                oldPassword: {
                    required: false
                },

                newPassword: {
                    required: false
                },

                passwordRepeat: {
                    required: false,
                    equalTo: '#newPassword'
                }

            });

            _.extend(validationOptions.messages, {

                oldPassword: {
                    required: 'Please enter old password.'
                },

                newPassword: {
                    required: 'Please enter new password.'
                },

                passwordRepeat: {
                    required: 'Please repeat your password.',
                    equalTo: 'Passwords have to be equal.'
                }

            });
        }

        return validationOptions;
    },

    /**
     * @private
     * @ovarride CJ.core.form.Widget
     * @param e
     * @returns {boolean}
     */
    onFormSubmit: function (e) {
        if (this.isValid()) {
            var responseError = null,
                validatorErrors = {},
                successFunc,
                errorFunc;

            this.currentModel.set(this.getValues());

            if (this.targetModel) {
                this.targetModel.set(this.currentModel.attributes);

                errorFunc = function (model, msg, args) {
                    responseError = JSON.parse(msg.responseText);
                    validatorErrors[responseError.field] = responseError.errorMessage;
                    this.validator.showErrors(validatorErrors);
                    this.onFormSubmitError();
                }.bind(this);

                successFunc = function (model, response) {
                    this.onFormSubmitSuccess();
                }.bind(this);

                this.targetModel.save({}, {
                    wait: true,
                    success: successFunc,
                    error: errorFunc
                });
            }
            else {
                errorFunc = function (model, msg, args) {
                    responseError = JSON.parse(msg.responseText);
                    validatorErrors[responseError.field] = responseError.errorMessage;
                    this.validator.showErrors(validatorErrors);
                    this.onFormSubmitError();
                }.bind(this);

                successFunc = function (model, response) {
                    var notifyPasswordSent = new CJ.core.window.Widget({
                        title: 'Password',
                        contentTpl: _.template('<p>Password has been sent to your email.</p>')
                    });
                    notifyPasswordSent.show();

                    setTimeout(function () {
                        notifyPasswordSent.close();
                        this.onFormSubmitSuccess();
                    }.bind(this), 3500);
                }.bind(this);

                this.currentModel.save({}, {
                    wait: true,
                    success: successFunc,
                    error: errorFunc
                });
            }

            return true;


        }

        return false;
    },

    /**
     * It is called when form is submitted successfully.
     */
    onFormSubmitSuccess: function () {
    },

    /**
     * This is called when form submit is failed.
     */
    onFormSubmitError: function () {
    }

});