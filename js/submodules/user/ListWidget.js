/**
 * Created by yurii on 02.07.15.
 */

CJ.submodules.user.ListWidget = CJ.core.View.extend({

    classType: 'UserListWidget',

    /**
     * @private
     */
    gridWidget: null,

    /**
     * @private
     */
    users: null,

    initialize: function (cfg) {
        this.tpl = JST['js/submodules/user/List.tpl.html'];

        CJ.core.View.prototype.initialize.apply(this, arguments);
        this.initGrid();
    },

    initGrid: function () {
        // Formatter for title column
        function usernameFormatter(row, cell, value, columnDef, dataContext) {
            return value;
        }

        var columns = [
                {id: 'username', name: 'User Name', field: 'username', formatter: usernameFormatter},
                {id: 'phone', name: 'Phone', field: 'phone'},
                {id: 'email', name: 'E-mail', field: 'email'},
                {id: 'role', name: 'Role', field: 'role', width: 50},
                {id: 'invoicecount', name: 'Total Invoice Count', field: 'invoicecount'},
                {id: 'buttons', name: '', field: 'buttons', formatter: usernameFormatter, width: 45}
            ],
            options = {
                enableCellNavigation: true,
                enableColumnReorder: false,
                forceFitColumns: true
            },
            data = [],
            gridRenderTo = this.renderTo + ' .gridWrapper',
            users,
            invoices,
            totalInvoiceCount = 0,
            totalInvoiceCountForEach = function (el, index, coll) {
                totalInvoiceCount += 1;
            };

        // After we got collection from storage
        var onUsersFetchSuccess = function (col) {
                users = this.users = col.models;
                invoices = CJ.app.collections.getCollection('invoices', onInvoicesFetchSuccess);
        }.bind(this),
            onInvoicesFetchSuccess = function (col) {
                // Fill data
                for (var i = 0; i < this.users.length; i++) {
                    // Some variables depend on other, so they are not just passed to new object parameters.
                    // They are configured first.
                    var usernameColumn = '<div class="cellImage"><img src="' + users[i].get('avatar') + '"></div>' +
                        users[i].get('firstName') + ' ' + users[i].get('lastName'),
                        phoneColumn = users[i].get('phone'),
                        emailColumn = users[i].get('email'),
                        roleColumn = users[i].get('role'),
                        invoicecountColumn,
                        _inactiveColumn = !users[i].get('isActive'),
                        activeClassColumn,
                        buttonsColumn,
                        idColumn = 'user' + users[i].id,
                        userInvoices = invoices.where({ userId: users[i].get('userId') });

                    totalInvoiceCount = 0;
                    userInvoices.forEach(totalInvoiceCountForEach);

                    invoicecountColumn = totalInvoiceCount;
                    activeClassColumn = _inactiveColumn ? 'inactive' : '';
                    buttonsColumn = '<div class="buttons"><a href=\"#users/edit/' + users[i].id + '\"><span' +
                        ' class="glyphicon' +
                        ' glyphicon-pencil"></span>' +
                        '</a><a href="javascript:void(0)"><span class="glyphicon glyphicon-off ' + activeClassColumn +
                        '"></span></a></div>';

                    data[i] = {
                        username: usernameColumn,
                        phone: phoneColumn,
                        email: emailColumn,
                        role: roleColumn,
                        invoicecount: invoicecountColumn,
                        _inactive: _inactiveColumn,
                        activeClass: activeClassColumn,
                        buttons: buttonsColumn,
                        id: idColumn
                    };
                }

                this.gridWidget = new CJ.core.grid.Widget({
                    gridData: data,
                    gridColumns: columns,
                    gridOptions: options,
                    renderTo: gridRenderTo,
                    dataCollection: users
                });

                // Should be after grid initialization
                this.initEvents();
            }.bind(this);

        CJ.app.collections.getCollection('users', onUsersFetchSuccess);
    },

    initEvents: function () {
        $('.grid', this.tpl).on('click', 'a .glyphicon-off',this.onUserBanClick.bind(this));
    },

    /**
     * User ban click handler.
     * @private
     * @param e
     */
    onUserBanClick: function (e) {
        var cell = this.gridWidget.grid.getCellFromEvent(e),
            row = cell.row,
            user = this.users[row],
            _inactiveColumn = !user.get('isActive'),
            activeClassColumn = !_inactiveColumn ? 'inactive' : '';

        user.set({
            isActive: _inactiveColumn
        });
        this.gridWidget.setRowData(row, {

            _inactive: !user.get('isActive'),

            buttons: '<div class="buttons"><a href=\"#users/edit/' + user.id + '\"><span' +
            ' class="glyphicon' +
            ' glyphicon-pencil"></span>' +
            '</a><a href="javascript:void(0)"><span class="glyphicon glyphicon-off ' + activeClassColumn +
            '"></span></a></div>'

        }, $(e.currentTarget));
    },

    /**
     * This method is called before remove(). It is used for removing references.
     * @private
     */
    onRemove: function () {
        if (this.gridWidget) {
            this.gridWidget.remove();
            this.gridWidget = null;
        }
    }

});