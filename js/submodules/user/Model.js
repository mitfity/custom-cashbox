/**
 * Created by mitfity on 29.06.2015.
 */

CJ.submodules.user.Model = Backbone.Model.extend({

    urlRoot: '/api/user',

    idAttribute: 'userId',

    defaults: {

        email: null,

        firstName: null,

        lastName: null,

        avatar: 'https://s3.amazonaws.com/upload.uxpin/files/8669/12532/placeholder.png',

        phone: null,

        role: null,

        isActive: true
    },

    initialize: function () {
    }

});