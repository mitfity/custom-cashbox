/**
 * Created by yurii on 08.07.15.
 */

CJ.submodules.viewport.Widget = CJ.core.View.extend({

    classType: 'ViewportWidget',

    currentContent: null,

    initialize: function () {
        this.tpl = JST['js/submodules/viewport/Viewport.tpl.html'];
        CJ.core.View.prototype.initialize.apply(this, arguments);

        this.initEvents();
        this.setNavigation();
    },

    initEvents: function () {
        $('.showUserActions', this.el).on('click', this.onShowUserActionsClick);
        $('.showUserActions', this.el).on('focusout', this.onShowUserActionsFocusOut);
        $('.showMenu .btn', this.el).on('click', this.onShowMenuClick.bind(this));
        $('.viewport').on('transitionend webkitTransitionEnd oTransitionEnd', this.onViewportTransitionEnd);

        this.listenTo(CJ.app.router, 'viewport.changeContent', this.onChangeContent.bind(this));

        CJ.app.userIdentity.model.on('change', function (e) {
            console.log('changed');
            console.log(arguments);
        });
    },

    /**
     * Toggles navigation item active.
     * @param {String} contentName Name of navigation item to activate.
     */
    setNavigation: function (contentName) {
        var navSelector,
            navItems = $('.navItems', this.el);

        navItems.children('li').each(function (index, el) {
            $('a', el).toggleClass('active', false);
        });

        if (!contentName) {
            contentName = 'users';
        }

        navSelector = '.' + contentName + 'Link';
        $(navSelector, this.el).toggleClass('active', true);
    },

    /**
     * Show menu click handler.
     * @param {Event} e
     */
    onShowMenuClick: function (e) {
        $('.viewport').toggleClass('slide-menu-push-toright');
        $('.slide-menu').toggleClass('slide-menu-open');
    },

    /**
     * Viewport transition end handler. Triggers 'resize' event.
     * @param e
     */
    onViewportTransitionEnd: function (e) {
        if (e.originalEvent.propertyName === 'width' &&
                e.eventPhase === Event.BUBBLING_PHASE) {
            $(this).trigger($.Event('resize'));
        }
    },

    /**
     * Show user actions click handler.
     * @param {Event} e
     */
    onShowUserActionsClick: function (e) {
        $(this).toggleClass('expand');
    },

    /**
     * Show user actions focus out. Resets arrow key to default state.
     * @param e
     */
    onShowUserActionsFocusOut: function (e) {
        $(this).toggleClass('expand', false);
    },

    getTplData: function () {
        var user = CJ.app.userIdentity.getModel();
        return { username: user.get('firstName') + user.get('lastName'), userAvatar: user.get('avatar') };
    },

    /**
     * @param {Object} cfg Config of content to create inside viewport.
     */
    setContent: function(cfg) {
        if (this.currentContent) {
            this.currentContent.remove();
        }

        if (!cfg) {
            cfg = {};
        }

        cfg.renderTo = '.contentContainer';
        this.currentContent = new cfg.contentPrototype(cfg);
    },

    /**
     * Change content handler.
     * @param {Object} cfg Event config.
     */
    onChangeContent: function (cfg) {
        this.setNavigation(cfg.contentName);
        this.setContent(cfg);
    },

    /**
     * This method is called before remove(). It is used for removing references.
     * @private
     */
    onRemove: function () {
        if (this.currentContent) {
            this.currentContent.remove();
        }
    }

});